<div class="bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <ul>
                    <li class="title">Hỗ trợ</li>
                    <li><a href="">FAQ</a></li>
                    <li><a href="">Liên hệ</a></li>
                    <li><a href="">Đổi trả</a></li>
                    <li><a href="">Bảo hành</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <ul>
                    <li class="title">Thông tin hữu ích</li>
                    <li><a href="">Everon FAMILY</a></li>
                    <li><a href="">Chính sách vận chuyển</a></li>
                    <li><a href="">Tìm cửa hàng</a></li>
                    <li><a href="">Sự kiện</a></li>
                    <li><a href="">BB</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <ul>
                    <li class="title">Về chúng tôi</li>
                    <li><a href="">Everpia JSC</a></li>
                    <li><a href="">Tin tức</a></li>
                    <li><a href="">Đầu tư</a></li>
                    <li><a href="">Việc làm</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <ul>
                    <li class="title">Social Media</li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<input type="hidden" id="thisMenu" value=""/>
<script type="application/javascript">
    $('.hero-slider').slick({
        arrows: false,
        dotted: false
    });

    $('.menu li a').hover(function () {
        $('.sub-menu').height(320);
        $('.sub-gift, .sub-bedroom, .sub-collection').hide();
        var id = $(this).parent().attr('row-id');
        if ($('.sub-' + id).css('display') == 'none') {

            if (id != $('#thisMenu').val()) {
                $('.sub-' + id).slideDown();
                $('.sub-' + id).show();
            } else {
                $('.sub-' + id).show();
            }

            $('#thisMenu').val(id);
        } else {

        }


    });


</script>
</body>
</html>