<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/lightslider.min.css">
    <title>Title</title>
</head>
<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="js/lightslider.min.js"></script>

<div class="wrapper">
    <div class="header clearfix">

        <div class="logo"><a href=""><img src="images/logo.png" alt=""></a></div>
        <div class="menu">
            <ul>
                <li row-id="gift"><a href="">Gift</a></li>
                <li row-id="bedroom"><a href="">Bedroom</a></li>
                <li row-id="collection"><a href="">Collection</a></li>
            </ul>
            <div class="sub-menu">

                <div class="sub-gift">
                    <div class="container">
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Mon
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    General
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="sub-bedroom">
                    <div class="container">
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Summer
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Bedding
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Insert
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Mattress
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sub-collection">
                    <div class="container">
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Artemis Collection
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Artemis Collection
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>
                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Artemis Collection
                                </a>
                            </div>
                        </div>
                        <div class="sub-item">
                            <div class="thumb">
                                <a href="category.php"><img src="images/collection_1.jpg" alt=""/></a>

                            </div>
                            <div class="title">
                                <a href="category.php">
                                    Artemis Collection
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sub-login" style="display: none">
                <div class="container">
                    <div class="col-sm-8">
                        <div class="col-sm-8">
                            <div class="row">
                                <form action="">
                                    <div class="login-title">Đăng Nhập</div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3"><label for="">Email</label></div>
                                        <div class="col-sm-9"><input type="text"/></div>
                                    </div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3"><label for="">Mật khẩu</label></div>
                                        <div class="col-sm-9"><input type="text"/></div>
                                    </div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3">&nbsp;</div>
                                        <div class="col-sm-9"><input type="submit" value="Đăng nhập"/>
                                            <a href="">Quên mật khẩu?</a></div>
                                    </div>


                                </form>
                            </div>
                        </div>
                        <div class="col-sm-4">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-login">
            <i class="glyphicon glyphicon-search"></i> <span></span> <a href="">Login</a>
        </div>
    </div>
