<?php include('header.php');?>
    <div class="main">
        <div class="hero-slider clearfix">
            <div class="slider-item">
                <img src="images/slider_1.png" alt="">
            </div>
            <div class="slider-item">
              <img src="images/slider_1.png" alt="">
            </div>
        </div>
        <div class="container clearfix">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="home-block">
                            <div class="block-title">Giới thiệu</div>
                            <div class="block-body">
                                <iframe src="https://www.youtube.com/embed/PV8CxWbcUP0" frameborder="0"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="home-block">
                            <div class="block-title">Sản phẩm bán chạy</div>
                            <div class="block-body">
                                <a href=""><img src="images/home-sanpham.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="sub-tab clearfix">
                                <ul>
                                    <li class="col-sm-4"><a href="">TVC</a></li>
                                    <li class="col-sm-4"><a href="">Thiết kế</a></li>
                                    <li class="col-sm-4"><a href="">Công nghệ</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6">

                                <div class="home-news">
                                    <div class="thumb"><a href=""><img src="images/home-sanpham.png" alt=""></a></div>
                                    <div class="title"><a href="">Thiết kế</a></div>
                                    <div class="desc">A good night’s sleep in a comfy bed. Bedroom furniture that gives
                                        you
                                        space to store your things (in a way that means you’ll find them again). With
                                        warm
                                        lighting to set the mood and soft textiles to snuggle up in. All at a price that
                                        lets you rest easy. It’s what sweet dreams are made of.
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="home-news">
                                    <div class="thumb"><a href=""><img src="images/home-sanpham.png" alt=""></a></div>
                                    <div class="title"><a href="">Thiết kế</a></div>
                                    <div class="desc">A good night’s sleep in a comfy bed. Bedroom furniture that gives
                                        you
                                        space to store your things (in a way that means you’ll find them again). With
                                        warm
                                        lighting to set the mood and soft textiles to snuggle up in. All at a price that
                                        lets you rest easy. It’s what sweet dreams are made of.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 mr-top-20">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="home-news">
                                    <div class="thumb"><a href=""><img src="images/sp-2.png" alt=""></a></div>
                                    <div class="title"><a href="">Bộ sưu tâp gối</a></div>
                                    <div class="desc">A good night’s sleep in a comfy bed. Bedroom furniture that gives
                                        you
                                        space to store your things (in a way that means you’ll find them again). With
                                        warm
                                        lighting to set the mood and soft textiles to snuggle up in. All at a price that
                                        lets you rest easy. It’s what sweet dreams are made of.
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="home-news">
                                    <div class="thumb"><a href=""><img src="images/sp-2.png" alt=""></a></div>
                                    <div class="title"><a href="">Bộ bàn ăn Yellow-S</a></div>
                                    <div class="desc">A good night’s sleep in a comfy bed. Bedroom furniture that gives
                                        you
                                        space to store your things (in a way that means you’ll find them again). With
                                        warm
                                        lighting to set the mood and soft textiles to snuggle up in. All at a price that
                                        lets you rest easy. It’s what sweet dreams are made of.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-free-ship">
            <div class="logo-2"><img src="images/everon-logo-2.png" alt=""></div>
            <h2>Hơn 600 đại lý trên toàn quốc</h2>
            <div><span class="free-ship">Miễn phí giao hàng và trả hàng</span></div>
        </div>
    </div>
  <?php include ('bottom.php')?>