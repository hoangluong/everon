<?php include('header.php'); ?>

    <div class="main graybg">
        <div class="container">
            <div class="row">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="">Trang chủ</a><span>/</span></li>
                        <li><a href="">Phòng ngủ</a><span>/</span></li>
                        <li><a href="">Chăn - Ga - Gối</a><span>/</span></li>
                        <li>EP1234</li>
                    </ul>
                </div>
                <div class="product-detail clearfix">
                    <div class="col-sm-5">
                        <div class="row">
                            <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                <li data-thumb="images/products.png">
                                    <img src="images/products.png"/>
                                </li>
                                <?php

                                for ($i = 0; $i < 5; $i++) {
                                    ?>
                                    <li data-thumb="images/products.png">
                                        <img src="images/products.png"/>
                                    </li>
                                <?php
                                }
                                ?>


                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="title">Bộ chăn ga EP1738</div>
                        <div class="vote">
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <i class="glyphicon glyphicon-star"></i>
                            <span>| Sẵn sàng giao hàng</span>
                        </div>
                        <div class="price"><label for="">Giá bán:</label> 100,000 VNĐ</div>
                        <div class="col-sm-12 size">
                            <select name="" class="slSize col-sm-8" id="">
                                <option value="">L</option>
                                <option value="">XL</option>
                                <option value="">XXL</option>
                            </select>
                            <select name="" class="slQty col-sm-4" id="">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                            </select>
                        </div>
                        <div>
                            <button class="btn-buy">MUA</button>
                        </div>
                        <div class="shoping-info">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="ico"><img src="images/cart-icon.jpg" alt=""/></div>
                                    <div>Thêm vào giỏ hàng</div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="ico"><img src="images/cart-icon.jpg" alt=""/></div>
                                    <div>Sản phẩm yêu thích</div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="ico"><img src="images/cart-icon.jpg" alt=""/></div>
                                    <div>Share</div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="ico"><img src="images/cart-icon.jpg" alt=""/></div>
                                    <div>Đại lý</div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <ul class="nav nav-tabs product-tabs">
                            <li class="active"><a data-toggle="tab" href="#desc">Miêu tả</a></li>
                            <li><a data-toggle="tab" href="#detail">Chi tiết</a></li>
                            <li><a data-toggle="tab" href="#vote">Đánh giá của khách hàng</a></li>

                        </ul>

                        <div class="tab-content">
                            <div id="desc" class="tab-pane fade in active">

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div id="detail" class="tab-pane fade">

                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                                    ea commodo consequat.</p>
                            </div>
                            <div id="vote" class="tab-pane fade">

                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam.</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                <hr/>
                <div class="ship-info">
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2">
                            <div class="ico"><img src="images/car-icon.jpg" alt=""/></div>
                            <div>Miễn phí vận chuyển</div>
                        </div>
                        <div class="col-sm-2">
                            <div class="ico"><img src="images/donggoi-ico.jpg" alt=""/></div>
                            <div>Đóng gói sản phẩm</div>
                        </div>
                        <div class="col-sm-2">
                            <div class="ico"><img src="images/chinhsach-icon.jpg" alt=""/></div>
                            <div>Chính sách đổi trả</div>
                        </div>
                        <div class="col-sm-2">
                            <div class="ico"><img src="images/chinhsach-icon.jpg" alt=""/></div>
                            <div>Bảo hành 24 tháng</div>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
                </div>

            </div>
        </div>

        <div class="white-bg ">
            <div class="container">
                <div class="col-sm-8">
                    <div class="list-product">
                        <div class="list-title">Tự chọn</div>
                        <hr/>
                        <div class="list-body">
                            <?php
                            for ($i = 0; $i < 5; $i++) {
                                ?>
                                <div class="product-item clearfix">
                                    <div class="col-sm-5">
                                        <div class="thumb">
                                            <img src="images/products.png" alt=""/>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">

                                        <div class="title">Bộ chăn ga EP1738</div>
                                        <div class="vote">
                                            <i class="glyphicon glyphicon-star"></i>
                                            <i class="glyphicon glyphicon-star"></i>
                                            <i class="glyphicon glyphicon-star"></i>
                                            <i class="glyphicon glyphicon-star"></i>
                                            <i class="glyphicon glyphicon-star"></i>
                                            <span>| Sẵn sàng giao hàng</span>
                                        </div>
                                        <div class="price"><label for="">Giá bán:</label> 100,000 VNĐ</div>
                                        <div class="col-sm-12 size">
                                            <select name="" class="slSize col-sm-8" id="">
                                                <option value="">L</option>
                                                <option value="">XL</option>
                                                <option value="">XXL</option>
                                            </select>
                                            <select name="" class="slQty col-sm-4" id="">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                            </select>
                                        </div>
                                        <div>
                                            <button class="btn-buy btn-green">THÊM VÀO GIỎ HÀNG</button>
                                        </div>

                                    </div>

                                </div>
                                <hr/>
                            <?php } ?>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="choosed">
                        <div class="title">Đã chọn</div>
                        <div class="sub-title">Đi tới giỏ hàng</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#image-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 5,
            slideMargin: 0,
            speed: 500,
            auto: false,
            loop: true,
            onSliderLoad: function () {
                $('#image-gallery').removeClass('cS-hidden');
            }
        });
    </script>
<?php include('bottom.php'); ?>