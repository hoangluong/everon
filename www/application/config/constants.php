<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


/* End of file constants.php */
/* Location: ./system/application/config/constants.php */

/* FTP FTP config for File image server */
define('FTP_IMG_HOST', '');
define('FTP_IMG_DOMAIN', '');
define('FTP_IMG_USER', '');
define('FTP_IMG_PASS', '');
define('FTP_IMG_REAL_PATH', '');
define('FTP_IMG_UPLOAD_REAL_PATH', '');
/* end FTP config for File image server */

define('STATIC_DOMAIN', "http://localhost/book/");
define('STATIC_MIN_VERSION', '.min');
define('PRICE_CPM', 4000);
define('UTF8_CASTING', 0);
define('DIR_JS', 'public/js/');
define('DIR_CSS', 'public/css/');
define('DIR_CACHE', 'public/files/');
define('JS_COMBINE_MODE', FALSE);
define('JS_PACK_MODE', FALSE);
define('JS_MINIFY_MODE', FALSE);
define('ALLOW_IMAGE_TYPE','.jpg, .jpeg, .png, .gif');
define('MAX_IMAGE_SIZE','1024');
 
define('API_PUBLIC_KEY','apiadmicro2014');
define('API_HDCN_URL','https://hd.admicro.vn/api/pr_adpage.asmx/');
define('API_HDCN_KEY','5530d551f0985fda8bf4c3c59724eef3');

define('CURLOPT_DF','df');
define('CURLOPT_DT','dt');
define('CURLOPT_BOC','boc');
define('CURLOPT_PT','pt');
define('CURLOPT_ID','id');
