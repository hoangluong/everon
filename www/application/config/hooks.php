<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['post_controller_constructor'] = array(
                                'class'    => 'App_Loader',
                                'function' => 'init',
                                'filename' => 'App_Loader.php',
                                'filepath' => 'core',
                                'params'   => '',
                                );
$hook['post_controller_constructor'] = array(
                                'class'    => 'Mycommon',
                                'function' => 'validAuth',
                                'filename' => 'Mycommon.php',
                                'filepath' => 'libraries',
                                'params'   => '',
                                );
$hook['pre_system'] = array(
                                'class'    => 'App_Loader',
                                'function' => 'pre_system',
                                'filename' => 'App_Loader.php',
                                'filepath' => 'core',
                                'params'   => '',
                                );


/* End of file hooks.php */
/* Location: ./system/application/config/hooks.php */