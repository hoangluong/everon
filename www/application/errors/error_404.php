 <?php
 session_start();
 $_CI = get_instance();
 $_CI->load->library('session'); 
$todate = date("Y-m-d");
$fromdate = date("Y-m-d",strtotime('- 30days'));
 ?>
<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"></html><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"></html><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title> Admin Lab Dashboard</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?php echo base_url()?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>css/style.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>css/style_responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>css/style_default.css" rel="stylesheet" id="style_color" />
    <link href="<?php echo base_url()?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" />    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/uniform/css/uniform.default.css" />
     <link rel="stylesheet" type="text/css" href="http://thevectorlab.net/adminlab/assets/data-tables/DT_bootstrap.css" />   
    <link href="<?php echo base_url()?>assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="<?php echo base_url()?>js/jquery-1.8.3.min.js"></script>
    <!-- Include Date Range Picker -->
     <script src="<?php echo base_url()?>js/moment.min.js"></script>    
<script type="text/javascript" src="<?php echo base_url()?>js/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/daterangepicker.css" />
<script type="text/javascript">
var base_url = '<?php echo base_url()?>';
</script>
</head>

<body class="fixed-top page_report">
        <div id="header" class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">

            <div class="container-fluid">
                <a class="brand" href="<?php echo base_url()?>"><img src="http://adnetwork.admicro.vn/adpage/images/PRlogo.png" alt="Admin Lab" /></a><a class="btn btn-navbar collapsed" id="main_menu_trigger" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="arrow"></span></a>
                <?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$pos = strpos($actual_link, '?');
$actual_link = str_replace('&type=all', '', $actual_link);
$actual_link = str_replace('&type=pc', '', $actual_link);
$actual_link = str_replace('&type=mob', '', $actual_link);
$actual_link = (!$pos)?$actual_link.'?':$actual_link;
                ?>
                <div class="menu-top">
                    <ul>
                        <li <?php echo ($_CI->session->userdata('type') == 'all' || !$_CI->session->userdata('type'))?' class="active"':'';?>><a href="<?php echo $actual_link?>&type=all">ALL</a></li>
                       <li <?php echo ($_CI->session->userdata('type') == 'pc')?' class="active"':'';?>><a href="<?php echo $actual_link?>&type=pc">PC</a></li>  
                       <li<?php echo ($_CI->session->userdata('type') == 'mob')?' class="active"':'';?>><a href="<?php echo $actual_link?>&type=mob">Mobile</a></li>   
                    </ul>
                </div>
                
                <div class="top-nav">
                    <ul class="nav pull-right top-menu">
                       <!-- <li class="dropdown mtop5"><a class="dropdown-toggle element" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title=""><i class="icon-cog"></i></a></li>
                        <li class="dropdown mtop5" id="header_notification_bar"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-bell-alt"></i><span class="badge badge-warning">7</span></a><ul class="dropdown-menu extended notification"><li><p>You have 7 new notifications</p></li><li><a href="#"><span class="label label-important"><i class="icon-bolt"></i></span> Server #3 overloaded. <span class="small italic">34 mins</span></a></li><li><a href="#"><span class="label label-warning"><i class="icon-bell"></i></span> Server #10 not respoding. <span class="small italic">1 Hours</span></a></li><li><a href="#"><span class="label label-important"><i class="icon-bolt"></i></span> Database overloaded 24%. <span class="small italic">4 hrs</span></a></li><li><a href="#"><span class="label label-success"><i class="icon-plus"></i></span> New user registered. <span class="small italic">Just now</span></a></li><li><a href="#"><span class="label label-info"><i class="icon-bullhorn"></i></span> Application error. <span class="small italic">10 mins</span></a></li><li><a href="#">See all notifications</a></li></ul></li>
                       --><li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="username"><?php echo $_CI->session->userdata['username']?></span><i class="icon-user"></i><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                              <!--  <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
                                <li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
                                <li><a href="#"><i class="icon-calendar"></i> Calendar</a></li>
                                <li class="divider"></li> -->
                                <li><a href="<?php echo base_url('login/logout')?>"><i class="icon-key"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
             <div id="container" class="row-fluid">
          
          
        <div id="sidebar" class="">
   
           
           <ul class="sidebar-menu">
            <li class="has-sub <?php echo !isset($domain)?'active':'';?>"><a href="<?php echo base_url('home/site')?>?fromdate=<?php echo $fromdate?>&todate=<?php echo $todate?>" ><span class="" ><i class="icon-star"></i></span> <b>All website data</b></a></li>
          
             <?php
             $variable = $_CI->common->dataSites();
                foreach ($variable as $key) {
             ?>
            <li class="has-sub <?php echo (@$domain == $key)?'active':'';?>"><a href="<?php echo base_url('home/site')?>?domain=<?php echo $key?>&fromdate=<?php echo $fromdate?>&todate=<?php echo $todate?>" ><span class="" ><i class="icon-star"></i></span> <b><?php echo $key?></b></a></li>
           <?php }?>
        
        </ul>
        </div>
        
        
        <div id="main-content-2" style="margin-left:250px;">
          <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <!--<h3 class="page_title">Website: <?php echo $info[0]->domain?></h3>-->
                        <div class="sub_menu_page">
                            <ul>
                                <li><a href="" class="active">Error</a></li>
                            </ul>
                        </div>

                        <div class="content_report">
                           <div class="row-fluid" >
               <div class="span12">
                   <div class="space20"></div>
                   <div class="space20"></div>
                   <div class="widget-body">
                       <div class="error-page" style="min-height: 500px">
                           <img src="<?php echo base_url()?>img/404.png" alt="404 error">
                           <h1>
                               <strong>404</strong> <br/>
                               Page Not Found
                           </h1>
                           <p>We're sorry, the page you were looking for doesn't exist anymore.</p>
                       </div>
                   </div>
               </div>
            </div>
          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
   
    <div id="footer">
    © Bản quyền 2015 thuộc về Admicro<br>
    
Tầng 20, Center Building Hapulico Complex, Số 1 Nguyễn Huy Tưởng, Thanh Xuân, Hà Nội        Điện thoại: (844) 7307 7979 | Fax: (844) 7307 7980          Email: contact@admicro.vn


        <div class="span pull-right"><span class="go-top"><i class="icon-arrow-up"></i></span></div>
    </div>
    <script src="<?php echo base_url()?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?php echo base_url()?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url()?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/jquery.blockui.js"></script>
    <script src="<?php echo base_url()?>js/jquery.cookie.js"></script>
    <!--[if lt IE 9]><script src="./js/excanvas.js"></script><script src="./js/respond.js"></script><![endif]-->
         <script src="<?php echo base_url()?>assets/jquery-knob/js/jquery.knob.js"></script>
    <script src="<?php echo base_url()?>assets/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>assets/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url()?>assets/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>assets/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>assets/flot/jquery.flot.crosshair.js"></script>
    <script src="<?php echo base_url()?>js/jquery.peity.min.js"></script>
<script src="<?php echo base_url()?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="http://thevectorlab.net/adminlab/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="http://thevectorlab.net/adminlab/assets/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>    
    <script src="<?php echo base_url()?>js/scripts.js"></script>

    <script type="text/javascript">
 
          </script>
              <script>
        jQuery(document).ready(function() {
            App.setMainPage(true);
            App.init();

        });

    </script>
</body>

</html>