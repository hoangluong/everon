<?php

function dq_auth_redirect() {
    if (!$user = is_user_logged_in()) {
        redirect(base_url('auth'));
        die;
    }
}

function is_logged_in($go_after_login = TRUE) {
    $CI = & get_instance();
    if ($CI->session->userdata('adm_widget_user_id')) {
        return true;
    }
    if ($go_after_login == TRUE) {
        redirect(base_url() . 'user/login');
    }

    return FALSE;
}

function is_exist_session() {
    $CI = & get_instance();
    if ($CI->session->userdata('adm_widget_user_id')) {
        return true;
    } else {
        return false;
    }
      
}

