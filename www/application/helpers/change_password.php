<div id="register_wrapper">
    <div id="register_heading">Đổi mật khẩu</div>
    <?php echo form_open(base_url() . 'user/change_password'); ?>
    <span class="warning"> <?php echo $this->session->flashdata('change_pass_success');?></span>
   
    <div class="field">
        <label for="old_password">Mật khẩu cũ</label>
        <input type="password" zise="20" id="old_password" name="old_password"/>
        <span class="error"><?php echo form_error('old_password'); ?></span>
       
    </div>
    <div class="field">
        <label for="new_password">Mật khẩu mới</label>
        <input type="password" zise="20" id="new_password" name="new_password"/>
        <span class="error"><?php echo form_error('new_password'); ?></span>
    </div>
    <div class="field">
        <label for="confpassword">Nhập lại mật khẩu </label>
        <input type="password" size="20" id="confpassword" name="confpassword" />
        <span class="error"><?php echo form_error('confpassword'); ?></span>
    </div>


    <input type="submit" name="create" value="Thay đổi" style="margin-top: 10px" name="submitRegister"/>

</form>

</div>

