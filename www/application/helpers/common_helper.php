<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 function font(){
   return  $arr = array('Arial,Helvetica,_sans', 'Verdana,Geneva,_sans','Lucida Sans Unicode,Lucida Grande,_sans', 'Times New Roman,Times,_serif');
 }
 function load_money_today(){
    $CI = & get_instance();
    $CI->load->model('customer/customer_model');
    return $CI->customer_model->load_money_today();
 }
 
 function load_money_yesterday(){
    $CI = & get_instance();
    $CI->load->model('customer/customer_model');
    return $CI->customer_model->load_money_by_day(date("Y-m-d",time() - 86400));    
 }
 
 function timelife(){
    $date_1 = strtotime(date("m/d/Y"));
    $date_2 = strtotime("02/10/2013");
    $date_range = $date_2 - $date_1;
     $date_range = ($date_range > 0) ? $date_range : 0;
    return $date_range/86400; 
 }
 
/**
 /**
 * @author dinh thuan
 * @copyright 2013
 */

function debug($value, $label = null) {
	$label = get_tracelog ( debug_backtrace (), $label );
	echo getdebug ( $value, $label );
	exit ();
}
function getdebug($value, $label = null) {
	$value = htmlentities ( print_r ( $value, true ) );
	return "<pre>$label$value</pre>";
}
function get_tracelog($trace, $label = null) {
	$line = $trace [0] ['line'];
	$file = is_set ( $trace [1] ['file'] );
	$func = $trace [1] ['function'];
	$class = is_set ( $trace [1] ['class'] );
	$log = "<span style='color:#FF3300'>-- $file - line:$line - $class-$func()</span><br/>";
	if ($label)
		$log .= "<span style='color:#FF99CC'>$label</span> ";
	return $log;
}
function is_set(&$var, $substitute = null) {
	return isset ( $var ) ? $var : $substitute;
}
function dump($value, $label = null) {
	$label = get_tracelog ( debug_backtrace (), $label );
	$value = htmlentities ( var_export ( $value, true ) );
	echo "<pre>$label$value</pre>";
}
 
?>
