<?php

function get_app($id) {
    global $the_qaunit_app;
    $app = get_post($id);
    $the_qaunit_app = $app;
    if ($app->post_type != 'qaunit' || $app->post_status == 'trash')
        return null;

    return $app;
}

function get_user($id) {
    $app = get_post($id);
    $user = get_userdata($app->post_author);
    //$the_user=$user;
    if ($user) {
        foreach ($user as $rs) {
            return $rs->user_email;
        }
    }
    return FALSE;
}

function the_app($app = 0) {
    global $the_qaunit_app;
    if ($app) {
        $the_qaunit_app = $app;
    }
    return $the_qaunit_app;
}

function set_app_data($app, $data = array()) {
    $app = wp_parse_args($app);
    if (!$data)
        $data = $_POST;
    $data = wp_parse_args($data);
    foreach ($data as $key => $value) {
        if ($key != 'ID') {
            $app[$key] = $value;
        }
    }
    if (!$app['post_status']) {
        $app['post_status'] = 'pending';
    }
    $app['post_type'] = 'qaunit';
    return $app;
}

function reset_app_data(&$data, $key, $original_app = '') {
    global $the_qaunit_app;
    if (!$original_app) {
        $original_app = $the_qaunit_app;
    }
    $original_app = wp_parse_args($original_app);
    $data[$key] = $original_app[$key];
}

function get_customer() {
    $args = array('role' => 'Subscriber');
    $list_cus = get_users($args);
    return $list_cus;
}

function get_name_user($id) {
    $user = get_userdata($id);
    return $user;
}

function get_all_apps_status($status = null) {
    $statuses = array(
            //'pending'=>'Pending',
            //'wait_for_paid'=>'Wait for paid',
            //'evaluation'=>'Evaluation',
            //'complete'=>'Completed'
    );
    if ($status === null) {
        $current_app = the_app();
        //print_r($current_app);die;
        if (!is_super_admin()) {
            $current_app_status = $current_app->post_status;
            $found = 0;
            foreach ($statuses as $st => $sv) {
                if ($st == $current_app_status) {
                    $found = 1;
                    //die;
                } else {
                    if (!$found) {
                        unset($statuses[$st]);
                    } else {
                        if ($found++ > 1) {
                            unset($statuses[$st]);
                        }
                    }
                }
            }
        }
    }
    if ($status == 'any') {
        return $statuses;
    }
    if ($status) {
        return $statuses[$status];
    }
    return $statuses;
}

function get_country() {
    $country_list = array(
        "Select", "Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"
    );

    return $country_list;
}

function get_widget_menu($role_id) {
    $CI = & get_instance();
    $CI->load->model('user/model_user');
    if ($role_id != 1) {
        return $CI->model_user->get_widgets_by_role($role_id);
    } else {
        return $CI->model_user->get_all_widgets_by_role();
    }
}

if (!function_exists('menu_anchor')) {

    function menu_anchor($uri = '', $title = '', $attributes = '') {
        $title = (string) $title;

        if (!is_array($uri)) {
            $site_url = (!preg_match('!^\w+://! i', $uri)) ? site_url($uri) : $uri;
        } else {
            $site_url = site_url($uri);
        }

        if ($title == '') {
            $title = $site_url;
        }

        if ($attributes != '') {
            $attributes = _parse_attributes($attributes);
        }

        $current_url = (!empty($_SERVER['HTTPS'])) ? "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] : "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $attributes .= ($site_url == $current_url) ? 'class="active"' : 'class="normal"';


        return '<a href="' . $site_url . '"' . $attributes . '>' . $title . '</a>';
    }

}

if (!function_exists('menu_anchor_img')) {

    function menu_anchor_img($uri = '', $title = '', $attributes = '', $img_path = '') {
        $title = (string) $title;

        if (!is_array($uri)) {
            $site_url = (!preg_match('!^\w+://! i', $uri)) ? site_url($uri) : $uri;
        } else {
            $site_url = site_url($uri);
        }

        if ($title == '') {
            $title = $site_url;
        }

        if ($attributes != '') {
            $attributes = _parse_attributes($attributes);
        }

        $current_url = (!empty($_SERVER['HTTPS'])) ? "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] : "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $attributes .= ($site_url == $current_url) ? 'class="active"' : 'class="normal"';


        return '<a href="' . $site_url . '"' . $attributes . '> <img src = "' . $img_path . '"/> <span>'.$title .'</span> </a>';
    }

}

