<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$lang['common_lang'] = array();
	$lang['common_lang']['hi'] = 'Hi';
	$lang['common_lang']['login'] = 'Login';
	$lang['common_lang']['logout'] = 'Logout';
	$lang['common_lang']['my_account'] = 'My Account';
	$lang['common_lang']['register'] = 'Register';
	$lang['common_lang']['lost_password'] = 'Lost password';
?>