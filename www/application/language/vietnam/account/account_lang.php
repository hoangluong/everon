<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['acount_lang'] = array();
	$lang['acount_lang']['email_subject_confirm_lostpassword']='Adnetwork: Confirm Reset password';
	$lang['acount_lang']['email_content_confirm_lostpassword']='Ban da kich hoat chuc nang khoi tao mat khau, hay kich vao lien ket ben duoi de xac nhan:<br/>
						<a href="%s">%s</a>';
	$lang['acount_lang']['email_not_available'] = 'Địa chỉ email của bạn không tồn tại trong hệ thống!';
	$lang['acount_lang']['auth_code_expire'] = 'Mã xác nhận đã hết hạn.';
	$lang['acount_lang']['auth_code_wrong'] = 'Mã xác nhận không đúng.';
	$lang['acount_lang']['acount_exists'] = 'Tên tài khoản đã tồn tại trên hệ thống!';
	$lang['acount_lang']['enter_username'] = 'Bạn chưa nhập Tên đăng nhập.';
	
	$lang['acount_lang']['lost_password'] = 'Quên mật khẩu';
	
	
	$lang['account_signup']['site_title'] = 'Tạo tài khoản – adnetwork.admicro.vn';
	$lang['account_signup']['site_meta_title'] = '';
	$lang['account_signup']['site_meta_keyword'] = '';
	$lang['account_signup']['site_meta_desc'] = 'Đăng ký, tạo tài khoản, điền đầy đủ thông tin liên hệ như:Số điện thoại, Email, Địa chỉ.';
	
	$lang['account_signup']['registry_account'] = 'Đăng ký tài khoản';
	$lang['account_signup']['enter_username'] = 'Bạn hãy nhập Tên đăng nhập';
	$lang['account_signup']['username_wrong_length'] = 'Tên đăng nhập phải trên 4 và nhỏ hơn 64 ký tự';
	$lang['account_signup']['username_contains_illegal'] = 'Tên đăng nhập chứa ký tự không hợp lệ';
	$lang['account_signup']['not_available'] = 'Tên đăng nhập đã tồn tại trên hệ thống.';
	$lang['account_signup']['wrong_captcha'] = 'Mã chống Spam không trùng khớp.';
	$lang['account_signup']['enter_password'] = 'Bạn hãy nhập Mật khẩu.';
	$lang['account_signup']['password_not_match'] = 'Mật khẩu không trùng khớp, bạn hãy thử lại.';
	$lang['account_signup']['password_wrong_length'] = 'Mật khẩu quá ngắn, phải trên 4 và dưới 64 ký tự.';
	$lang['account_signup']['enter_email'] = 'Bạn hãy nhập Email.';
	$lang['account_signup']['email_invalid'] = 'Địa chỉ Email không hợp lệ.';
	$lang['account_signup']['email_not_available'] = 'Địa chỉ Email đã tồn tại trên hệ thống.';
	$lang['account_signup']['enter_fullname'] = 'Bạn hãy nhập Họ tên.';
	$lang['account_signup']['account_not_active'] = 'Tài khoản chưa được kích hoạt, bạn phải kích hoạt tài khoản để sử dụng chức năng này.';
	
	$lang['account_signup']['email_subject1'] = 'Đăng ký thành viên';
	$lang['account_signup']['email_content1'] = 'Chào %s,<br /><br />
			Bạn vừa đăng ký tài khoản trong hệ thống Adnetwork. Để hoàn thành việc đăng ký, bạn hãy click vào đường link dưới đây, chú ý link chỉ tồn tại trong vòng 24 giờ kể từ khi bạn nhận được email này.
			<br /><br />%s<br /><br />
			Hoặc bạn copy đường dẫn dưới đây, và sử dụng mã %s để hoàn thành việc đăng ký:
			<br />%s<br /><br />
			Hệ thống Adnetwork sẽ giúp bạn quảng cáo trúng đích với chi phí tốt nhất. Khi bạn gia nhập vào hệ thống Adnetwork, bạn có thể quảng cáo sản phẩm của bạn đến hơn 25 triệu độc giả.<br /><br />
			Cám ơn bạn,<br /><br />
			Hệ thống quảng cáo Adnetwork – AdMicro, công ty cổ phần truyền thông Việt Nam.
	';
	$lang['account_signup']['email_subject2'] = 'Hoàn thành đăng ký';
	$lang['account_signup']['email_content2'] = 'Chào %s,<br /><br />
			Bạn đã hoàn thành việc đăng ký tài khoản trong hệ thống Adnetwork. Bạn hãy tham gia và quảng cáo sản phẩm của bạn ngay hôm nay.
			<br />Để đăng nhập vào hệ thống, bạn hãy click vào liên kết: <a href="'.site_url('login').'">'.site_url('login').'</a>
			<br /><br />
			Cám ơn bạn,
			<br /><br />
			Hệ thống quảng cáo Adnetwork – AdMicro, công ty cổ phần truyền thông Việt Nam.
	';
	$lang['account_signup']['email_subject3'] = 'Quên mật khẩu';
	$lang['account_signup']['email_content3'] = 'Chào %s,<br /><br />
			Bạn vừa yêu cầu reset lại mật khẩu. Để reset mật khẩu của bạn, click theo link dưới đây, chú ý link này chỉ tồn tại 15 phút kể từ lúc bạn nhận được thư này.
			<br /><br />%s<br /><br />
			(Nếu bạn click vào link không được, hãy thử copy và dán vào trình duyệt của bạn)
			<br /><br />
			Nếu bạn không có yêu cầu reset lại mật khẩu, hãy bỏ qua email này.
			<br /><br />
			Hệ thống quảng cáo Adnetwork – AdMicro, công ty cổ phần truyền thông Việt Nam.
	';
	$lang['account_signup']['email_subject4'] = 'Mật khẩu mới';
	$lang['account_signup']['email_content4'] = 'Chào %s,<br /><br />
			Mật khẩu của bạn đã được khởi tạo, bạn hãy sử dụng mật khẩu mới dưới đây để đăng nhập:
			<br /><br />Tên đăng nhập: <b>%s</b><br />Mật khẩu: <b>%s</b><br /><br />
			Để đăng nhập hệ thống, bạn hãy kích vào liên kết: <a href="'.site_url('login').'">'.site_url('login').'</a>
			<br /><br />
			Hệ thống quảng cáo Adnetwork – AdMicro, công ty cổ phần truyền thông Việt Nam.
	';
	$lang['account_signup']['active_success'] = 'Bạn đã kích hoạt tài khoản thành công! Nhấn vào <a href="%s"><b>đây</b></a> để đăng nhập.';
	$lang['account_signup']['code_not_wrong'] = 'Mã kích hoạt không đúng, bạn hãy thử lại!';
	$lang['account_signup']['code_expire'] = 'Mã kích hoạt đã hết hạn! (Để lấy lại mã kích hoạt, bạn hãy nhấn vào <a href="%s"><b>đây</b></a>)';
	$lang['account_signup']['enter_code_auth'] = 'Bạn hãy nhập mã kích hoạt vào ô bên dưới để kích hoạt tài khoản!';
	$lang['account_signup']['account_actived'] = 'Tài khoản của bạn đã được kích hoạt! Nhấn vào <a href="%s"><b>đây</b></a> để đăng nhập.';
	$lang['account_signup']['resend_active_expire'] = '"Mã kích hoạt của bạn đã được tạo thành công, một thư điện tử xác nhận đã được gửi đến địa chỉ email của bạn. Bạn hãy kiểm tra lại hòm thư và làm theo hướng dẫn để kích hoạt tài khoản. Bấm vào <a href="%s" class="orange">đây</a> để đăng nhập vào hệ thống."';
	// estimate box
	//Signup
	$lang['account_signup']['signup_success'] = '"Bạn đã tạo tài khoản thành công, một thư điện tử xác nhận đã được gửi đến địa chỉ email của bạn. Bạn hãy kiểm tra lại hòm thư và làm theo hướng dẫn để kích hoạt tài khoản. Bấm vào <a href="%s" class="orange">đây</a> để đăng nhập vào hệ thống."';
	$lang['account_signup']['home'] = 'Trang chủ';
	$lang['account_signup']['create_account'] = 'Tạo tài khoản';
	$lang['account_signup']['username'] = 'Tên đăng nhập';
	$lang['account_signup']['password'] = 'Mật khẩu';
	$lang['account_signup']['old_password'] = 'Mật khẩu cũ';
	$lang['account_signup']['new_password'] = 'Mật khẩu mới';
	$lang['account_signup']['repassword'] = 'Gõ lại mật khẩu';
	$lang['account_signup']['renewpassword'] = 'Gõ lại mật khẩu mới';
	$lang['account_signup']['enter_old_password'] = 'Bạn hãy nhập Mật khẩu cũ';
	$lang['account_signup']['old_password_wrong'] = 'Mật khẩu cũ không đúng';
	$lang['account_signup']['fullname'] = 'Họ và tên';
	$lang['account_signup']['email'] = 'Email';
	$lang['account_signup']['number_phone'] = 'Số điện thoại';
	$lang['account_signup']['address'] = 'Địa chỉ';
	$lang['account_signup']['anti_spam'] = 'Mã chống Spam';
	$lang['account_signup']['refesh_spam'] = '[Tải lại]';
	$lang['account_signup']['register'] = 'Đăng ký';
	$lang['account_signup']['account_info'] = 'Thông tin tài khoản';
	$lang['account_signup']['account_info_updated'] = 'Thông tin tài khoản đã được cập nhật.';
	$lang['account_signup']['mail_setting'] = 'Thiết lập mail';
	$lang['account_signup']['change_pass'] = 'ĐỔI MẬT KHẨU';
	$lang['account_signup']['new_pass'] = 'Mật khẩu mới';
	$lang['account_signup']['re_pass'] = 'Gõ lại Mật khẩu';
	$lang['account_signup']['title_account_info'] = 'THÔNG TIN TÀI KHOẢN';
	$lang['account_signup']['active_account'] = 'Kích hoạt tài khoản';
	$lang['account_signup']['code_auth'] = 'Mã xác nhận';
	$lang['account_signup']['confirm'] = 'Xác nhận';
	$lang['account_signup']['forgot_pass'] = 'Quên mật khẩu';
	$lang['account_signup']['retore'] = 'Khôi phục';
	$lang['account_signup']['forgot_confirm'] = '"Mã xác nhận của bạn đã được gửi vào địa chỉ Email. Bạn hãy kiểm tra lại hòm thư và làm theo hướng dẫn để xác nhận khôi phục lại mật khẩu. Bấm vào <a href="%s" class="orange">đây</a> để đăng nhập vào hệ thống."';
	$lang['account_signup']['forgot_success'] = '"Mật khẩu mới của bạn đã được gửi vào địa chỉ Email. Bạn hãy kiểm tra lại hòm thư và làm theo hướng dẫn để khôi phục lại mật khẩu. Bấm vào <a href="%s" class="orange">đây</a> để đăng nhập vào hệ thống."';
	
	$lang['account_signup']['update_btn'] 			= 'Cập nhật';
	$lang['account_signup']['update_ok'] 			= 'Cập nhập thành công.';
	
	$lang['account_lang']['account_info_title'] 	= 'Thông tin cá nhân';
	$lang['account_signup']['ready_term'] = 'Tôi đã đọc, và đồng ý tuân theo các <a href="'.site_url('policy').'" target="_blank">điều khoản</a> của Adnetwork';
	$lang['account_signup']['enter_term'] = 'Bạn chưa chấp nhận các Điều khoản, nên việc đăng ký không thể tiếp tục';
	
	// inbox
	$lang['account_lang']['inbox_title'] 			= 'Thông báo';
	$lang['account_lang']['ib_send_to'] 			= 'Gửi tới';
	$lang['account_lang']['ib_sender'] 				= 'Người gửi';
	$lang['account_lang']['ib_send_btn'] 			= 'Gửi';
	$lang['account_lang']['ib_title'] 				= 'Tiêu đề';
	$lang['account_lang']['ib_content'] 			= 'Nội dung';
	$lang['account_lang']['ib_send_time'] 			= 'Thời gian gửi';
	$lang['account_lang']['ib_delete_btn'] 			= 'Xóa';
	$lang['account_lang']['ib_page'] 				= 'Trang';
	$lang['account_lang']['close_btn'] 				= 'Đóng';
	// end for inbox	
?>