<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['agencies_lang'] = array();
    $lang['agencies_lang']['filter'] = 'Lọc theo';
    $lang['agencies_lang']['privilege'] = 'Nhóm';
    $lang['agencies_lang']['search'] = 'Tìm kiếm';
    $lang['agencies_lang']['createUser'] = 'Tạo người dùng';
    $lang['agencies_lang']['lstUser'] = 'Danh sách người dùng';
    $lang['agencies_lang']['#'] = 'STT';
    $lang['agencies_lang']['username'] = 'Tên người dùng';
    $lang['agencies_lang']['fullname'] = 'Tên đầy đủ';
    $lang['agencies_lang']['level'] = 'Cấp bậc';
    $lang['agencies_lang']['action'] = 'Thao tác';
    //select option
    $lang['agencies_lang']['all'] = 'Tất cả';
    $lang['agencies_lang']['agency'] = 'Quản lý';
    $lang['agencies_lang']['publisher'] = 'Người duyệt';
    $lang['agencies_lang']['user'] = 'Người dùng';
    $lang['agencies_lang']['viewonly'] = 'Người xem';
    
    $lang['agencies_lang']['active'] = 'Đã kích hoạt';
    $lang['agencies_lang']['unactive'] = 'Chưa kích hoạt';
    $lang['agencies_lang']['delete'] = 'Đã xóa';
    $lang['agencies_lang']['block'] = 'Bị khóa';
    $lang['agencies_lang']['unblock'] = 'Bỏ khóa';
    $lang['agencies_lang']['undelete'] = 'Bỏ xóa';
    //create
    $lang['agencies_lang']['create_user'] = 'Tạo mới tài khoản';
    $lang['agencies_lang']['password'] = 'Mật khẩu';
    $lang['agencies_lang']['re_password'] = 'Gõ lại Mật khẩu';
    $lang['agencies_lang']['email'] = 'Email';
    $lang['agencies_lang']['phone'] = 'Điện thoại';
    $lang['agencies_lang']['address'] = 'Địa chỉ';
    $lang['agencies_lang']['role'] = 'Cấp bậc';
    $lang['agencies_lang']['group'] = 'Group';
    $lang['agencies_lang']['status'] = 'Trạng thái';
    $lang['agencies_lang']['running'] = 'Đang chạy';
    $lang['agencies_lang']['out_off_money'] = 'Chưa có tiền';
    $lang['agencies_lang']['create'] = 'Tạo mới';
    $lang['agencies_lang']['enter_username'] = 'Hãy nhập Tên đăng nhập';
    $lang['agencies_lang']['username_existst'] = 'Tên đăng nhập đã tồn tại';
    $lang['agencies_lang']['enter_password'] = 'Hãy nhập Mật khẩu';
    $lang['agencies_lang']['enter_repassword'] = 'Hãy nhập Gõ lại Mật khẩu';
    $lang['agencies_lang']['password_not_match'] = 'Mật khẩu và Gõ lại Mật khẩu không trùng khớp';
    $lang['agencies_lang']['enter_email'] = 'Hãy nhập địa chỉ Email';
    $lang['agencies_lang']['email_invalid'] = 'Địa chỉ Email không hợp lệ';
    $lang['agencies_lang']['email_exists'] = 'Địa chỉ Email đã tồn tại';
    $lang['agencies_lang']['enter_fullname'] = 'Hãy nhập Họ và Tên';
    $lang['agencies_lang']['enter_phone'] = 'Hãy nhập Số điện thoại';
	$lang['agencies_lang']['phone_invalid'] = 'Số điện thoại phải là dạng số';
    $lang['agencies_lang']['module_title'] = 'User manager';
    //edit
    $lang['agencies_lang']['edit_user'] = 'Cập nhật tài khoản';
    $lang['agencies_lang']['change_password'] = 'THAY ĐỔI MẬT KHẨU';
    $lang['agencies_lang']['info_user'] = 'THÔNG TIN TÀI KHOẢN';
    $lang['agencies_lang']['state'] = 'TRẠNG THÁI';
    $lang['agencies_lang']['edit'] = 'Cập nhật';
    $lang['agencies_lang']['submit'] = 'Cập nhật';
    $lang['agencies_lang']['page'] = 'Trang';
    $lang['agencies_lang']['lstCampaign'] = 'Danh sách chiến dịch';
    // agencies
    $lang['agencies_lang']['user_manager'] 			= 'Quản lý người dùng';
    $lang['agencies_lang']['home'] 					= 'Trang chủ';
    $lang['agencies_lang']['level'] 				= 'Cấp bậc';
    //action
    $lang['agencies_lang']['Block'] = 'Khóa';
    $lang['agencies_lang']['Delete'] = 'Xóa';
    $lang['agencies_lang']['actived'] = 'Kích hoạt';
    $lang['agencies_lang']['setpublisher'] = 'Chuyển thành người duyệt';
    $lang['agencies_lang']['setuser'] = 'Chuyển thành người dùng';
    $lang['agencies_lang']['setviewonly'] = 'Chuyển thành người xem';
    $lang['agencies_lang']['setcampaign'] = 'Gán chiến dịch';
    $lang['agencies_lang']['assign'] = 'Gán';
    $lang['agencies_lang']['cancelTooltip'] = 'Đóng';
    $lang['agencies_lang']['username_nonevn'] = 'Tên đăng nhập không hợp lệ';
    //Tiennd add
    $lang['agencies_lang']['move_money'] = 'Chuyển tiền';
    $lang['agencies_lang']['date'] = 'Ngày';
    $lang['agencies_lang']['sub_money'] = 'Số tiền trừ';
    $lang['agencies_lang']['sum'] = 'Tổng';
    $lang['agencies_lang']['report_budget_use_agency'] = 'Báo cáo tiền đã dùng';
    $lang['agencies_lang']['arr_status'] = array('0'=>'Chờ/Lỗi', '1'=>'Thành công');
    //Move money
	$lang['agencies_lang']['recieve_account'] = 'Tài khoản nhận';
	$lang['agencies_lang']['select_user'] = 'Hãy chọn một tài khoản nhận tiền.';
	$lang['agencies_lang']['cancel'] = 'Hủy bỏ';
	$lang['agencies_lang']['option'] = 'Tùy chọn';
	$lang['agencies_lang']['amount'] = 'Số tiền';
	$lang['agencies_lang']['enter_amount'] = 'Hãy nhập Số tiền cần chuyển.';
	$lang['agencies_lang']['type_balance'] = 'Loại tiền';
	$lang['agencies_lang']['money_balance'] = 'Tiền tài khoản chính';
	$lang['agencies_lang']['money_promotion'] = 'Tiền tài khoản khuyến mại';
	$lang['agencies_lang']['move_money_not_success'] = 'Không có quyền hoặc không đủ tiền để chuyển. Chuyển tiền không thành công!';
	$lang['agencies_lang']['move_money_success'] = 'Yêu cầu chuyển tiền đã thành công!';
	$lang['agencies_lang']['email_content_move_money'] = 'Chào [username],<br /><br />
			Bạn vừa được chuyển [amount]đ vào tài khoản [balance_type] qua kênh đại lý trong hệ thống AdMarket. Bạn có thể sử dụng số tiền đã có để mua các quảng cáo trên hệ thống.
			<br /><br />
			Để xem chi tiết thông tin tài khoản, bạn hãy kích vào liên kết dưới đây để đăng nhập hệ thống:<br />[login]		
			<br /><br />
			Hệ thống AdMarket sẽ giúp bạn quảng cáo trúng đích với chi phí tốt nhất. Khi bạn gia nhập vào hệ thống AdMarket, bạn có thể quảng cáo sản phẩm của bạn đến hơn 25 triệu độc giả.<br /><br />
			Cám ơn bạn,<br /><br />
			Hệ thống quảng cáo AdMarket, phòng Adtech – AdMicro, công ty cổ phần truyền thông Việt Nam.
	';
	$lang['agencies_lang']['label_balance'] = 'Tài khoản chính';
	$lang['agencies_lang']['label_promotion'] = 'Tài khoản khuyến mại';
	$lang['agencies_lang']['user_manager'] = 'Quản lý Khách hàng';
	$lang['agencies_lang']['account'] = 'Tài khoản';
	$lang['agencies_lang']['content'] = 'Nội dung';
	$lang['agencies_lang']['email_subject_pay'] = 'Cập nhật tài khoản';
	$lang['agencies_lang']['transactions'] = 'Các giao dịch';
//End
	$lang['agencies_lang']['customer'] = 'Khách hàng';
	$lang['agencies_lang']['agency_sale'] = 'Sale';
	$lang['agencies_lang']['is_sale'] = 'Là Sale';
	$lang['agencies_lang']['select'] = '- Lựa chọn -';
	$lang['agencies_lang']['customer_info'] = 'Quản lý khách hàng';
	$lang['agencies_lang']['convert_to_sale'] = 'Chuyển thành Sale';
    //End Tiennd