<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['archive_lang'] = array();
    $lang['archive_lang']['archive'] = 'Lưu trữ quảng cáo';
	$lang['archive_lang']['creatAd'] = 'Tạo quảng cáo';
	$lang['archive_lang']['notice'] = 'Thông báo';
	$lang['archive_lang']['ad'] = 'Quảng cáo';
    $lang['archive_lang']['cam'] = 'Chiến dịch';
	$lang['archive_lang']['finance'] = 'Ngân sách';
    $lang['archive_lang']['searchbanner'] = 'Tìm quảng cáo';
    $lang['archive_lang']['searchcampaign'] = 'Tìm chiến dịch';
	
	$lang['archive_lang']['to'] = 'đến';
	$lang['archive_lang']['#'] = 'Stt';
	$lang['archive_lang']['preview'] = 'Xem trước';
	
    $lang['archive_lang']['status'] = 'Trạng thái';
    $lang['archive_lang']['agencies'] = 'Thông tin đại lý';
    $lang['archive_lang']['export'] = 'Xuất file(.xls)';
    $lang['archive_lang']['date'] = 'Ngày';
    $lang['archive_lang']['north'] = 'Miền Bắc';
    $lang['archive_lang']['middle'] = 'Miền Trung';
    $lang['archive_lang']['south'] = 'Miền Nam';
    $lang['archive_lang']['other'] = 'Vùng khác';
    $lang['archive_lang']['lifeTime'] = 'Thời gian chạy';
    $lang['archive_lang']['chooseAGraph'] = 'Chọn đồ thị';
    $lang['archive_lang']['adsLocation'] = 'Vị trí quảng cáo';
    $lang['archive_lang']['all'] = 'Tất cả';
    $lang['archive_lang']['preview'] = 'Xem trước';
    $lang['archive_lang']['chart'] = 'Biểu đồ';
    $lang['archive_lang']['Error'] = 'Phải chọn ít nhất 1 giá trị ở mỗi mục';
    $lang['archive_lang']['ok'] = 'Sửa';
    $lang['archive_lang']['cancel'] = 'Từ chối';
    $lang['archive_lang']['tryAgain'] = 'Có lỗi xảy ra,xin thử lại!';
    $lang['archive_lang']['run'] = 'Chạy';
    //status
    $lang['archive_lang']['completed'] = 'Hoàn thành';
    $lang['archive_lang']['running'] = 'Đang chạy';
    $lang['archive_lang']['paused'] = 'Tạm dừng';
    $lang['archive_lang']['waiting'] = 'Chờ duyệt';
    $lang['archive_lang']['problem'] = 'Có lỗi';
    $lang['archive_lang']['deleted'] = 'Xóa';
    $lang['archive_lang']['outofmoney'] = 'Hết ngân sách ngày';
    $lang['archive_lang']['waittorun'] = 'Chờ chạy';
    $lang['archive_lang']['temp_deleted'] = 'Lưu trữ';
    //table1
    $lang['archive_lang']['acc'] = 'Tài khoản';
    $lang['archive_lang']['promotions'] = 'Khuyến mãi';
    $lang['archive_lang']['overdraft'] = 'Thấu chi';
    $lang['archive_lang']['today'] = 'Hôm nay';
    $lang['archive_lang']['yesterday'] = 'Hôm qua';
    $lang['archive_lang']['have'] = 'Có';
    $lang['archive_lang']['ad_running']='quảng cáo đang chạy';
    $lang['archive_lang']['ad_completed']='quảng cáo đã hoàn thành';
    $lang['archive_lang']['cam_completed']='chiến dịch đã hoàn thành';
    $lang['archive_lang']['page']='Trang ';
    $lang['archive_lang']['dateview']='Ngày xem';
    //tooltip
    $lang['archive_lang']['tooltip_title_tk']='Tài khoản';
    $lang['archive_lang']['tooltip_content_tk']='Số tiền khả dụng trong tài khoản của bạn. Để tăng tài khoản, bạn hãy sử dụng chức năng ngân quỹ để nạp tiền';
    
    $lang['archive_lang']['tooltip_title_tc']='Số tiền thấu chi';
    $lang['archive_lang']['tooltip_content_tc']='Là số tiền bạn có thể tiêu thêm khi tài khoản chính của bạn hết tiền. Số tiền bạn đã tiêu trong tài khoản thấu chi sẽ được trừ vào tài khoản chính trong lần nạp tiền tới của bạn.';
    
    $lang['archive_lang']['tooltip_title_click']='Tăng click quảng cáo.';
    $lang['archive_lang']['tooltip_content_click']='Giúp bạn tăng lượng click cho banner. Khi bạn thay đổi tốc độ, nhanh hơn hay chậm hơn ví dụ 20% so với tốc độ mặc định, lượng click vào quảng cáo của bạn sẽ tăng hoặc giảm tương ứng giúp quảng cáo của bạn thực hiện hiệu quả hơn.';
    
    $lang['archive_lang']['ADpreview'] = 'Xem trước quảng cáo';
    $lang['archive_lang']['targeting'] = 'Đối tượng quảng cáo';
    $lang['archive_lang']['editThisAd'] = 'Sửa quảng cáo này';
    $lang['archive_lang']['editThisAdTip'] = 'Bạn có thể sửa tên, bid hay trạng thái của quảng cáo này bằng việc click vào các giá trị trong bảng của trang này.';
    $lang['archive_lang']['editThisAdTip2'] = 'Click vào nút “Chỉnh sửa” để xem quảng cáo chi tiết hơn.';
    
    $lang['archive_lang']['targetUser'] = 'Đối tượng quảng cáo :';
    $lang['archive_lang']['SearchBanner'] = 'Tìm quảng cáo';
    
    $lang['archive_lang']['editTooltip'] = 'Chỉnh sửa';
    $lang['archive_lang']['cancelTooltip'] = 'Đóng lại';
    $lang['archive_lang']['impressions'] = 'Lượt xem';
    $lang['archive_lang']['uv'] = 'Người xem';
    $lang['archive_lang']['copyad'] = 'Tạo quảng cáo tương tự';
    $lang['archive_lang']['restoreAd'] = 'Khôi phục quảng cáo';
    $lang['archive_lang']['restore'] = 'Khôi phục';
    $lang['archive_lang']['restoreConfirm'] = 'Bạn có chắc chắn muốn khôi phục ko???';
	$lang['archive_lang']['recharge'] = 'Xem inventory';