<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$lang['banner_lang'] = array();
	$lang['banner_lang']['AllBanner'] = 'Tất cả quảng cáo';
	$lang['banner_lang']['creatAd'] = 'Tạo quảng cáo';
	$lang['banner_lang']['notice'] = 'Thông báo';
	$lang['banner_lang']['ad'] = 'Quảng cáo';
    $lang['banner_lang']['cam'] = 'Chiến dịch';
	$lang['banner_lang']['finance'] = 'Ngân sách';
    $lang['banner_lang']['searchbanner'] = 'Tìm quảng cáo';
    $lang['banner_lang']['searchcampaign'] = 'Tìm chiến dịch';
	
	$lang['banner_lang']['to'] = 'đến';
	$lang['banner_lang']['#'] = 'Stt';
	$lang['banner_lang']['preview'] = 'Xem trước';
	
    $lang['banner_lang']['status'] = 'Trạng thái';
    $lang['banner_lang']['agencies'] = 'Thông tin đại lý';
    $lang['banner_lang']['export'] = 'Xuất file(.xls)';
    $lang['banner_lang']['date'] = 'Ngày';
    $lang['banner_lang']['north'] = 'Miền Bắc';
    $lang['banner_lang']['middle'] = 'Miền Trung';
    $lang['banner_lang']['south'] = 'Miền Nam';
    $lang['banner_lang']['location'] = 'Vùng Miền';
    $lang['banner_lang']['other'] = 'Vùng khác';
    $lang['banner_lang']['lifeTime'] = 'Thời gian chạy';
    $lang['banner_lang']['chooseAGraph'] = 'Chọn đồ thị';
    $lang['banner_lang']['adsLocation'] = 'Vị trí quảng cáo';
    $lang['banner_lang']['all'] = 'Tất cả';
    $lang['banner_lang']['preview'] = 'Xem trước';
    $lang['banner_lang']['chart'] = 'Biểu đồ';
    $lang['banner_lang']['Error'] = 'Phải chọn ít nhất 1 giá trị ở mỗi mục';
    $lang['banner_lang']['ok'] = 'Sửa';
    $lang['banner_lang']['cancel'] = 'Từ chối';
    $lang['banner_lang']['tryAgain'] = 'Có lỗi xảy ra,xin thử lại!';
    $lang['banner_lang']['run'] = 'Chạy';
    //status
    $lang['banner_lang']['completed'] = 'Hoàn thành';
    $lang['banner_lang']['running'] = 'Đang chạy';
    $lang['banner_lang']['paused'] = 'Tạm dừng';
    $lang['banner_lang']['waiting'] = 'Chờ duyệt';
    $lang['banner_lang']['problem'] = 'Có lỗi';
    $lang['banner_lang']['deleted'] = 'Xóa';
    $lang['banner_lang']['outofmoney'] = 'Hết ngân sách ngày';
    $lang['banner_lang']['waittorun'] = 'Chờ chạy';
    //table1
    $lang['banner_lang']['acc'] = 'Tài khoản';
    $lang['banner_lang']['promotions'] = 'Khuyến mãi';
    $lang['banner_lang']['overdraft'] = 'Thấu chi';
    $lang['banner_lang']['today'] = 'Hôm nay';
    $lang['banner_lang']['yesterday'] = 'Hôm qua';
    $lang['banner_lang']['have'] = 'Có';
    $lang['banner_lang']['ad_running']='quảng cáo đang chạy';
    $lang['banner_lang']['ad_completed']='quảng cáo đã hoàn thành';
    $lang['banner_lang']['cam_completed']='chiến dịch đã hoàn thành';
    $lang['banner_lang']['page']='Trang ';
    $lang['banner_lang']['dateview']='Ngày xem';
    //tooltip
    $lang['banner_lang']['tooltip_title_tk']='Tài khoản';
    $lang['banner_lang']['tooltip_content_tk']='Số tiền khả dụng trong tài khoản của bạn. Để tăng tài khoản, bạn hãy sử dụng chức năng ngân quỹ để nạp tiền';
    
    $lang['banner_lang']['tooltip_title_tc']='Số tiền thấu chi';
    $lang['banner_lang']['tooltip_content_tc']='Là số tiền bạn có thể tiêu thêm khi tài khoản chính của bạn hết tiền. Số tiền bạn đã tiêu trong tài khoản thấu chi sẽ được trừ vào tài khoản chính trong lần nạp tiền tới của bạn.';
    
    $lang['banner_lang']['tooltip_title_click']='Tăng click quảng cáo.';
    $lang['banner_lang']['tooltip_content_click']='Giúp bạn tăng lượng click cho banner. Khi bạn thay đổi tốc độ, nhanh hơn hay chậm hơn ví dụ 20% so với tốc độ mặc định, lượng click vào quảng cáo của bạn sẽ tăng hoặc giảm tương ứng giúp quảng cáo của bạn thực hiện hiệu quả hơn.';
    
    $lang['banner_lang']['ADpreview'] = 'Xem trước quảng cáo';
    $lang['banner_lang']['targeting'] = 'Đối tượng quảng cáo';
    $lang['banner_lang']['editThisAd'] = 'Sửa quảng cáo này';
    $lang['banner_lang']['editThisAdTip'] = 'Bạn có thể sửa tên, bid hay trạng thái của quảng cáo này bằng việc click vào các giá trị trong bảng của trang này.';
    $lang['banner_lang']['editThisAdTip2'] = 'Click vào nút “Chỉnh sửa” để xem quảng cáo chi tiết hơn.';
    
    $lang['banner_lang']['targetUser'] = 'Đối tượng quảng cáo :';
    $lang['banner_lang']['SearchBanner'] = 'Tìm quảng cáo';
    
    $lang['banner_lang']['editTooltip'] = 'Chỉnh sửa';
    $lang['banner_lang']['cancelTooltip'] = 'Đóng lại';
    $lang['banner_lang']['impressions'] = 'Lượt xem';
    $lang['banner_lang']['uv'] = 'Người xem';
    $lang['banner_lang']['copyad'] = 'Tạo quảng cáo tương tự';
    $lang['banner_lang']['temp_deleted'] = 'Lưu trữ quảng cáo';
    $lang['banner_lang']['temp_delete'] = 'Lưu trữ';
    $lang['banner_lang']['notice_tittle'] = 'Chiến dịch của bạn đã chạy được một thời gian nhưng chưa đạt được hiệu quả click như bạn mong muốn. Để tăng thêm hiệu quả click, bạn hãy thử những cách sau:';
    $lang['banner_lang']['notice_content'] = '<ol>
    <li>Chọn mục tăng click trong ngày: giúp quảng cáo của bạn hiển thị nhiều hơn và tăng lượng click</li>
    <li>Chọn thêm khu vực hiển thị quảng cáo.Ví dụ:Quảng cáo của bạn không chỉ hiển thị ở Hà Nội, Hải Phòng mà có thể toàn miền bắc.</li>
    <li>Chọn thêm các Website: chúng tôi mang quảng cáo của bạn đến rất nhiều Website lớn như: dantri.com.vn, kenh14.vn, muachung.vn,…</li>
    <li>Nếu bạn cần giải đáp,<a href='.site_url('contact').'>liên hệ</a> ngay với chúng tôi.</li><ol>';
    $lang['banner_lang']['deleteConfirm'] = 'Bạn có thực sự muốn lưu trữ không???';
    $lang['banner_lang']['cam_endding']='chiến dịch sắp kết thúc';
	$lang['banner_lang']['recharge']='Xem inventory';
	
	$lang['banner_lang']['destroy']='Hủy';
?>