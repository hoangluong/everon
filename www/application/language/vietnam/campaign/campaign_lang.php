<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['campaign_lang'] = array();
	$lang['campaign_lang']['all_campaign'] = 'Tất cả chiến dịch';
    $lang['campaign_lang']['creatAd'] = 'Tạo quảng cáo';
    $lang['campaign_lang']['to'] = 'đến';
    $lang['campaign_lang']['running'] = 'Đang chạy';
    $lang['campaign_lang']['completed'] = 'Hoàn thành';
    $lang['campaign_lang']['all'] = 'Tất cả';
    $lang['campaign_lang']['paused'] = 'Tạm dừng';
    $lang['campaign_lang']['wait'] = 'Chờ duyệt';
    //main
    $lang['campaign_lang']['campaign'] = 'Chiến dịch';
    $lang['campaign_lang']['status'] = 'Trạng thái';
    $lang['campaign_lang']['budget'] = 'Ngân sách';
    $lang['campaign_lang']['byDate'] = 'theo ngày';
    $lang['campaign_lang']['byCam'] = 'theo chiến dịch';
    $lang['campaign_lang']['#'] = 'Stt';
    $lang['campaign_lang']['spent'] = 'Số tiền sử dụng (VND)';
    $lang['campaign_lang']['dateview'] = 'Ngày xem';
    $lang['campaign_lang']['page'] = 'Trang : ';
    $lang['campaign_lang']['username'] = 'Người dùng';
    //status
    $lang['campaign_lang']['run'] = 'Chạy';
    $lang['campaign_lang']['paused'] = 'Tạm dừng';
    $lang['campaign_lang']['deleted'] = 'Lưu trữ';
    $lang['campaign_lang']['delete'] = 'Đã lưu trữ';
    $lang['campaign_lang']['outofmoney'] = 'Hết ngân sách ngày';
    $lang['campaign_lang']['waittorun'] = 'Chờ chạy';
    //tooltip
    $lang['campaign_lang']['cancelTooltip'] = 'Thoát';
    $lang['campaign_lang']['impressions'] = 'Lượt xem';
    $lang['campaign_lang']['uv'] = 'Người xem';
    $lang['campaign_lang']['searchcampaign'] = 'Tìm chiến dịch';
    $lang['campaign_lang']['total'] = 'Tổng cộng';
	$lang['campaign_lang']['recharge'] = 'Xem inventory';
?>