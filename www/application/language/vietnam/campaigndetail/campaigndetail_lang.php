<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['campaigndetail_lang'] = array();
    $lang['campaigndetail_lang']['creatAd'] = 'Tạo quảng cáo';
    $lang['campaigndetail_lang']['campaign'] = 'Chiến dịch';
    $lang['campaigndetail_lang']['campaigndetail'] = 'Chi tiết chiến dịch';
    $lang['campaigndetail_lang']['AllCampaign'] = 'Tất cả chiến dịch';
    $lang['campaigndetail_lang']['to'] = 'đến';
    //main
    $lang['campaigndetail_lang']['#'] = 'Stt';
    $lang['campaigndetail_lang']['preview'] = 'Xem trước';
    $lang['campaigndetail_lang']['ad'] = 'Quảng cáo';
    $lang['campaigndetail_lang']['status'] = 'Trạng thái';
    $lang['campaigndetail_lang']['spent'] = 'Số tiền sử dụng ';
    $lang['campaigndetail_lang']['total'] = 'Tổng cộng';
    $lang['campaigndetail_lang']['dateview'] = 'Ngày xem';
    $lang['campaigndetail_lang']['chart'] = 'Biểu đồ';
    $lang['campaigndetail_lang']['page'] = 'Trang : ';
    $lang['campaigndetail_lang']['contract'] = 'Mã hợp đồng';
    //status
    $lang['campaigndetail_lang']['run'] = 'Chạy';
    $lang['campaigndetail_lang']['running'] = 'Đang chạy';
    $lang['campaigndetail_lang']['paused'] = 'Tạm dừng';
    $lang['campaigndetail_lang']['completed'] = 'Hoàn thành';
    $lang['campaigndetail_lang']['waiting'] = 'Chờ duyệt';
    $lang['campaigndetail_lang']['problem'] = 'Có lỗi';
    $lang['campaigndetail_lang']['deleted'] = 'Xóa';
    $lang['campaigndetail_lang']['running'] = 'Đang chạy';
    $lang['campaigndetail_lang']['waittorun'] = 'Chờ chạy';
    $lang['campaigndetail_lang']['delete'] = 'Đã lưu trữ';
    $lang['campaigndetail_lang']['destroy'] = 'Hủy';
    $lang['campaigndetail_lang']['temp_delete'] = 'Hủy';
    //edit
    $lang['campaigndetail_lang']['campaignName'] = 'Tên chiến dịch';
    $lang['campaigndetail_lang']['budget'] = 'Ngân sách';
    $lang['campaigndetail_lang']['duration'] = 'Khoảng thời gian';
    $lang['campaigndetail_lang']['edit'] = 'sửa';
    
    $lang['campaigndetail_lang']['byDate'] = 'theo ngày';
    $lang['campaigndetail_lang']['byCam'] = 'theo chiến dịch';
    
    $lang['campaigndetail_lang']['editTooltip'] = 'Chỉnh sửa';
    $lang['campaigndetail_lang']['cancelTooltip'] = 'Thoát';
    
    $lang['campaigndetail_lang']['impressions'] = 'Lượt xem';
    $lang['campaigndetail_lang']['uv'] = 'Người xem';
    $lang['campaigndetail_lang']['null_campaignname'] = 'Tên campaign không được để trống';
    $lang['campaigndetail_lang']['copyad'] = 'Tạo quảng cáo tương tự';
    $lang['campaigndetail_lang']['outofmoney'] = 'Hết ngân sách ngày';
    $lang['campaigndetail_lang']['label'] = 'Nội dung';
    $lang['campaigndetail_lang']['temp_deleted'] = 'Lưu trữ';
    $lang['campaigndetail_lang']['notice_tittle'] = 'Chiến dịch của bạn đã chạy được một thời gian nhưng chưa đạt được hiệu quả click như bạn mong muốn. Để tăng thêm hiệu quả click, bạn hãy thử những cách sau:';
    $lang['campaigndetail_lang']['notice_content'] = '1.  Chọn mục tăng click trong ngày: giúp quảng cáo của bạn hiển thị nhiều hơn và tăng lượng click.<br/>2.  Chọn thêm khu vực hiển thị quảng cáo.Ví dụ: Quảng cáo của bạn không chỉ hiển thị ở Hà Nội, Hải Phòng mà có thể toàn miền bắc.<br/>3.  Chọn thêm các Website: chúng tôi mang quảng cáo của bạn đến rất nhiều Website lớn như: dantri.com.vn, kenh14.vn, muachung.vn,…<br/>4.  Nếu bạn cần giải đáp, liên hệ ngay với chúng tôi.';
    $lang['campaigndetail_lang']['deleteConfirm'] = 'Bạn có thực sự muốn lưu trữ không???';
	$lang['campaigndetail_lang']['recharge'] = 'Xem inventory';
	
	$lang['campaigndetail_lang']['custommer'] = 'Khách hàng';
	$lang['campaigndetail_lang']['save'] = 'Lưu';
?>