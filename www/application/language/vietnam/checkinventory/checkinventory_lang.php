<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['checkinventory_lang']=array();
	$lang['checkinventory_lang']['targeting']='Đối tượng quảng cáo';
	$lang['checkinventory_lang']['checkinventory']='Xem inventory';
	$lang['checkinventory_lang']['location']='Khu vực';
	
	$lang['checkinventory_lang']['size']='Kích thước';
	$lang['checkinventory_lang']['select_size']='Chọn kích thước';
	$lang['checkinventory_lang']['select_size_error']='Bạn chưa chưa chọn kích thước';
	
	
	$lang['checkinventory_lang']['estimate']='Nguồn inventory';
	$lang['checkinventory_lang']['view_inventory']='Xem nguồn tài nguyên inventory và estimate';
	// estimate box
	$lang['checkinventory_lang']['estimate_number_label'] 						= 'Thống kê';
	$lang['checkinventory_lang']['all'] 										= 'Tất cả';
	$lang['checkinventory_lang']['all_region'] 									= 'Toàn quốc';
	$lang['checkinventory_lang']['north_region'] 								= 'Miền Bắc';
	$lang['checkinventory_lang']['middle_region'] 								= 'Miền Trung';
	$lang['checkinventory_lang']['south_region'] 								= 'Miền Nam';
	$lang['checkinventory_lang']['region_error'] 								= 'Bạn chưa chọn đối tượng quảng cáo theo vị trí địa lý.';
	$lang['checkinventory_lang']['like_interest'] 								= 'Theo nhóm đối tượng';
	$lang['checkinventory_lang']['like_interest_error'] 						= 'Bạn chưa chọn nhóm đối tượng khách hàng.';
	
	$lang['checkinventory_lang']['select_website'] 								= 'Theo website';
	$lang['checkinventory_lang']['expand'] 										= 'Mở rộng';
	$lang['checkinventory_lang']['collapse'] 									= 'Thu hẹp';
	
	$lang['checkinventory_lang']['btn_continue'] 								= 'Tiếp tục';
	
	$lang['checkinventory_lang']['btn_view'] 								= 'Xem';
	
	$lang['checkinventory_lang']['campaign_start_date'] 						= 'Ngày bắt đầu';
	$lang['checkinventory_lang']['campaign_end_date'] 							= 'Ngày kết thúc';
	$lang['checkinventory_lang']['ttip_location_title'] 							= 'Khu vực';
	$lang['checkinventory_lang']['ttip_location'] 							= 'Khu vực';
	$lang['checkinventory_lang']['ttip_total_title'] 							= 'Tổng số CPM tối đa';
	$lang['checkinventory_lang']['ttip_total_content'] 							= 'Tổng số CPM tối đa của channel';
	$lang['checkinventory_lang']['ttip_avai_title'] 							= 'Tổng số CPM có thể book';
	$lang['checkinventory_lang']['ttip_avai_content'] 							= 'Tổng số CPM có thể book của channel';
	
?>