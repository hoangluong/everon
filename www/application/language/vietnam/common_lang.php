<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['common_lang'] = array();
	$lang['common_lang']['hi'] = 'Chào';
	$lang['common_lang']['login'] = 'Đăng nhập';
	$lang['common_lang']['logout'] = 'Thoát';
	$lang['common_lang']['my_account'] = 'Tài khoản';
	$lang['common_lang']['register'] = 'Đăng ký';
	$lang['common_lang']['lost_password'] = 'Quên mật khẩu?';
	
	$lang['common_lang']['account_info'] = 'Thông tin cá nhân';
	
	$lang['common_lang']['arr_location'] = array('0'=>'Toàn quốc',
													'1'=>'Miền Bắc',
													'2'=>'Miền Trung',
													'3'=>'Miền Nam',
											);
	
	// for switch user
	$lang['common_lang']['switch_user_title'] = 'Nhập username để chuyển đổi';
	$lang['common_lang']['switch_user_btn'] = 'Chuyển';
	$lang['common_lang']['switch_user_close'] = 'Đóng';
	$lang['common_lang']['switch_user_title'] = 'Chuyển user';
	$lang['common_lang']['switch_buname'] = 'Tài khoản gốc';