<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['contact_lang'] = array();
	$lang['contact_lang']['send'] = 'Gửi';
	$lang['contact_lang']['reset'] = 'Làm lại';
	$lang['contact_lang']['content'] = 'Nội dung';
	$lang['contact_lang']['Fax'] = 'Số Fax';
	$lang['contact_lang']['Tel'] = 'Điện thoại';
	$lang['contact_lang']['Address'] = 'Địa chỉ';
    $lang['contact_lang']['Name'] = 'Cá nhân/Công ty';
    $lang['contact_lang']['contact'] = 'Liên hệ';
    $lang['contact_lang']['home'] = 'Trang chủ';
    //error
    $lang['contact_lang']['enter_name'] = 'Xin điền tên/cơ quan của bạn.';
    $lang['contact_lang']['enter_address'] = 'Xin điền địa chỉ của bạn.';
    $lang['contact_lang']['enter_phone'] = 'Xin điền số điện thoại của bạn.';
    $lang['contact_lang']['enter_email'] = 'Xin điền email của bạn.';
    $lang['contact_lang']['enter_content'] = 'Xin điền nội dung bạn muốn gửi cho chúng tôi.';
    $lang['contact_lang']['invalid_email'] = 'Địa chỉ email không hợp lệ.';
    $lang['contact_lang']['illegal_email'] = 'Địa chỉ email không được chứa ký tự đặc biệt.';
    $lang['contact_lang']['phone_length'] = 'Số điện thoại không hợp lệ.';
    $lang['contact_lang']['success'] = 'Cảm ơn bạn đã góp ý cho dịch vụ của chúng tôi.';
    
    
    $lang['contact_lang']['site_title'] = 'Liên hệ với Admarket để biết chính sách, ưu đãi';
    $lang['contact_lang']['site_meta_title'] = '';
    $lang['contact_lang']['site_meta_keyword'] = 'Liên hệ, quảng cáo';
    $lang['contact_lang']['site_meta_desc'] = 'CÔNG TY CỔ PHẦN TRUYỀN THÔNG VIỆT NAM, Tầng 22 - 23 tháp B Vincom,191 Bà Triệu, Hà Nội. Chúng tôi sẽ liên hệ với bạn trong thời gian nhanh nhất.';