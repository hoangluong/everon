<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['cservice']['home'] = 'Trang chủ';
$lang['cservice']['monitor'] = 'monitor';
$lang['cservice']['campaign_bad'] = 'Các Chiến dịch không chạy hết ngân sách';
$lang['cservice']['user'] = 'Tài khoản';
$lang['cservice']['campaign'] = 'Chiến dịch';
$lang['cservice']['date'] = 'Ngày';
$lang['cservice']['budget'] = 'Ngân sách';
$lang['cservice']['group'] = 'Đại lý';
$lang['cservice']['to_day'] = 'Theo ngày';
$lang['cservice']['to_campaign'] = 'Theo chiến dịch';
$lang['cservice']['total_click'] = 'Tổng click';
$lang['cservice']['realclick'] = 'Click thực';
$lang['cservice']['view'] = 'Views';
$lang['cservice']['ctr'] = 'CTR';
$lang['cservice']['percent'] = 'Percent';
$lang['cservice']['max_value_day'] = 'maxValueDay';
$lang['cservice']['expire_value'] = 'ExpireValue';
$lang['cservice']['from_date'] = 'Ngày bắt đầu';
$lang['cservice']['to_date'] = 'Ngày kết thúc';
$lang['cservice']['page'] = 'Trang';
$lang['cservice']['enduser'] = 'enduser';
$lang['cservice']['all'] = 'all';
$lang['cservice']['apply'] = 'Áp dụng';
$lang['cservice']['user_no_action'] = 'Thành viên không hoạt động';
$lang['cservice']['fullname'] = 'Họ tên';
$lang['cservice']['email'] = 'Email';
$lang['cservice']['phone'] = 'Điện thoại';
$lang['cservice']['address'] = 'Địa chỉ';
$lang['cservice']['balance'] = 'Số dư tài khoản';
$lang['cservice']['promotion'] = 'Tiền khuyến mại';
$lang['cservice']['total_balance'] = 'Tổng tiền đã nạp';
$lang['cservice']['lasttime'] = 'Gần đây nhất';
$lang['cservice']['currency'] = 'đ';
$lang['cservice']['target'] = 'Target';
$lang['cservice']['location'] = 'Vùng miền';
$lang['cservice']['list_site'] = 'Site';
$lang['cservice']['to'] = ' đến ';

$lang['cservice']['banner_error_list'] = 'Danh sách Quảng cáo lỗi';
$lang['cservice']['user'] = 'Tài khoản';
$lang['cservice']['banner'] = 'Quảng cáo';
$lang['cservice']['campaign'] = 'Chiến dịch';
$lang['cservice']['description'] = 'Mô tả';

$lang['cservice']['banner_nrun_list'] = 'Danh sách Quảng cáo chưa chạy';
$lang['cservice']['status'] = 'Trạng thái';
$lang['cservice']['cpanel'] = 'Bảng điều khiển';
$lang['cservice']['customer_service'] = 'Dịch vụ khách hàng';

//Banner warring
$lang['cservice']['banner'] = 'Quảng cáo';
$lang['cservice']['banner_warring'] = 'Cảnh báo quảng cáo';
$lang['cservice']['image'] = 'Ảnh';
$lang['cservice']['warring'] = 'Cảnh báo';
$lang['cservice']['edit_warring'] = 'Sửa Cảnh báo';
$lang['cservice']['close'] = 'Đóng';
$lang['cservice']['add'] = 'Thêm';
$lang['cservice']['save_not_success'] = 'Lưu dữ liệu không thành công, bạn hãy thử lại.';
$lang['cservice']['save_success'] = 'Đã lưu dữ liệu thành công.';
$lang['cservice']['enter_warring'] = 'Bạn hãy nhập Cảnh báo.';
$lang['cservice']['click'] = 'Click';
$lang['cservice']['realclick'] = 'Click thực';
$lang['cservice']['view'] = 'Views';
$lang['cservice']['ctr'] = 'CTR';
$lang['cservice']['percent'] = 'Percent';
//end

//campaign completed
$lang['cservice']['campaign_completed'] = 'Chiến dịch đã hoàn thành';
$lang['cservice']['click_charge'] = 'Số click tính tiền';
//end