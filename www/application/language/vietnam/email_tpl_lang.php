<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	$lang['email_tpl_lang'] = array();
	
	/* email report */
	$lang['email_tpl_lang']['report'] = array();
	$lang['email_tpl_lang']['report']['report_from'] 										= 'Báo cáo từ ngày';
	$lang['email_tpl_lang']['report']['report_to'] 											= 'đến ngày';
	
	$lang['email_tpl_lang']['report']['report_title'] 										= 'Báo cáo hiệu quả quảng cáo';
	
	$lang['email_tpl_lang']['report']['impression_title'] 									= 'Impressions';
	$lang['email_tpl_lang']['report']['click_title'] 										= 'Clicks';
	$lang['email_tpl_lang']['report']['ctr_title'] 											= 'CTR';
	$lang['email_tpl_lang']['report']['spent_title'] 										= 'Số tiền đã sử dụng';
	$lang['email_tpl_lang']['report']['campname_title'] 									= 'Chiến dịch';
	$lang['email_tpl_lang']['report']['createad_title'] 									= 'Tạo thêm quảng cáo mới';
	$lang['email_tpl_lang']['report']['createad_content_title'] 							= 'Chạy nhiều quảng cáo sẽ cho phép bạn kiểm thử các quảng cáo khác nhau với các hình ảnh, đối tượng quảng cáo để quyết định quảng cáo nào có hiệu quả tốt nhất.';
	$lang['email_tpl_lang']['report']['recomment_title'] 									= 'Gợi ý cho bạn';
	$lang['email_tpl_lang']['report']['recomment_content_title_1'] 							= 'Để tìm hiểu thêm về ngân sách, đối tượng quảng cáo và các hướng dẫn tạo chiến dịch hiệu quả, truy cập vào';
	$lang['email_tpl_lang']['report']['recomment_content_title_2'] 							= 'hướng dẫn sử dụng';
	$lang['email_tpl_lang']['report']['recomment_content_title_3'] 							= 'để tạo quảng cáo tốt nhất.';
	
	$lang['email_tpl_lang']['report']['thank_title'] 										= 'Cám ơn bạn đã sử dụng hệ thống quảng cáo AdMarket';
	$lang['email_tpl_lang']['report']['sincerely_title'] 									= 'Trân trọng';
	$lang['email_tpl_lang']['report']['team_title'] 										= 'Hệ thống quảng cáo AdMarket';
	$lang['email_tpl_lang']['report']['ps_title'] 											= 'Tái bút, chúng tôi đánh giá cao phản hồi của bạn và sẽ tiếp tục nâng cao chất lượng để mang lại sản phẩm cho bạn tốt hơn.';
	$lang['email_tpl_lang']['report']['go_back_title'] 										= 'Truy cập hệ thống quảng cáo AdMarket';
	$lang['email_tpl_lang']['report']['create_new_ad_manage_title'] 						= 'Để tạo mới quảng cáo và chiến dịch, bạn hãy click vào link dưới đây.';
	/* end email report */
	
	// email add banner
	$lang['email_tpl_lang']['add_banner']['hi'] 											= 'Xin chào';
	$lang['email_tpl_lang']['add_banner']['thank_title'] 									= 'Cám ơn bạn đã tạo quảng cáo trên hệ thống AdMarket. Hệ thống chúng tôi sẽ kiểm tra lại nội dung quảng cáo của bạn trong thời gian sớm nhất. Bạn chú ý việc tạo quảng cáo với nội dung phù hợp với thuần phong mỹ tục và luật pháp nước Việt Nam. Bạn chỉ bị trừ tiền khi người dùng xem và click vào quảng cáo của bạn. Tổng số tiền bạn phải trả luôn không vượt quá số tiền bạn đã đặt cho toàn chiến dịch.<br/><br/>Để tăng hiệu quả quảng cáo của bạn, bạn có thể tạo ra nhiều phiên bản quảng cáo. Hãy tạo ra từ 5 đến 10 phiên bản cho mỗi quảng cáo để trải nghiệm các cách kết hợp nội dung với hình ảnh quảng cáo trên hệ thống của chúng tôi, giúp bạn có thể tìm ra cách tạo quảng cáo hiệu quả nhất.';
	$lang['email_tpl_lang']['add_banner']['request_title_1'] 								= 'Nếu bạn có bất kỳ câu hỏi hay thắc mắc gì về';
	$lang['email_tpl_lang']['add_banner']['request_title_2'] 								= 'hệ thống quảng cáo AdMarket';
	$lang['email_tpl_lang']['add_banner']['request_title_4'] 								= 'hãy truy cập vào hệ thống trợ giúp của chúng tôi để chúng tôi có thể giải đáp các thắc mắc giúp bạn.';
	
	$lang['email_tpl_lang']['add_banner']['ad_preview_title'] 								= 'Nội dung quảng cáo';
	
	$lang['email_tpl_lang']['add_banner']['note_title'] 									= 'Chúng tôi sẽ gửi cho bạn thông báo khi quảng cáo của bạn được duyệt và đăng trên hệ thống. Bạn chỉ phải trả tiền khi quảng cáo của bạn được chạy, và bạn có thể xem trong tài khoản của bạn ở hệ thống.';
	$lang['email_tpl_lang']['add_banner']['optimize_title'] 								= 'Tối ưu các quảng cáo của bạn, xem các báo cáo và nhiều hơn thế nữa.';
	$lang['email_tpl_lang']['add_banner']['camp_info_title'] 								= 'Thông tin chiến dịch';
	$lang['email_tpl_lang']['add_banner']['camp_name_title'] 								= 'Tên chiến dịch';
	$lang['email_tpl_lang']['add_banner']['camp_budget_title'] 								= 'Ngân sách';
	$lang['email_tpl_lang']['add_banner']['camp_duration_title'] 							= 'Thời gian chạy';
	
	$lang['email_tpl_lang']['add_banner']['mess_to_pub_title'] 								= 'Quảng cáo mới được tạo.';
	$lang['email_tpl_lang']['add_banner']['user_create_info_title'] 						= 'Thông tin người tạo';
	$lang['email_tpl_lang']['add_banner']['user_create_fullname_title'] 					= 'Họ và tên';
	// end email add banner
	
	
	// email thong bao banner loi
	$lang['email_tpl_lang']['ad_error_alert']['error_content'] 								= 'Nội dung';
	$lang['email_tpl_lang']['ad_error_alert']['go_back_title'] 								= 'Truy cập hệ thống quảng cáo AdMarket';
	$lang['email_tpl_lang']['ad_error_alert']['create_new_ad_manage_title'] 				= 'Để tạo mới quảng cáo và chiến dịch, bạn hãy click vào link dưới đây.';
	// end email thong bao banner loi
	
	// email thong bao banner da duoc duyet
	$lang['email_tpl_lang']['ad_accepted_alert']['go_back_title'] 							= 'Truy cập hệ thống quảng cáo AdMarket';
	$lang['email_tpl_lang']['ad_accepted_alert']['create_new_ad_manage_title'] 				= 'Để tạo mới quảng cáo và chiến dịch, bạn hãy click vào link dưới đây.';
	// end email thong bao banner da duoc duyet
?>