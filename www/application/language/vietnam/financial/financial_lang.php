<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['financial_lang'] = array();
	$lang['financial_lang']['financial'] = 'Tài chính';
    $lang['financial_lang']['creatAd'] = 'Tạo quảng cáo';
    $lang['financial_lang']['page'] = 'Trang';
    $lang['financial_lang']['timefilter'] = 'Lọc theo thời gian';
    $lang['financial_lang']['day'] = 'Ngày';
    $lang['financial_lang']['week'] = 'Tuần';
    $lang['financial_lang']['month'] = 'Tháng';
    $lang['financial_lang']['year'] = 'Năm';
    $lang['financial_lang']['money-pay'] = 'Tiền đã nạp';
    $lang['financial_lang']['money-use'] = 'Tiền đã nạp';
    $lang['financial_lang']['promotion'] = 'Tiền khuyến mãi';
    $lang['financial_lang']['time'] = 'Thời gian';
    $lang['financial_lang']['apply'] = 'Áp dụng';
    $lang['financial_lang']['user'] = 'Tên người dùng';
    $lang['financial_lang']['total'] = 'Tổng cộng';
?>