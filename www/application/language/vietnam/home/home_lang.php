<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	$lang['home_lang'] = array();
	$lang['home_lang']['create_ad_page_title'] 							= 'Tạo mới quảng cáo';
	$lang['home_lang']['create_camp_page_title'] 							= 'Tạo mới chiến dịch';
	// estimate box
	$lang['home_lang']['estimate_number_label'] 						= 'Thống kê';
	
	// step 1
	$lang['home_lang']['design_your_ad'] 								= 'Tạo quảng cáo';
	$lang['home_lang']['design_your_ad_faq']							= 'Câu hỏi thường gặp [FAQs]';
	$lang['home_lang']['copy_ad'] 										= 'Chọn quảng cáo đã có';
	$lang['home_lang']['optional'] 										= 'tùy chọn';
	$lang['home_lang']['example_ad_title'] 								= 'Tiêu đề quảng cáo';
	$lang['home_lang']['body_text_here'] 								= 'Nội dung quảng cáo.';
	
	$lang['home_lang']['des_url'] 										= 'URL đích';
	$lang['home_lang']['des_url_error'] 								= 'URL không hợp lệ.';
	$lang['home_lang']['suggest_ad'] 									= 'Gợi ý quảng cáo';
	
	$lang['home_lang']['example'] 										= 'Ví dụ';
	
	$lang['home_lang']['title'] 										= 'Tiêu đề';
	$lang['home_lang']['title_error'] 									= 'Bạn chưa nhập tiêu đề quảng cáo.';
	
	$lang['home_lang']['body_text'] 									= 'Nội dung';
	$lang['home_lang']['body_text_error'] 								= 'Bạn chưa nhập nội dung quảng cáo.';
	
	$lang['home_lang']['image'] 										= 'Hình ảnh';
	$lang['home_lang']['image_error'] 									= 'Bạn hãy chọn ảnh cho quảng cáo.';
	
	$lang['home_lang']['char_left'] 									= 'ký tự';
	$lang['home_lang']['select_an_ad'] 									= 'Bạn hãy chọn một quảng cáo';
	
	$lang['home_lang']['set_style_max'] 								= 'Tối đa';
	
	// step 2
	$lang['home_lang']['targeting'] 									= 'Đối tượng quảng cáo';
	$lang['home_lang']['target_faq'] 									= 'Câu hỏi thường gặp [FAQs]';
	$lang['home_lang']['location'] 										= 'Khu vực';
	$lang['home_lang']['location_city'] 								= 'Theo Tỉnh - Thành phố';
	$lang['home_lang']['view_more']	 									= 'Xem thêm';
	$lang['home_lang']['target_review_ad_btn']	 						= 'Duyệt quảng cáo';
	
	$lang['home_lang']['all'] 											= 'Tất cả';
	$lang['home_lang']['all_region'] 									= 'Toàn quốc';
	$lang['home_lang']['north_region'] 									= 'Miền Bắc';
	$lang['home_lang']['middle_region'] 								= 'Miền Trung';
	$lang['home_lang']['south_region'] 									= 'Miền Nam';
	$lang['home_lang']['region_error'] 									= 'Bạn chưa chọn đối tượng quảng cáo theo vị trí địa lý.';
	
	$lang['home_lang']['like_interest'] 								= 'Theo nhóm đối tượng';
	$lang['home_lang']['like_interest_error'] 							= 'Bạn chưa chọn nhóm đối tượng khách hàng.';
	$lang['home_lang']['like_suggest_title'] 							= 'Có thể bạn quan tâm?';
	
	$lang['home_lang']['target_channel_error'] 							= 'Bạn chưa chọn đối tượng quảng cáo theo website.';
	
	$lang['home_lang']['select_website'] 								= 'Theo website';
	$lang['home_lang']['expand'] 										= 'Mở rộng';
	$lang['home_lang']['collapse'] 										= 'Thu hẹp';
	
	$lang['home_lang']['btn_continue'] 									= 'Tiếp tục';
	$lang['home_lang']['people']	 									= 'người dùng';
	
	// step 3
	$lang['home_lang']['step3_label'] 									= 'Chiến dịch quảng cáo';
	$lang['home_lang']['step3_hdcode_label'] 							= 'Mã hợp đồng';
	$lang['home_lang']['step3_hd_guest_label'] 							= 'Tên khách hàng';
	
	$lang['home_lang']['step3_campaign_faq'] 							= 'Câu hỏi thường gặp về chiến dịch và giá';
	$lang['home_lang']['campaign_budget'] 								= 'Chiến dịch và ngân sách';
	$lang['home_lang']['campaign_budget_label'] 						= 'Ngân sách';
	
	$lang['home_lang']['campaign_name'] 								= 'Tên chiến dịch';
	$lang['home_lang']['campaign_budget'] 								= 'Chiến dịch, ngân sách';
	$lang['home_lang']['campaign_budget_vnd'] 							= 'nghìn VND';
	$lang['home_lang']['vnd_title'] 									= 'VND';
	$lang['home_lang']['campaign_budget_perday'] 						= 'mỗi ngày';
	$lang['home_lang']['campaign_budget_lifetime'] 						= 'Toàn chiến dịch';
	$lang['home_lang']['campaign_budget_lifetime_lower'] 				= 'toàn chiến dịch';
	$lang['home_lang']['campaign_budget_note'] 							= 'bạn muốn sử dụng bao nhiêu tiền mỗi ngày? (ít nhất 100,000 VND)';
	$lang['home_lang']['campaign_start_date'] 							= 'Ngày bắt đầu';
	$lang['home_lang']['campaign_end_date'] 							= 'Ngày kết thúc';
	$lang['home_lang']['choose_exist_campaign'] 						= 'Chọn chiến dịch đã được tạo';
	$lang['home_lang']['campaign_create_new'] 							= 'Tạo mới chiến dịch';
	$lang['home_lang']['campaign_price'] 								= 'Giá';
	
	$lang['home_lang']['campaign_price_note_1']                                                     = 'Giá mỗi click là';
	$lang['home_lang']['campaign_price_note_2']                                                     = 'giá này chưa bao gồm thuế VAT.';
	
	$lang['home_lang']['btn_review_ad']                                                             = 'Duyệt quảng cáo';
	
	$lang['home_lang']['create_camp_name_error']                                                    = 'Bạn chưa nhập tên chiến dịch.';
	$lang['home_lang']['create_camp_budget_error']                                                  = 'Chi phí nhỏ nhất là 100,000 VND.';
	$lang['home_lang']['create_camp_time_error']                                                    = 'Ngày bắt đầu và Ngày kết thúc chiến dịch không hợp lệ. Ngày bắt đầu và Ngày kết thúc phải lớn hơn hoặc bằng ngày hiện tại, và Ngày kết thúc phải lớn hơn hoặc bằng Ngày bắt đầu. Bạn hãy chọn lại giá trị phù hợp.';
	$lang['home_lang']['choose_camp_name_error']                                                    = 'Bạn chưa chọn chiến dịch.';
	
	
	// step 4
	$lang['home_lang']['review_ad_title'] 								= 'Duyệt lại quảng cáo của bạn';
	$lang['home_lang']['ad_review'] 								= 'Xem trước quảng cáo';
	
	$lang['home_lang']['ad_review_ad_name'] 							= 'Tên quảng cáo';
	$lang['home_lang']['review_location_label']                                                     = 'Định hướng theo vị trí địa lý bạn đã chọn';
	$lang['home_lang']['review_like_label'] 							= 'Định hướng theo nhóm đói tượng khách hàng bạn đã chọn';
	$lang['home_lang']['review_website'] 								= 'Website';
	$lang['home_lang']['review_website_is_target']                                                  = 'Định hướng theo website bạn đã chọn';
	$lang['home_lang']['review_bid_type'] 								= 'Loại hình quảng cáo';
	$lang['home_lang']['review_bid'] 								= 'Đơn giá';
	$lang['home_lang']['price_vat_note'] 								= 'chưa bao gồm VAT';
	
	$lang['home_lang']['daily_budget'] 								= 'Chi phí';
	$lang['home_lang']['vnd_per_click'] 								= 'VNĐ mỗi click';
	$lang['home_lang']['duration'] 									= 'Trong khoảng thời gian';
	$lang['home_lang']['place_order'] 								= 'Đặt quảng cáo';
	$lang['home_lang']['edit_ad'] 									= 'Chỉnh sửa quảng cáo';	
	$lang['home_lang']['edit_ad_btn'] 								= 'Chỉnh sửa';
	
	
	$lang['home_lang']['review_like_interest_title']                                                = 'Nhóm đói tượng';
	$lang['home_lang']['day_title'] 								= 'ngày';
	
	$lang['home_lang']['from_day_title'] 								= 'từ ngày';
	$lang['home_lang']['to_day_title'] 								= 'đến ngày';
	
	// estimate box
	$lang['home_lang']['estimate_have'] 								= 'Có';
	$lang['home_lang']['estimate_location'] 							= 'Ở khu vực';
	$lang['home_lang']['estimate_city_location']                                                    = 'Tỉnh - Thành phố';
	$lang['home_lang']['estimate_like_interest']                                                    = 'Thuộc nhóm đối tượng';
	$lang['home_lang']['estimate_review_website']                                                   = 'Là độc giả các website';
	
	
	// edit ad
	$lang['home_lang']['edit_ad_invalid'] 								= 'Bạn không có quyền thao tác với quảng cáo này.';
	$lang['home_lang']['edit_banner_expire_error']                                                  = 'Quảng cáo đã hoàn thành. Bạn không có quyền chỉnh sửa banner này.';
	
	
	// tool tip
	$lang['home_lang']['ttip_des_url_title'] 							= 'Url đích';
	$lang['home_lang']['ttip_tracking_title'] 							= 'Third party impression tracking';
	$lang['home_lang']['ttip_tracking']                                                             = 'Hệ thống hỗ trợ đo lượt impressions đối với 1 số AdNetwork đối tác gồm Google Analytics, MediaMind/Eyeblaster, DoubleClick';

	$lang['home_lang']['ttip_des_url'] 								= 'Trang web khi người dùng click vào quảng cáo sẽ được chuyển đến trang này.';
	
	$lang['home_lang']['ttip_suggest_ad_title']                                                     = 'Gợi ý quảng cáo';
	$lang['home_lang']['ttip_suggest_ad'] 								= 'Gợi ý giúp bạn phần tiêu đề và nội dung quảng cáo ứng với URL bạn mong muốn.';
	
	$lang['home_lang']['ttip_title_title'] 								= 'Tiêu đề';
	$lang['home_lang']['ttip_title'] 									= 'Thể hiện tiêu đề quảng cáo của bạn thật ngắn gọn, súc tích, nổi bật, đơn giản. Mục đích nổi bật sản phẩm, độ dài không quá 40 ký tự. Hạn chế các ký tự lạ, các ký tự đặc biệt, không phù hợp với ngữ cảnh. Bạn được sử dụng tối đa 4 ký tự màu đỏ, chỉ được sử dụng cho phần giá sản phẩm hoặc giảm giá.';
	
	$lang['home_lang']['ttip_body_title'] 								= 'Nội dung';
	$lang['home_lang']['ttip_body'] 									= 'Miêu tả đầy đủ nội dung về sản phẩm hay dịch vụ mà bạn cung cấp. Nhấn mạnh những lợi ích cung cấp cho người dùng, chất lượng cao cấp, hệ thống giảm giá, cung cấp bảo hành,…. Bạn được sử dụng tối đa 20 ký tự màu đỏ, chỉ được sử dụng cho phần giá sản phẩm hoặc giảm giá.';
	
	$lang['home_lang']['ttip_image_title'] 								= 'Hình ảnh';
	$lang['home_lang']['ttip_image']                                                                = 'Đây là 1 file ảnh hay 1 file dạng flash của bạn. Hình ảnh mà bạn đưa lên hệ thống sẽ được xuất hiện trên các website theo đúng kích thước cho phép. Để có một banner đúng chuẩn, bạn có thể xem hướng dẫn <b>tại đây</b>.';
	
	$lang['home_lang']['ttip_location_title'] 							= 'Khu vực';
	$lang['home_lang']['ttip_location'] 								= ':  Nếu sản phẩm của bạn chỉ hướng đến khách hàng Miền Nam thì đây là cách giúp bạn lựa chọn đúng đối tượng theo vùng miền. Quảng cáo của bạn sẽ tập trung xuất hiện ở Miền Nam, như vậy sẽ giảm chi phí tối đa cho những lượt xem ở vùng miền khác tạo ra khi bạn không sử dụng tính năng này.';
	
	$lang['home_lang']['ttip_like_title'] 								= 'Nhóm đối tượng';
	$lang['home_lang']['ttip_like'] 									= 'Lựa chọn nhóm khách hàng mà bạn hướng tới, nên chọn từ hai đến nhiều loại để quảng cáo của bạn trúng đích hơn.';
	
	$lang['home_lang']['ttip_website_title'] 							= 'Theo website';
	$lang['home_lang']['ttip_website'] 								= 'Quảng cáo của bạn chỉ hiện trên những website mà bạn lựa chọn. Hơn thế nữa, hệ thống chỉ tính tiền cho những lượt xem từ những website mà bạn chọn, bạn sẽ không phải trả tiền vì những lượt xem từ các website khác nếu có.';
	
	$lang['home_lang']['ttip_create_campaign_title']                                                = 'Tạo mới chiến dịch';
	$lang['home_lang']['ttip_create_campaign'] 							= 'Một chiến dịch cho phép có nhiều quảng cáo chạy cùng lúc. Bạn chỉ nên tạo mới chiến dịch khi thấy có quá nhiều quảng cáo hoặc cùng lúc bạn muốn chạy nhiều ngân sách khác nhau.';
	
	$lang['home_lang']['ttip_choose_campaign_title']                                                = 'Chọn chiến dịch đã được tạo';
	$lang['home_lang']['ttip_choose_campaign'] 							= 'Chọn Chiến dịch đã được tạo nếu bạn muốn bạn muốn thêm quảng cáo vào một Chiến dịch đã tạo từ trước.';
	
	$lang['home_lang']['ttip_campaign_budget_title']                                                = 'Ngân sách';
	$lang['home_lang']['ttip_campaign_budget']                                                      = 'Chọn ngân sách bạn chi cho chiến dịch quảng cáo, có thể theo ngày hoặc theo toàn chiến dịch. Nếu bạn chọn theo ngày, đó là chi phí <b>tối đa trong 1 ngày</b> cho chiến dịch. Nếu bạn chọn chạy theo toàn chiến dịch, thì có nghĩa là chiến dịch này sẽ hoàn thành (bị dừng) nếu chạy hết số tiền được chỉ định.';
	
	$lang['home_lang']['ttip_banner_title']                                                         = 'Tên banner';
	$lang['home_lang']['ttip_banner']                                                               = 'Đặt tên cho banner giúp bạn quản lý các quảng cáo của bạn dễ hơn.';
	
	// button
	$lang['home_lang']['btn_back'] 									= 'Quay lại';
	$lang['home_lang']['city_label'] 								= 'Tỉnh - Thành phố';
	$lang['home_lang']['per_day'] 									= 'mỗi ngày';
	
	$lang['home_lang']['invalid_file_upload'] 							= 'File upload không hợp lệ. Chỉ upload file .gif, .png, .jpg';
	$lang['home_lang']['upload_not_connect_server']                                                 = 'Không upload được ảnh lên server';
	$lang['home_lang']['upload_file_exists'] 							= 'Trùng tên file';
        $lang['home_lang']['file_upload_have_link'] 							= 'Không được phép đặt link trong file flash. Xem chuẩn file flash <b>tại đây</b>';
	
	$lang['home_lang']['city_suggest_title'] 							= 'Nhập tên tỉnh thành phố để lựa chọn.';
	$lang['home_lang']['cat_suggest_title'] 							= 'Nhập lĩnh vực để lựa chọn.';
	$lang['home_lang']['view_more_all'] 								= 'Xem tất cả';
	
	$lang['home_lang']['approximate'] 								= 'Xấp xỉ';
	$lang['home_lang']['click_title'] 								= 'click';
	
	
	// email
	$lang['home_lang']['email_create_ad_title']                                                     = 'Tạo mới quảng cáo: ';
	
	// copy ad
	$lang['home_lang']['copy_ad'] 									= 'Copy quảng cáo';
	
	// subcribe 
	$lang['home_lang']['subcribe_invalid_email']                                                    = 'Email không hợp lệ.';
	$lang['home_lang']['subcribe_email_exists']                                                     = 'Cảm ơn bạn, email này đã tồn tại trong hệ thống.';
	$lang['home_lang']['subcribe_ok'] 								= 'Cảm ơn bạn, email của bạn đã được lưu vào hệ thống.';
	
	
	$lang['home_lang']['index_site_title'] 								= 'Mạng quảng cáo của người Việt';
	$lang['home_lang']['index_site_meta_title']                                                     = '';
	$lang['home_lang']['index_site_meta_keyword']                                                   = 'quảng cáo, dantri, kenh14, soha, socvui, 8x01, quảng cáo VCTV, quảng cáo muare, quảng cáo vneconomy, quảng cáo sannhac';
	$lang['home_lang']['index_site_meta_desc'] 							= 'Chợ quảng cáo online, quảng cáo chọn ngân sách theo ngày, quản lý hiệu suất các quảng cáo, chặn gian lận quảng cáo';
	
	// error code
	$lang['home_lang']['createad_error_title'] 							= 'Tạo mới quảng cáo lỗi';
	$lang['home_lang']['editad_error_title'] 							= 'Chỉnh sửa quảng cáo lỗi';
	$lang['home_lang']['101'] 									= 'Url đích không đúng.';
	$lang['home_lang']['1021'] 									= 'Bạn chưa nhập tiêu đề quảng cáo.';
	$lang['home_lang']['1022'] 									= 'Số ký tự tiêu đề vượt quá giới hạn cho phép.';
	$lang['home_lang']['1023'] 									= 'Số ký tự màu đỏ ở tiêu đề vượt quá giới hạn cho phép.';
	$lang['home_lang']['1024'] 									= 'Chỉnh màu cho tiêu đề quảng cáo không hợp lệ';
	$lang['home_lang']['1025'] 									= 'Thẻ html ở tiêu đề chưa đúng.';
        $lang['home_lang']['1026'] 									= 'Ngày bắt đầu hoặc Ngày kết thúc banner không hợp lệ. Ngày bắt đầu và Ngày kết thúc phải lớn hơn hoặc bằng ngày hiện tại, và Ngày kết thúc phải lớn hơn hoặc bằng Ngày bắt đầu.';
        $lang['home_lang']['1027'] 									= 'Bạn chưa chọn site để quảng cáo.';
        $lang['home_lang']['1028'] 									= 'Bạn chưa chọn loại quảng cáo.';
        $lang['home_lang']['1029'] 									= 'Bạn chưa upload file quảng cáo.';
        $lang['home_lang']['1030'] 									= 'Link view không đúng. Hệ thống chỉ hỗ trợ đo lượt impressions đối với 1 số AdNetwork đối tác gồm MediaMind/Eyeblaster, DoubleClick';
        
	
	$lang['home_lang']['1031'] 											= 'Bạn chưa nhập nội dung quảng cáo.';
	$lang['home_lang']['1032'] 											= 'Số ký tự nội dung quảng cáo vượt quá giới hạn cho phép.';
	$lang['home_lang']['1033'] 											= 'Số ký tự bôi đậm hoặc màu đỏ vượt quá giới hạn cho phép. Số ký tự đậm tối đa là {b}. Số ký tự màu đỏ tối đa là {c}. Tổng số ký tự tối đa là {t}.';
	$lang['home_lang']['1034'] 											= 'Chỉnh màu cho nội dung quảng cáo không hợp lệ.';
	$lang['home_lang']['1035'] 											= 'Thẻ html ở nội dung chưa đúng.';
	$lang['home_lang']['1036'] 											= 'Kích thước banner không phù hợp.';
	$lang['home_lang']['1037'] 											= 'Tên banner không chính xác.';
	$lang['home_lang']['1038'] 											= 'File backup không chính xác.';
	$lang['home_lang']['1039'] 											= 'File expand không chính xác.';
	
	$lang['home_lang']['201'] 											= 'Bạn chưa chọn chuyên mục website.';
	$lang['home_lang']['202'] 											= 'Bạn cần nhập chính xác giá trị phân bổ theo ngày và website.';
	
	$lang['home_lang']['301'] 											= 'Bạn chưa nhập tên chiến dịch.';
	$lang['home_lang']['302'] 											= 'Ngân sách tối thiểu là 100000 VNĐ';
	$lang['home_lang']['303'] 											= 'Ngày bắt đầu hoặc Ngày kết thúc chiến dịch không hợp lệ. Ngày bắt đầu và Ngày kết thúc phải lớn hơn hoặc bằng ngày hiện tại, và Ngày kết thúc phải lớn hơn hoặc bằng Ngày bắt đầu.';
	$lang['home_lang']['304'] 											= 'Bạn không có quyền truy cập chiến dịch này.';
	$lang['home_lang']['305'] 											= 'Chiến dịch không tồn tại hoặc đã hết hạn.';
	$lang['home_lang']['306'] 											= 'Ngày bắt đầu hoặc Ngày kết thúc quảng cáo không hợp lệ. Ngày bắt đầu và Ngày kết thúc phải lớn hơn hoặc bằng ngày hiện tại, và Ngày kết thúc phải lớn hơn hoặc bằng Ngày bắt đầu.';
	$lang['home_lang']['307'] 											= 'Bạn chưa nhập tên chiến dịch.';
	$lang['home_lang']['308'] 											= 'Chiến dịch đã hết hạn.';
	$lang['home_lang']['309'] 											= 'Tổng số tiền phân bổ cho các loại quảng cáo không được vượt quá ngân sách.';
	$lang['home_lang']['3010'] 											= 'Ngân sách tối đa cho 1 ngày không được vượt quá tổng ngân sách.';
	$lang['home_lang']['3011'] 											= 'Bạn phải chọn ít nhất 1 gói CPM.';
	$lang['home_lang']['3012'] 											= 'Bạn phải nhập chính xác mã hợp đồng.';
	$lang['home_lang']['3013'] 											= 'Bạn phải nhập chính xác tên khách hàng.';
	$lang['home_lang']['3014'] 											= 'Booking id không chính xác.';

	$lang['home_lang']['focus_desurl_tooltip'] 							= 'Bạn nên đặt Url giới thiệu sản phẩm, không được sử dụng các dạng shorten url.';
	$lang['home_lang']['focus_title_bodytext_tooltip'] 					= 'Bạn phải viết đúng chính tả, viết hoa chữ đầu tiên, không sử dụng toàn bộ chữ in hoa. Ngôn ngữ bạn sử dụng phải thuần việt, không dùng ký tự đặc biệt. Bạn có thể sử dụng bôi đỏ để nhấn mạnh giá cho sản phẩm và % giảm giá, các chữ khác có thể sử dụng bôi đậm.';
	$lang['home_lang']['focus_image_tooltip'] 							= 'Bạn chỉ sử dụng hình ảnh các sản phẩm, không sử dụng text hoặc logo thương hiệu. Bạn chú ý ảnh phải rõ nét, không bị khuyết, bị mờ.';

        $lang['home_lang']['invalid_file_size'] 						= 'File upload không đúng kích cỡ.';
        $lang['home_lang']['invalid_file_weight'] 						= 'File upload quá lớn. Độ lớn của file upload không được vượt quá '.IMAGE_SIZE.'KB.';

        $lang['home_lang']['submit_success'] 							= 'Tạo banner thành công. Tiếp tục nhập thông tin banner để có thể thêm mới banner.';
        $lang['home_lang']['third_party_tracking_title']                = 'Công cụ theo dõi quảng cáo của bên thứ 3';
        $lang['home_lang']['third_party_tracking']                      = 'Mặc định, quảng cáo của bạn sẽ được hệ thống của AdMicro theo dõi từng lượt hiển thị, lượt click của người dùng. Nhưng đôi khi, bạn muốn thêm một công cụ của hãng thứ 3 đo kiểm lại số liệu để có thể so sánh. Hiện chúng tôi chỉ hỗ trợ 1 vài hãng thứ 3 là đối tác của AdMicro, và danh sách này sẽ được cập nhật thêm sau.';
        $lang['home_lang']['google_tracking_title']                     = 'Google analytics';
        $lang['home_lang']['google_tracking']                           = 'Công cụ Google Analytics (GA) là công cụ theo dõi, phân tích các lượt visit của website. Nên công cụ này không thể đo lượt click hay lượt xem của mỗi quảng cáo. Bạn chỉ dùng công cụ này trong trường hợp bạn muốn theo dõi và so sánh giữa lượt clicks trên hệ thống AdMicro và lượt visit trên GA. Tìm hiểu thêm <b><a target=&quot;_blank&quot; href=&quot;http://admarket.admicro.vn/faqs/topic/5&quot;>tại đây</a></b>';
        $lang['home_lang']['other_tracking_title']                      = 'Công cụ theo dõi quảng cáo của bên khác';
        $lang['home_lang']['other_tracking']                            = 'Công cụ theo dõi quảng cáo của bên khác như serving-sys.com, doubleclick.net';
        $lang['home_lang']['link_view']                                 = 'Link theo dõi lượt view của quảng cáo';
        $lang['home_lang']['link_view_content']                         = 'Chúng tôi chỉ cho phép nhập các url đến từ serving-sys.com, doubleclick.net.';
        $lang['home_lang']['ttip_total_title'] 							= 'Tổng số CPM tối đa';
	$lang['home_lang']['ttip_total_content'] 							= 'Tổng số CPM tối đa của channel';
	$lang['home_lang']['ttip_avai_title'] 							= 'Tổng số CPM có thể book';
	$lang['home_lang']['ttip_avai_content'] 							= 'Tổng số CPM có thể book của channel';