<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$lang['left_menu_lang'] = array();
$lang['left_menu_lang']['home'] = 'Trang chủ';
$lang['left_menu_lang']['campaign'] = 'Chiến dịch';
$lang['left_menu_lang']['reports'] = 'Báo cáo';
$lang['left_menu_lang']['budget'] = 'Ngân quỹ';
$lang['left_menu_lang']['setting'] = 'Thiết lập';
$lang['left_menu_lang']['help'] = 'Trợ giúp';
$lang['left_menu_lang']['FAQ'] = 'Câu hỏi thường gặp';
$lang['left_menu_lang']['viewAll'] = 'Xem tất cả';
$lang['left_menu_lang']['contact_to_help'] = 'Liên hệ để được trợ giúp';
$lang['left_menu_lang']['searchbanner'] = 'Tìm quảng cáo';
$lang['left_menu_lang']['searchcampaign'] = 'Tìm chiến dịch';
$lang['left_menu_lang']['breview'] = 'Duyệt banner';
$lang['left_menu_lang']['account_info'] = 'Thông tin cá nhân';
$lang['left_menu_lang']['agencies_info'] = 'Thông tin đại lý';
$lang['left_menu_lang']['archive'] = 'Lưu trữ quảng cáo';
$lang['left_menu_lang']['support_title'] = 'Hỗ trợ trực tuyến';
$lang['left_menu_lang']['switch_user_title'] = 'Chuyển user sử dụng';
$lang['left_menu_lang']['check_inventory'] = 'Xem inventory';
$lang['left_menu_lang']['customer_info'] = 'Quản lý khách hàng';