<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	$lang['login_lang'] = array();
	$lang['login_lang']['login_title'] 										= 'Đăng nhập tài khoản';
	$lang['login_lang']['username'] 										= 'Tên đăng nhập';
	$lang['login_lang']['password'] 										= 'Mật khẩu';
	
	$lang['login_lang']['register_new_account'] 							= 'Đăng ký';
	$lang['login_lang']['error_mess'] 										= 'Tên đăng nhập hoặc mật khẩu không đúng.';
	
	$lang['login_lang']['btn_login'] 										= 'Đăng nhập';
	$lang['login_lang']['or'] 												= 'hoặc';
	
	$lang['login_lang']['error_username'] 									= 'Bạn hãy nhập tên tài khoản!';
	$lang['login_lang']['error_password'] 									= 'Bạn hãy nhập mật khẩu!';
	$lang['login_lang']['register_lostpassword'] 							= 'Quên mật khẩu?';
	$lang['login_lang']['remember'] 										= 'Ghi nhớ tài khoản?';
?>