<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['payment_lang'] = array();

$lang['payment_lang']['title_help_balance'] = 'Số dư tài khoản chính (VNĐ)';
$lang['payment_lang']['title_help_promotions'] = 'Số dư tài khoản khuyến mại (VNĐ)';
$lang['payment_lang']['help_balance'] = 'Số dư tài khoản chính.';
$lang['payment_lang']['title_help_overdraft'] = 'Định mức thấu chi (VNĐ)';
$lang['payment_lang']['help_overdraft'] = 'Định mức thấu chi.';
$lang['payment_lang']['title_help_money_use'] = 'Số tiền đã sử dụng (VNĐ)';
$lang['payment_lang']['help_money_use'] = 'Số tiền đã sử dụng.';
$lang['payment_lang']['home'] = 'Trang chủ';
$lang['payment_lang']['fund'] = 'Ngân quỹ';
$lang['payment_lang']['recharge'] = 'Nạp tiền';
$lang['payment_lang']['request_recharge'] = 'Yêu cầu nạp tiền';
$lang['payment_lang']['month'] = 'Tháng';
$lang['payment_lang']['date'] = 'Ngày';
$lang['payment_lang']['offer'] = 'Loại giao dịch';
$lang['payment_lang']['all'] = 'Tất cả';
$lang['payment_lang']['deductione'] = 'Trừ tiền';
$lang['payment_lang']['currency'] = '(VNĐ)';
$lang['payment_lang']['transaction_code'] = 'Mã giao dịch';
$lang['payment_lang']['content'] = 'Nội dung';
$lang['payment_lang']['payment_type'] = 'Loại giao dịch';
$lang['payment_lang']['recharge_money'] = 'Số tiền nạp';
$lang['payment_lang']['deductione_money'] = 'Số tiền trừ';
$lang['payment_lang']['promotion_money'] = 'Số tiền khuyến mại';
$lang['payment_lang']['account_balance'] = 'Số dư tài khoản chính';
$lang['payment_lang']['account_promotion'] = 'Số dư tài khoản khuyến mại';
$lang['payment_lang']['info_balance'] = 'Tài khoản';
$lang['payment_lang']['info_balance_des'] = 'Số tiền khả dụng trong tài khoản của bạn. Để tăng tài khoản, bạn hãy sử dụng chức năng ngân quỹ để nạp tiền.';
$lang['payment_lang']['info_promotions'] = 'Khuyến mại';
$lang['payment_lang']['info_promotions_des'] = 'Là số tiền bạn được khuyến mại';
$lang['payment_lang']['info_overdraft'] = 'Số tiền thấu chi';
$lang['payment_lang']['info_overdraft_des'] = 'Là số tiền bạn có thể tiêu thêm khi tài khoản chính của bạn hết tiền. Số tiền bạn đã tiêu trong tài khoản thấu chi sẽ được trừ vào tài khoản chính trong lần nạp tiền tới của bạn.';
$lang['payment_lang']['info_money_use'] = 'Số tiền đã sử dụng';
$lang['payment_lang']['info_money_use_des'] = 'Là số tiền bạn đã sử dụng.';
$lang['payment_lang']['account'] = 'Tài khoản';
$lang['payment_lang']['page'] = 'Trang';
$lang['payment_lang']['mess_floot'] = 'Bạn đã làm sai quá 3 lần';

$lang['payment_lang']['hinhthuc'] = 'Chọn hình thức thanh toán';
$lang['payment_lang']['next'] = 'Tiếp tục';
$lang['payment_lang']['select_pay'] = 'Hãy chọn hình thức thanh toán';
$lang['payment_lang']['pay_internet_baking'] = '<b>Thanh toán online dùng Visa, Master Card, thẻ ATM, tài khoản có Internet Banking</b><br />Thanh toán nhanh gọn và có thể mua hàng hoặc sử dụng ngay dịch vụ sau khi thanh toán.';
$lang['payment_lang']['pay_internet_comment'] = '<ul><li>Không mất phí thanh toán.</li><li>Thẻ của Quý khách phải được kích hoạt chức năng thanh toán trực tuyến hoặc đã đăng ký Internet Banking.</li><li>Thanh toán nhanh gọn và có thể sử dụng dịch vụ ngay sau khi thanh toán.</li></lu>';
$lang['payment_lang']['pay_card_mobile'] = '<b>Thanh toán bằng thẻ cào điện thoại</b>';
$lang['payment_lang']['select_card_type'] = 'Hãy chọn loại thẻ cào';
$lang['payment_lang']['enter_card_number'] = 'Hãy nhập số thẻ cào';
$lang['payment_lang']['card_number_invalid'] = 'Số thẻ cào phải là dạng số';
$lang['payment_lang']['enter_phone'] = 'Bạn hãy nhập số điện thoại';
$lang['payment_lang']['phone_invalid'] = 'Số điện thoại phải là dạng số';
$lang['payment_lang']['enter_fullname'] = 'Bạn hãy nhập Họ Tên';
$lang['payment_lang']['enter_email'] = 'Bạn hãy nhập địa chỉ Email';
$lang['payment_lang']['use_sohapay'] = '<p>Chúng tôi sử dụng  dịch vụ thanh toán của cổng thanh toán <b>muachungpay</b>.</p><p>Xem kỹ điều khoản sử dụng dịch vụ tại <a class="underline" href="https://muachungpay.vn/info/help/quy-dinh-chinh-sach/dieu-khoan-su-dung.html" target="_blank"><b>đây</b></a>.</p>';
$lang['payment_lang']['pay_way'] = 'Các phương thức thanh toán';
$lang['payment_lang']['pay_way_content'] = '<ul class="square text-666">
						<li class="martop5">
							Thanh toán trực tuyến bằng thẻ tín dụng, thẻ ghi nợ, thẻ trả trước mang thương hiệu Quốc tế Visa và Mastercard.
						</li>
						
						<li class="martop5">
							Thanh toán bằng các loại thẻ nội địa - ATM do các ngân hàng nội địa phát hành: Vietcombank, VietinBank, VIB, HDBank, TienPhongBank.
						</li>
						
						<li class="martop5">
							Thanh toán bằng Internet Banking của các ngân hàng Đông Á, Techcombank.
						</li>
					</ul>';
$lang['payment_lang']['pay_value'] = 'Giá trị thanh toán';
$lang['payment_lang']['order_info'] = 'Thông tin đơn hàng';
$lang['payment_lang']['pay'] = 'Thanh toán';
$lang['payment_lang']['type_card'] = 'Loại thẻ cào';
$lang['payment_lang']['mess_invalid'] = 'Hãy nhập thông tin đơn hàng';
$lang['payment_lang']['number_phone'] = 'Số điện thoại';
$lang['payment_lang']['number_card'] = 'Số thẻ cào';
$lang['payment_lang']['close'] = 'Đóng';
$lang['payment_lang']['pay_detail'] = 'Chi tiết giao dịch';
$lang['payment_lang']['detail'] = 'Chi tiết';
$lang['payment_lang']['print'] = 'In';
$lang['payment_lang']['pay_date'] = 'Ngày giao dịch';
$lang['payment_lang']['pay_sum_money'] = 'Tổng số tiền giao dịch';
$lang['payment_lang']['pay_state'] = 'Trạng thái giao dịch';
$lang['payment_lang']['date_detail'] = 'Chi tiết theo ngày';
$lang['payment_lang']['toform'] = 'Cho tất cả các quảng cáo tính từ <b>0h</b> ngày <b>%s</b> đến <b>0h</b> ngày <b>%s</b>';
$lang['payment_lang']['ad_code'] = 'Mã quảng cáo';
$lang['payment_lang']['ad_name'] = 'Tên quảng cáo';
$lang['payment_lang']['money_num'] = 'Số tiền';
$lang['payment_lang']['sum'] = 'Tổng cộng';
$lang['payment_lang']['money_date_list'] = 'Danh sách thanh toán tiền trong ngày';
$lang['payment_lang']['ad'] = 'Quảng cáo';
$lang['payment_lang']['money_payed'] = 'Số tiền đã thanh toán';
$lang['payment_lang']['email_subject_cod'] = 'Chuyển khoản (Thu tiền tận nơi)';
$lang['payment_lang']['email_content_cod'] = '%s vừa lựa chọn hình thức Chuyển khoản (hoặc Thu tiền tận nơi) với số tiền là %sđ vào hệ thống AdMarket.
		<br /><br />
		Hệ thống AdMarket sẽ giúp bạn quảng cáo trúng đích với chi phí tốt nhất. Khi bạn gia nhập vào hệ thống AdMarket, bạn có thể quảng cáo sản phẩm của bạn đến hơn 25 triệu độc giả.<br /><br />
		Cám ơn bạn,<br /><br />
		Hệ thống quảng cáo AdMarket, phòng Adtech – AdMicro, công ty cổ phần truyền thông Việt Nam.
';
$lang['payment_lang']['email_subject_pay'] = 'Cập nhật tài khoản';
$lang['payment_lang']['email_content_pay'] = 'Chào %s,<br /><br />
		Bạn vừa nạp %s vào tài khoản trong hệ thống AdMarket thông qua hệ thống thanh toán trực tuyến MuachungPay. Bạn có thể sử dụng số tiền đã nạp để mua các quảng cáo trên hệ thống.
		<br /><br />
		Để xem chi tiết thông tin tài khoản, bạn hãy kích vào liên kết dưới đây để đăng nhập hệ thống:<br />%s		
		<br /><br />
		Hệ thống AdMarket sẽ giúp bạn quảng cáo trúng đích với chi phí tốt nhất. Khi bạn gia nhập vào hệ thống AdMarket, bạn có thể quảng cáo sản phẩm của bạn đến hơn 25 triệu độc giả.<br /><br />
		Cám ơn bạn,<br /><br />
		Hệ thống quảng cáo AdMarket, phòng Adtech – AdMicro, công ty cổ phần truyền thông Việt Nam.
';
$lang['payment_lang']['error_price'] = 'Giá trị thanh toán không hợp lệ, thấp nhất là 50,000 VND.';
$lang['payment_lang']['fullname'] = 'Họ tên';
$lang['payment_lang']['username'] = 'Tên tài khoản';
$lang['payment_lang']['tel'] = 'Tel';
$lang['payment_lang']['email'] = 'Email';
$lang['payment_lang']['phone'] = 'Điện thoại';
$lang['payment_lang']['sum_before_tax'] = 'Tổng tiền trước thuế';
$lang['payment_lang']['sum_after_tax'] = 'Tổng tiền sau thuế';
$lang['payment_lang']['tax_vat'] = 'Tiền thuế (VAT 10%)';
$lang['payment_lang']['transaction'] = 'Giao dịch';
$lang['payment_lang']['customer'] = 'Khách hàng';
$lang['payment_lang']['num_order'] = 'Số hóa đơn';
$lang['payment_lang']['order'] = 'Hóa đơn';
$lang['payment_lang']['head_order'] = '<p>
							Công ty cổ phần truyền thông Việt Nam
						</p>
						<p>
							Tầng 22 tháp B Vincom 191b Bà Triệu, Hai Bà Trưng, Hà Nội
						</p>
						<p>
							Mã số thuế. 0101871229
						</p>';
$lang['payment_lang']['address_home_cold'] = 'Số nhà, Đường/Phố, Phường/Xã';
$lang['payment_lang']['curency'] = 'đ';
$lang['payment_lang']['province'] = 'Tỉnh/Thành phố';
$lang['payment_lang']['districts'] = 'Quận/Huyện';
$lang['payment_lang']['ship_cost'] = 'Phí';
$lang['payment_lang']['money_cost'] = 'Số tiền cần thu';
$lang['payment_lang']['comment_cold'] = 'Thời gian, Ghi chú thêm';
$lang['payment_lang']['comment_cold_des'] = 'Vui lòng cho AdMarket biết thời gian có thể đến thu tiền hoặc yêu cầu thêm nếu có (Lưu ý: AdMarket không thu vào buổi tối)';
$lang['payment_lang']['pay_cold'] = '<b>Thu tiền tận nơi</b> <span style="color:red">(Có tính phí)</span>';
$lang['payment_lang']['pay_cold_des'] = 'Trong thời gian từ 2-7 ngày làm việc, nhân viên AdMarket sẽ đến tận nơi thu tiền của Quý khách.';
$lang['payment_lang']['continue'] = 'Tiếp theo';
$lang['payment_lang']['enter_cost'] = 'Bạn hãy nhập Số tiền (tối thiểu '.number_format(SHIP_COST_MIN).$lang['payment_lang']['curency'].')';
$lang['payment_lang']['enter_cold_address'] = 'Bạn hãy nhập '.$lang['payment_lang']['address_home_cold'];
$lang['payment_lang']['enter_cold_province'] = 'Bạn hãy nhập '.$lang['payment_lang']['province'];
$lang['payment_lang']['enter_cold_district'] = 'Bạn hãy nhập '.$lang['payment_lang']['districts'];
$lang['payment_lang']['enter_cold_comment'] = 'Bạn hãy nhập '.$lang['payment_lang']['comment_cold'];
$lang['payment_lang']['custom_info'] = 'Thông tin khách hàng';
$lang['payment_lang']['address'] = 'Địa chỉ';
$lang['payment_lang']['back'] = 'Quay lại';
$lang['payment_lang']['type_pay'] = 'Hình thức thanh toán';
$lang['payment_lang']['type_pay_value'] = 'Nộp tiền tận nơi';
$lang['payment_lang']['type_pay_transfer'] = 'Chuyển khoản thanh toán qua ATM hoặc nộp tiền tại quầy GD ngân hàng về:';
$lang['payment_lang']['order_info'] = 'Thông tin hóa đơn';
$lang['payment_lang']['money_add'] = 'Số tiền cần nộp';
$lang['payment_lang']['sum_money_add'] = 'Tổng tiền cần nộp';
$lang['payment_lang']['pay_cold_success'] = '<b>THÔNG TIN HÓA ĐƠN ĐÃ GỬI THÀNH CÔNG.</b><br />AdMarket sẽ liên hệ với bạn trong thời gian sớm nhất, trân trọng cảm ơn!';
$lang['payment_lang']['transfer_pay_label'] = '<b>Chuyển khoản qua máy ATM hoặc tại Ngân hàng</b> <span style="color:red">(Quý khách tự thanh toán chi phí chuyển khoản)</span>';
$lang['payment_lang']['transfer_pay_des'] = 'Quý khách chuyển tiền vào tài khoản của AdMarket. Quý khách phải chờ từ <b>4-24</b> giờ để AdMarket xác nhận thông tin.';
$lang['payment_lang']['bank_transfer_free'] = 'Quý khách vui lòng tự thanh toán chi phí Chuyển khoản';
$lang['payment_lang']['bank_info_vcb'] = '<div class="fl"><div class="vc_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 0021.0018.392.43<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng Vietcombank (VCB) Hà Nội</strong></p></div></div>';
$lang['payment_lang']['bank_info_dab'] = '<div class="fl"><div class="donga_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 0023.&shy;8734.0001<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng Đông Á chi nhánh Bạch Mai - Hà Nội</strong></p></div></div>';
$lang['payment_lang']['bank_info_tcb'] = '<div class="fl"><div class="techcom_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 1032.0141.354.011<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng Techcombank CN Bà Triệu - Hà Nội</strong></p></div></div>';
$lang['payment_lang']['bank_info_agb'] = '<div class="fl"><div class="agribank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 1483.2010.047.40<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng AGRIBANK chi nhánh Thủ Đô</strong></p></div></div>';
$lang['payment_lang']['bank_info_bidv'] = '<div class="fl"><div class="bidv_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 1201.0000.318.895<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng BIDV Sở Giao dịch</strong></p></div></div>';
$lang['payment_lang']['bank_info_vtb'] = '<div class="fl"><div class="vietinbank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 1020.1000.1108.169<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng Công thương Việt Nam chi nhánh Hai Bà Trưng</strong></p></div></div>';
$lang['payment_lang']['bank_info_mbb'] = '<div class="fl"><div class="mb_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 0651.1000.160.02<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng thương mại cổ phần Quân đội - CN Hai Bà Trưng (MB)</strong></p></div></div>';
$lang['payment_lang']['bank_info_acb'] = '<div class="fl"><div class="acb_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong> 3718.2319<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng Á Châu chi nhánh Hà Nội (ACB)</strong></p></div></div>';
$lang['payment_lang']['bank_info_vib'] = '<div class="fl"><div class="vib_bank"></div></div><div class="fl"><div class="mTop10"><p><strong>- Số TK:</strong>  001704060035755<br><strong>- Chủ tài khoản:</strong> Công ty Cổ phần Truyền thông Việt Nam<br><strong>- Ngân hàng TMCP Quốc tế Việt Nam (VIB)</strong></p></div></div>';
$lang['payment_lang']['bank_go'] = 'Chọn ngân hàng khác';
$lang['payment_lang']['bank_title'] = 'Chọn ngân hàng của AdMarket mà Quý khách sẽ chuyển tiền vào';
$lang['payment_lang']['bank_des'] = '<ul style="padding-left:15px"><li>Khi chuyển khoản qua Internet Banking hoặc Quầy giao dịch, Quý khách vui lòng tự chịu phí chuyển khoản.</li><li>Sau khi chuyển khoản, Quý khách vui lòng thông báo cho AdMarket được biết</li><li>Đơn hàng của Quý khách chỉ được chấp nhận khi tiền đã về tài khoản của AdMarket</li></ul>';
$lang['payment_lang']['cold_des'] = '<ul style="padding-left:15px"><li>Quý khách sẽ mất phí khi chọn hình thức này.</li><li>Nhân viên thu tiền của AdMarket sẽ liên hệ với Quý khách trước khi thu.</li><li>Quý khách chỉ được sử dụng dịch vụ sau khi đã thanh toán tiền cho nhân viên AdMarket.</li></ul>';
$lang['payment_lang']['enter_bank_transfer'] = 'Quý khách chưa chọn ngân hàng để chuyển tiền vào.';
$lang['payment_lang']['status'] = 'Trạng thái';
$lang['payment_lang']['arr_status'] = array('0'=>'Chờ/Lỗi', '1'=>'Thành công');
//Move money
$lang['payment_lang']['move_money'] = 'Chuyển tiền';
$lang['payment_lang']['recieve_account'] = 'Tài khoản nhận';
$lang['payment_lang']['select_user'] = 'Hãy chọn một tài khoản nhận tiền.';
$lang['payment_lang']['cancel'] = 'Hủy bỏ';
$lang['payment_lang']['option'] = 'Tùy chọn';
$lang['payment_lang']['amount'] = 'Số tiền';
$lang['payment_lang']['enter_amount'] = 'Hãy nhập Số tiền cần chuyển.';
$lang['payment_lang']['type_balance'] = 'Loại tiền';
$lang['payment_lang']['money_balance'] = 'Tiền tài khoản chính';
$lang['payment_lang']['money_promotion'] = 'Tiền tài khoản khuyến mại';
$lang['payment_lang']['move_money_not_success'] = 'Không có quyền hoặc không đủ tiền để chuyển. Chuyển tiền không thành công!';
$lang['payment_lang']['move_money_success'] = 'Yêu cầu chuyển tiền đã thành công!';
$lang['payment_lang']['email_content_move_money'] = 'Chào [username],<br /><br />
		Bạn vừa được chuyển [amount]đ vào tài khoản [balance_type] qua kênh đại lý trong hệ thống AdMarket. Bạn có thể sử dụng số tiền đã có để mua các quảng cáo trên hệ thống.
		<br /><br />
		Để xem chi tiết thông tin tài khoản, bạn hãy kích vào liên kết dưới đây để đăng nhập hệ thống:<br />[login]		
		<br /><br />
		Hệ thống AdMarket sẽ giúp bạn quảng cáo trúng đích với chi phí tốt nhất. Khi bạn gia nhập vào hệ thống AdMarket, bạn có thể quảng cáo sản phẩm của bạn đến hơn 25 triệu độc giả.<br /><br />
		Cám ơn bạn,<br /><br />
		Hệ thống quảng cáo AdMarket, phòng Adtech – AdMicro, công ty cổ phần truyền thông Việt Nam.
';
$lang['payment_lang']['label_balance'] = 'Tài khoản chính';
$lang['payment_lang']['label_promotion'] = 'Tài khoản khuyến mại';
$lang['payment_lang']['user_manager'] = 'Quản lý người dùng';
//End