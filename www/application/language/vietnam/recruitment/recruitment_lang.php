<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['recruitment_lang'] = array();
$lang['recruitment_lang']['index_title'] = 'Tuyển dụng nhân sự';
$lang['recruitment_lang']['index_meta_title'] = 'tuyển dụng, nhân sự, kinh doanh, trợ lý dự án, trưởng phòng, chăm sóc khách hàng, chuyên viên nội dung, phát triển thị trường';
$lang['recruitment_lang']['index_meta_keyword'] = 'tuyển dụng, nhân sự, kinh doanh, trợ lý dự án, trưởng phòng, chăm sóc khách hàng, chuyên viên nội dung, phát triển thị trường';
$lang['recruitment_lang']['index_meta_desc'] = 'tuyển dụng nhân sự, kinh doanh, trợ lý dự án, trưởng phòng, chăm sóc khách hàng, chuyên viên nội dung, phát triển thị trường';