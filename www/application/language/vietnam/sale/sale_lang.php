<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['sale'] = array();
$lang['sale']['home'] = 'Trang chủ';
$lang['sale']['sale'] = 'Sale';
$lang['sale']['budget_manager'] = 'Quản lý tài chính';
$lang['sale']['user_manager'] = 'Quản lý Khách hàng';
$lang['sale']['budget_manager'] = 'Quản lý Tài chính';
$lang['sale']['custom_manager'] = 'Quản lý khách hàng';
$lang['sale']['username'] = 'Tài khoản';
$lang['sale']['fullname'] = 'Họ tên';
$lang['sale']['address'] = 'Địa chỉ';
$lang['sale']['email'] = 'Email';
$lang['sale']['phone'] = 'Điện thoại';
$lang['sale']['create_new'] = 'Tạo khách hàng';
$lang['sale']['user_info'] = 'Thông tin tài khoản';
$lang['sale']['btn_new'] = 'Tạo mới';
$lang['sale']['btn_save'] = 'Lưu';
$lang['sale']['login_info'] = 'Thông tin đăng nhập';
$lang['sale']['password'] = 'Mật khẩu';
$lang['sale']['re_password'] = 'Gõ lại Mật khẩu';
$lang['sale']['enter_username'] = 'Bạn chưa nhập Tên đăng nhập.';
$lang['sale']['username_wrong_length'] = 'Tên đăng nhập phải trên 4 và nhỏ hơn 64 ký tự';
$lang['sale']['username_contains_illegal'] = 'Tên đăng nhập chứa ký tự không hợp lệ';
$lang['sale']['not_available'] = 'Tên đăng nhập đã tồn tại trên hệ thống.';
$lang['sale']['enter_username'] = 'Bạn chưa nhập Tên đăng nhập.';
$lang['sale']['password_not_match'] = 'Mật khẩu không trùng khớp, bạn hãy thử lại.';
$lang['sale']['password_wrong_length'] = 'Mật khẩu quá ngắn, phải trên 4 và dưới 64 ký tự.';
$lang['sale']['enter_email'] = 'Bạn hãy nhập Email.';
$lang['sale']['email_invalid'] = 'Địa chỉ Email không hợp lệ.';
$lang['sale']['email_not_available'] = 'Địa chỉ Email đã tồn tại trên hệ thống.';
$lang['sale']['enter_fullname'] = 'Bạn hãy nhập Họ tên.';
$lang['sale']['enter_phone'] = 'Bạn hãy nhập Điện thoại.';
$lang['sale']['create_custom_success'] = 'Tài khoản đã được lưu thành công.';
$lang['sale']['expense'] = 'Tiền đã tiêu';
$lang['sale']['earn'] = 'Tiền kiếm được';
$lang['sale']['earn_for_sale'] = 'Tài chính của Sale';
$lang['sale']['date'] = 'Ngày';
$lang['sale']['page'] = 'Trang';
$lang['sale']['move_money'] = 'Chuyển tiền';
$lang['sale']['amount'] = 'Số tiền';
$lang['sale']['type_balance'] = 'Kiểu tài khoản';
$lang['sale']['money_balance'] = 'Tài khoản chính';
$lang['sale']['money_promotion'] = 'Tài khoản Khuyến mãi';
$lang['sale']['money_job'] = 'Tài khoản Lương';
$lang['sale']['recieve_account'] = 'Tài khoản nhận tiền';
$lang['sale']['content'] = 'Mô tả';
$lang['sale']['option'] = 'tùy chọn';
$lang['sale']['cancel'] = 'Hủy bỏ';
$lang['sale']['enter_amount'] = 'Hãy nhập số tiền cần chuyển';
$lang['sale']['select_user'] = 'Hãy chọn Tài khoản';
$lang['sale']['confirm_move_money'] = 'Xác nhận chuyển tiền';
$lang['sale']['email_subject_pay'] = 'Cập nhật tài khoản';
$lang['sale']['email_content_move_money'] = 'Chào [username],<br /><br />
		Bạn vừa được chuyển [amount]đ vào tài khoản [balance_type] qua kênh đại lý trong hệ thống AdMarket. Bạn có thể sử dụng số tiền đã có để mua các quảng cáo trên hệ thống.
		<br /><br />
		Để xem chi tiết thông tin tài khoản, bạn hãy kích vào liên kết dưới đây để đăng nhập hệ thống:<br />[login]		
		<br /><br />
		Hệ thống AdMarket sẽ giúp bạn quảng cáo trúng đích với chi phí tốt nhất. Khi bạn gia nhập vào hệ thống AdMarket, bạn có thể quảng cáo sản phẩm của bạn đến hơn 25 triệu độc giả.<br /><br />
		Cám ơn bạn,<br /><br />
		Hệ thống quảng cáo AdMarket, phòng Adtech – AdMicro, công ty cổ phần truyền thông Việt Nam.
';
$lang['sale']['move_money_success'] = 'Yêu cầu chuyển tiền đã thành công!';
$lang['sale']['move_money_not_success'] = 'Không có quyền hoặc không đủ tiền để chuyển. Chuyển tiền không thành công!';
$lang['sale']['sale_earn'] = 'Tiền Sale được hưởng';
$lang['sale']['sum'] = 'Tổng';
$lang['sale']['customer'] = 'Khách hàng';
$lang['sale']['block'] = 'Khóa';
$lang['sale']['no_block'] = 'Hoạt động';
$lang['sale']['deleted'] = 'Đã xóa';
$lang['sale']['unactive'] = 'Chưa kích hoạt';