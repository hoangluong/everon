<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['setting_lang'] = array();
$lang['setting_lang']['home'] = 'Trang chủ';
$lang['setting_lang']['account'] = 'Tài khoản';
$lang['setting_lang']['setting'] = 'Thiết lập';
$lang['setting_lang']['email'] = 'Email';
$lang['setting_lang']['email_monthly'] = 'Nhận Email hàng tháng';
$lang['setting_lang']['email_report_ad'] = 'Nhận Email tổng kết quảng cáo';
$lang['setting_lang']['email_promotion'] = 'Nhận Email khuyến mãi';
$lang['setting_lang']['email_approved_ad'] = 'Nhận Email duyệt quảng cáo';
$lang['setting_lang']['saved'] = 'Thiết lập đã được lưu.';