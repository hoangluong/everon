<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['suser_lang'] = array();
$lang['suser_lang']['site_title'] = 'Chuyển đổi user';
$lang['suser_lang']['input_username_default'] = 'Nhập username';
$lang['suser_lang']['list_user_title'] = 'Danh sách người dùng';
$lang['suser_lang']['username_title'] = 'Username';
$lang['suser_lang']['fullename_title'] = 'Tên người dùng';
$lang['suser_lang']['action'] = 'Thao tác';
$lang['suser_lang']['page'] = 'Trang';
$lang['suser_lang']['access_denied'] = 'Bạn không có quyền truy cập chức năng này.';
$lang['suser_lang']['base_user'] = 'Chuyển lại user ban đầu';
$lang['suser_lang']['search_user'] = 'Tìm kiếm';
$lang['suser_lang']['search_keyword'] = 'Nhập từ khóa tìm kiếm';
$lang['suser_lang']['all_user'] = 'Tất cả user';
$lang['suser_lang']['switch_btn'] = 'Chuyển';
$lang['suser_lang']['sby_uname'] = 'Theo username';
$lang['suser_lang']['sby_fname'] = 'Theo họ và tên';
$lang['suser_lang']['filter'] = 'Lọc theo nhóm';