<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	$lang['user_lang'] = array();
	$lang['user_lang']['profile_title'] 												= 'Thông tin cá nhân';
	$lang['user_lang']['home_title'] 													= 'Trang chủ';
	$lang['user_lang']['change_pass_title'] 											= 'Thay đổi mật khẩu';
	$lang['user_lang']['old_pass_title'] 												= 'Mật khẩu hiện tại';
	$lang['user_lang']['new_pass_title'] 												= 'Mật khẩu mới';
	$lang['user_lang']['retype_new_pass_title'] 										= 'Gõ lại mật khẩu mới';
	$lang['user_lang']['update_btn'] 													= 'Cập nhật';
	$lang['user_lang']['email_title'] 													= 'Email';
	$lang['user_lang']['fullname_title'] 												= 'Họ và tên';
	$lang['user_lang']['phone_title'] 													= 'Số điện thoại';
	$lang['user_lang']['address_title'] 												= 'Địa chỉ';
	$lang['user_lang']['old_pass_empty_error'] 											= 'Bạn chưa nhập mật khẩu cũ.';
	$lang['user_lang']['invalid_old_pass'] 												= 'Mật khẩu cũ không đúng.';
	$lang['user_lang']['new_pass_error'] 												= 'Bạn chưa nhập mật khẩu mới hoặc mật khẩu mới không trùng khớp.';
	$lang['user_lang']['change_pass_ok'] 												= 'Cập nhật password thành công!';
	$lang['user_lang']['change_info_ok'] 												= 'Cập nhật thông tin thành công!';
	$lang['user_lang']['pass_length_error'] 											= 'Mật khẩu quá ngắn. Tối thiểu 6 ký tự';