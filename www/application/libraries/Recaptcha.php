<?php

/**
 * Created by PhpStorm.
 * User: AnhNguyen
 * Date: 10/12/15
 * Time: 5:15 PM
 */
class Recaptcha
{

    private $_CI;
    private $_ppk = null;
    private $_pub = null;
    private $_error = null;
    private $_endpoint = 'https://www.google.com/recaptcha/api/siteverify';

    public function __construct()
    {
        $this->_CI =& get_instance();
 

        $this->_pub = '6Lf-2A4TAAAAAMzgnWvhv1sZV5gJOrwvi4vdosTu';
        $this->_ppk = '6Lf-2A4TAAAAAJJJhck1Hy2SQRY5eoDZSSwGv9hN';
    }

    public function getCaptcha($size = '')
    {
        return "<script src='https://www.google.com/recaptcha/api.js'></script><div class=\"g-recaptcha\" data-sitekey=\"{$this->_pub}\" " . ($size != '' ? "data-size=\"$size\"" : '') . "></div>";
    }

    public function verify()
    {

        $response = $this->_CI->input->post('g-recaptcha-response', TRUE);

        try {

            $process = curl_init($this->_endpoint);
            curl_setopt($process, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
            curl_setopt($process, CURLOPT_HEADER, FALSE);
            curl_setopt($process, CURLOPT_HTTPHEADER, array(
                'Accept: application/json'
            ));
            curl_setopt($process, CURLOPT_POST, 1);
            curl_setopt($process, CURLOPT_POSTFIELDS, http_build_query(array(
                'secret'   => $this->_ppk,
                'response' => $response
            )));

            curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
            $return = curl_exec($process);
            curl_close($process);

            $return = json_decode($return);
            if($return->success) return true;

            $this->_error = $return->{'error-codes'};

        } catch (Exception $ex) {
            return false;
        }
    }

    public function getError()
    {
        return $this->_error;
    }

}