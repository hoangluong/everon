﻿<?php


/*

 * To change this template, choose Tools | Templates

 * and open the template in the editor.

 * 

 */

class common

{

    function __construct()
    {

        // parent::__construct();

        $this->_CIA = &get_instance();

        // $this->_CIA->load->model('product/product_model');

        $this->_CIA->load->library('layout');


    }


    function dataSites()
    {
        return array('', 'dantri.com.vn', 'afamily.vn', 'kenh14.vn', 'genk.vn', 'gamek.vn', 'vneconomy.vn', 'cafef.vn', 'cafebiz.vn', 'giadinh.net.vn', 'autopro.com.vn', 'soha.vn', 'suckhoedoisong.vn');
    }

    public function loadMail()

    {

        $this->_CIA->config->load('email');

        $config = $this->_CIA->config->item('mail');

        $this->_CIA->load->library('PHPMailer/phpmailer');

        $this->_CIA->phpmailer->IsSMTP();

        $this->_CIA->phpmailer->SMTPAuth = true;

        $this->_CIA->phpmailer->SMTPSecure = $config['SMTPSecure'];

        $this->_CIA->phpmailer->Host = $config['host'];

        $this->_CIA->phpmailer->Port = $config['port'];

        $this->_CIA->phpmailer->Username = $config['username'];

        $this->_CIA->phpmailer->Password = $config['password'];

        $this->_CIA->phpmailer->SetFrom($config['username'], $config['contentFrom']);

        $this->_CIA->phpmailer->CharSet = $config['charset'];

    }

    /*End*/

    function VndText($amount)

    {

        if ($amount <= 0) {

            return $textnumber = "False";

        }

        $Text = array("không", "một", "hai", "ba", "bốn", "năm", "sáu", "bẩy", "tám", "chín");

        $TextLuythua = array("", "nghìn", "triệu", "tỷ", "ngàn tỷ", "triệu tỷ", "tỷ tỷ");

        $textnumber = "";

        $length = strlen($amount);


        for ($i = 0; $i < $length; $i++)

            $unread[$i] = 0;


        for ($i = 0; $i < $length; $i++) {

            $so = substr($amount, $length - $i - 1, 1);


            if (($so == 0) && ($i % 3 == 0) && ($unread[$i] == 0)) {

                for ($j = $i + 1; $j < $length; $j++) {

                    $so1 = substr($amount, $length - $j - 1, 1);

                    if ($so1 != 0)

                        break;

                }


                if (intval(($j - $i) / 3) > 0) {

                    for ($k = $i; $k < intval(($j - $i) / 3) * 3 + $i; $k++)

                        $unread[$k] = 1;

                }

            }

        }


        for ($i = 0; $i < $length; $i++) {

            $so = substr($amount, $length - $i - 1, 1);

            if ($unread[$i] == 1)

                continue;


            if (($i % 3 == 0) && ($i > 0))

                $textnumber = $TextLuythua[$i / 3] . " " . $textnumber;


            if ($i % 3 == 2)

                $textnumber = 'trăm ' . $textnumber;


            if ($i % 3 == 1)

                $textnumber = 'mươi ' . $textnumber;


            $textnumber = $Text[$so] . " " . $textnumber;

        }


        //Phai de cac ham replace theo dung thu tu nhu the nay

        $textnumber = str_replace("không mươi", "lẻ", $textnumber);

        $textnumber = str_replace("lẻ không", "", $textnumber);

        $textnumber = str_replace("mươi không", "mươi", $textnumber);

        $textnumber = str_replace("một mươi", "mười", $textnumber);

        $textnumber = str_replace("mươi năm", "muoi lăm", $textnumber);

        $textnumber = str_replace("mươi một", "mươi mốt", $textnumber);

        $textnumber = str_replace("mười năm", "mười lăm", $textnumber);


        return ucfirst($textnumber . " đồng chẵn");

    }


    function clear_sumbol($text)
    {

        if (!$text) return false;

        $text = strtolower($text);

        $text = str_replace("à", "a", $text);

        $text = str_replace("á", "a", $text);

        $text = str_replace("â", "a", $text);

        $text = str_replace("ợ", "o", $text);

        $text = str_replace("ư", "u", $text);

        return $text;

    }


    function locdau($str)
    {

        $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",

            "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề"

        , "ế", "ệ", "ể", "ễ",

            "ì", "í", "ị", "ỉ", "ĩ",

            "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ"

        , "ờ", "ớ", "ợ", "ở", "ỡ",

            "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",

            "ỳ", "ý", "ỵ", "ỷ", "ỹ",

            "đ",

            "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă"

        , "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",

            "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",

            "Ì", "Í", "Ị", "Ỉ", "Ĩ",

            "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ"

        , "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",

            "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",

            "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",

            "Đ",

            "“", "”", "&", " ", ",", "'", ")", "(", "/", "%", "|", "]", "[", "$", "'"

        );


        $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"

        , "a", "a", "a", "a", "a", "a",

            "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",

            "i", "i", "i", "i", "i",

            "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o"

        , "o", "o", "o", "o", "o",

            "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",

            "y", "y", "y", "y", "y",

            "d",

            "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A"

        , "A", "A", "A", "A", "A",

            "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",

            "I", "I", "I", "I", "I",

            "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O"

        , "O", "O", "O", "O", "O",

            "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",

            "Y", "Y", "Y", "Y", "Y",

            "D",

            "", "", "-", "-", "-", "", "", "", "-", "", "", "", "", "$", "");

        $a = str_replace($marTViet, $marKoDau, $str);

        return ($a);

    }

    function genCode($id){
        $code = 'SP';
        if($id<10){
            $code.= '000'.$id;
        }elseif($id<100){
            $code.= '00'.$id;
        }elseif($id<1000){
            $code.= '0'.$id;
        }else{
            $code.=$id;
        }
        return $code;
    }
}


?>

