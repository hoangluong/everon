<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
 
class layout 
{
    protected static $files = array();
    protected static $noPackJS = array();
    var $obj;
    var $layout;

    function layout($layout = "")
    {   
        
     //   echo $layout;
        //$this->obj =& get_instance();
        $this->obj = & get_instance();
        $this->ci = $this->obj;
        $this->load = $this->obj->load;
        $this->layout = $layout;
    }

    function setLayout($layout)
    {
      $this->layout = $layout;
    }

    function view($view, $data=null, $return=false)
    {
        $loadedData = array();
        $loadedData['content_for_layout'] = $this->obj->load->view($view,$data,true);

        if($return)
        {
            $output = $this->obj->load->view($this->layout, $loadedData, true);
            return $output;
        }
        else
        {
            $this->obj->load->view($this->layout, $loadedData, false);
        }
    }
    
}