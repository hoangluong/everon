<?php
ob_start();
require_once(dirname(__FILE__) . '/mpdf/mpdf.php');

class mpdfreport {

    function process_images($text) {
        $html = $text;
        $pattern = '/<img[^>]+\>/i';
        $matches = '';
        //Xoa cac text truoc <img de lay dung
        if (substr($text, 0, 4) != '<img') {
            $index = strpos($text, '<img');
            $text = substr($text, $index - 1, strlen($text) - $index);
        }

        preg_match_all($pattern, $text, $matches);

        foreach ($matches[0] as $img) {
            $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
            $matches2 = '';
            preg_match_all($pattern, $img, $matches2);
            $img_src = $matches2[1][0];
            if (!$this->image_remote_exists($img_src))
                $html = str_replace($img, '', $html);
        }
        return $html;
    }

    function image_remote_exists($url) {
        if (@fclose(@fopen(urldecode($url), 'r')))
            return true;
        else
            return false;
    }

    function writeHtml($html, $filename = '', $ck = true) {
        /* $pdf = new MYPDF('utf-8',    // mode - default ''
          'A4',    // format - A4, '' ,//for example, default ''
          '',     // font size - default 0
          'arial',    // default font family
          20,    // margin_left
          20,    // margin right
          10,     // margin top
          10,    // margin bottom
          10,     // margin header
          10     // margin footer
          );  // L - landscape, P - portrait
         */





        if ($ck) {
            $pdf = new mPDF('utf-8', 'A4', '', 'Arial', 10, 10, 20, 20, 0, 10);

            $pdf->mirrorMargins = 1;


            // set font
            $pdf->SetFont('Arial', '', 8);
            //$pdf->SetFont('freeserifi', '', 10);
            $header = '<div style="background:#f7f7f7;border-bottom:1px solid #ccc;height:50px;" ><p align="left" style="font-weight: inherit; font-size: 17px;">' . STATIC_DOMAIN . '</p></div>';
            $pdf->SetHTMLHeader($header);
            $pdf->SetHTMLHeader($header, 'E');

            $footer = '<table style="border-bottom:5px solid #ccc;" cellpadding="3" width="100%" cellspacing="0"><tr><td width="145" valign="top" style="font-size:11px;"><ul><li><span color="#ff9900">W</span>: www.admicro.vn</li><li><span color="#ff9900">E</span>: contact@admicro.vn</li></ul></td><td width="180" valign="top" style="font-size:11px;">     	<ul><li><span color="#ff9900">T</span>: (84 4) 3974 3410 ext: 843</li><li><span color="#ff9900">F</span>: (84 4) 3974 3413</li></ul></td><td width="280" valign="top" style="font-size:11px;"><ul><li><span color="#ff9900">A</span>: Level 22, Vincom B Tower, 191 Ba Trieu, Hai Ba Trung Dist, Ha Noi</li></ul></td><td width="40" valign="top"><img src="' . base_url() . 'app/libraries/tcpdf/images/Logo_PDF.jpg" border="0"/></td></tr></table>';
            $pdf->SetHTMLFooter($footer);
            $pdf->SetHTMLFooter($footer, 'E');
        } else {
            $pdf = new mPDF('utf-8', 'A4', '', 'Arial', 0, 0, 0, 0, 0, 0);
        }


        $pdf->writeHTML($this->process_images($html));

        if ($filename == '') {
            $pdf->Output();
        } else {
            $pdf->Output($filename);
        }
        //Close and output PDF document
    }

    function writeHtml1($html, $filename = 'adsapp.pdf', $mod="F") {
        /* $pdf = new MYPDF('utf-8',    // mode - default ''
          'A4',    // format - A4, '' ,//for example, default ''
          '',     // font size - default 0
          'arial',    // default font family
          20,    // margin_left
          20,    // margin right
          10,     // margin top
          10,    // margin bottom
          10,     // margin header
          10     // margin footer
          );  // L - landscape, P - portrait
         */

        $pdf = new mPDF('utf-8', 'A4', '', '', 10, 10, 10, 20, 2, 10,'P');

        $pdf->mirrorMargins = 1;
 
     
        //$pdf->SetFont('freeserifi', '', 10);
 $pdf->writeHTML($this->process_images($html));
   
        //Close and output PDF document
        //$pdf->Output();
        $pdf->Output($filename, $mod);
    }

}

?>