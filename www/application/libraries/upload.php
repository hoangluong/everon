<?php

class upload
{
    function __construct() {
        $this->_CIA = & get_instance();
        $this->_CIA->load->library('layout');
        $this->_CIA->load->library('common');
        $this->allow_image_type = array('jpg','png');
        $this->allow_image_size = 1*1024*1024;//1mb
        $this->folder_image = 'public/uploads/news/';
        
        $this->allow_file_type = array('doc','docx','pdf');
        $this->allow_file_size = 2*1024*1024;//1mb
        $this->folder_file = 'public/uploads/attach/';
    }
    public function image()
    {
        $isok = true;
        $file = $_FILES['userfile']['name'];
        if((int)$_FILES['userfile']['size'] > 0){
             $file_ext = pathinfo($file);
             // check file type allow
             if(!in_array($file_ext['extension'],$this->allow_image_type)){
                $data['allow_file'] = 'File upload không đúng định dạng!';
                $isok = false;
             }
             // check file size
             $file_info = stat($_FILES['userfile']['tmp_name']);
             if($file_info['size'] > $this->allow_image_size){
                 $data['allow_size'] = 'File upload <= 1 mb';
                  $isok = false;
             }
              
                 $filename = $this->_CIA->common->locdau($file_ext['filename']).'_'.date('HisYmd').'.'.$file_ext['extension'];
                 move_uploaded_file($_FILES['userfile']['tmp_name'],$this->folder_image.$filename);
        }else{
            $filename = "";
            $file_info['size'] = 0;
        }
        $arr = array(
                    'imagename' => $filename,
                    'isok' => $isok  
                );
        return $arr;
        
    }
    public function file()
    {
        $isok = true;
        $file = $_FILES['fileupload']['name'];
        if((int)$_FILES['fileupload']['size'] > 0){
             $file_ext = pathinfo($file);
             // check file type allow
             if(!in_array($file_ext['extension'],$this->allow_file_type)){
                $data['allow_file'] = 'File upload không đúng định dạng!';
                $isok = false;
             }
             // check file size
             $file_info = stat($_FILES['fileupload']['tmp_name']);
             if($file_info['size'] > $this->allow_file_size){
                 $data['allow_size'] = 'File upload <= 1 mb';
                  $isok = false;
             }
              
                 $filename = $this->_CIA->common->locdau($file_ext['filename']).'_'.date('HisYmd').'.'.$file_ext['extension'];
                 move_uploaded_file($_FILES['fileupload']['tmp_name'],$this->folder_file.$filename);
        }else{
            $filename = "";
            $file_info['size'] = 0;
        }
        $arr = array(
                    'filename' => $filename,
                    'isok' => $isok  
                );
        return $arr;
        
    }
}


?>
