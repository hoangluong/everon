<?php


class account extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("account/account_model");
        $this->layout->setLayout('layouts/hospital_layout');
    }

    function index()
    {
        $data = array();
        $allAccount = $this->account_model->getAllAccount();
        $data['accounts'] = $allAccount;
        $this->layout->view('account/account_views', $data);
    }

    function create()
    {
        $data = array();
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $arr = array(
                'username' => $input['username'],
                'password' => $input['password'],
                'isadmin' => $input['isadmin'],
                'fullname' =>$input['fullname'],
                'birthday'=> date('Y-m-d',strtotime($input['birthday'])),
                'email' => $input['email'],
                'address' => $input['address'],
                'phone' => $input['phone']
            );
        }
        $this->layout->view('account/account_create_view', $data);
    }

}

?>