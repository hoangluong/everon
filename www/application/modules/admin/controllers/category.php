<?php

/**
 * @author 
 * @copyright 2014
 */

class category extends MX_Controller
{

    function __construct()
    {
        parent::__construct();  
        $this->load->model("category/category_model");
        $this->load->model("industry/industry_model");
        $this->load->model("general/general_model");
        $this->layout->setLayout('layouts/hospital_layout');
         $this->allow_image_type = array('jpg', 'png', 'jpeg', 'gif');
        $this->allow_image_size = 2 * 1024 * 1024;//2mb
        $this->folder_image = 'data/category/';
    }
    
    function create(){
        $data = array();
        $data['industries'] = $this->industry_model->getAllIndustries();

        if($this->input->post("name",true)){
         $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = "";
                $file_info['size'] = 0;
            }
         $arr = array(
                'name' => $input['name'], 
                'alias' => $input['alias'],
              //  'title' => $input['title'],
                 'image' => $filename_bg,
                  'industry_id' => $input['industry'],
                  'parent' => isset($input['parent'])?$input['parent']:0,
                  'order' => 0,
                  'date' => date('Y-m-d H:i:s')
         );
           
         $this->category_model->insert_category($arr);
         redirect(base_url("admin/category"));
        }
        $this->layout->view("category/category_add_views",$data);
    }
    
    function alias(){
        $name = $this->input->post("name",true);
        echo strtolower(str_replace(' ','-',$this->common->locdau($name)));die;
    }
    
    function index(){
        $data = array();
        $data['categories'] = $this->category_model->get_all_category();
        $this->layout->view("category/category_views",$data);
    }
    
    function edit(){
        $id = $this->uri->rsegment(3);
        $data = array();
        $data['industries'] = $this->industry_model->getAllIndustries();
        $data['categorys'] = $this->category_model->get_all_category_by_parent(0);
        
        $data['thisCategory'] = $this->category_model->get_category_by_id($id);
        if($this->input->post("submit")){
        $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = $input['hidImage'];
                $file_info['size'] = 0;
            }
            $arr = array(
                'name' => $input['name'],
                'alias' => $input['alias'],
                'image' => $filename_bg,
                'industry_id' => $input['industry'],
                'parent' => isset($input['parent'])?$input['parent']:0,
                'order' => 0,
                'date' => date('Y-m-d H:i:s')
            );

         $this->general_model->update('categories',$arr,$input['id']);
         redirect(base_url("admin/category"));
        }
        $this->layout->view("category/category_edit_view",$data);
    }
    
    function delete(){
        $id = $this->input->post("id");
        $this->category_model->delete_category($id);
    }
    
    function getCategory(){
        $id = $this->input->post('industryId',true);
        $selected = $this->input->post('selected',true);
        $sub = $this->category_model->getCategoryByIndustry($id);
        echo '<option value="0">- None -</option>';
        foreach($sub as $item){
            if($item['id'] != $selected){
                echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
            }else{
                echo '<option value="'.$item['id'].'" selected="selected">'.$item['name'].'</option>';
            }
        }
      
        die;
    }
    function getSub(){
        $id = $this->input->post('categoryId',true);
        $selected = $this->input->post('selected',true);
        $sub = $this->category_model->get_all_category_by_parent($id);
        echo '<option value="0">- None -</option>';
        foreach($sub as $item){
            if($item['id'] != $selected){
                echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
            }else{
                echo '<option value="'.$item['id'].'" selected="selected">'.$item['name'].'</option>';
            }
        }

        die;
    }

    
    function setStatus(){
        
        $videoid = $this->input->post('id');
        $val = $this->input->post('val');
        $arr= array(
            'active'=>($val=='true')?1:0
        );
      
        $this->category_model->update_category($videoid,$arr);
    }
}

?>