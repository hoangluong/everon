<?php


class industry extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("industry/industry_model");
        $this->load->model("general/general_model");
        $this->layout->setLayout('layouts/hospital_layout');
    }

    function index(){

        $data = array();

        $industries = $this->industry_model->getAllIndustries();
        $data['industries'] = $industries;
        $this->layout->view('industry/industry_views',$data);

    }

    function create(){
        $data = array();
        if($this->input->post('submit')){
            $input = $this->input->post();
            $arr = array(
                'name'=>$input['name'],
                'key' => $input['key']
            );
            $this->general_model->insert('industries',$arr);
            redirect(base_url('admin/industry'));
        }
        $this->layout->view("industry/create_views",$data);
    }

    function delete(){
        $id = $this->input->post("id",true);
        $this->general_model->delete('industries',$id);
        die;
    }


}
?>