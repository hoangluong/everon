<?php

class login extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("login/login_model");
        $this->layout->setLayout('layouts/login_layout');
        $this->load->library('recaptcha');
    }

    function index()
    {
        $data = array();
          if(isset($this->session->userdata['id'])){
         	redirect(base_url("admin/home"));
          }

        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $info = $this->login_model->login($input['username'], $input['password']);

            if ($info) {
                $this->session->set_userdata('id', $info['id']);
                $this->session->set_userdata('username', $info['username']);
                $this->session->set_userdata('doctor_id', $info['doctor_id']);
                redirect(base_url("admin/product"));
            }
        }
        $this->layout->view("login/login_view", $data);
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url("admin/login"));
    }

}

?>