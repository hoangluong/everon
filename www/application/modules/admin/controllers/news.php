<?php


class news extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("news/news_model");
        $this->load->model("general/general_model");
        $this->layout->setLayout('layouts/hospital_layout');
        $this->allow_image_type = array('jpg', 'png');
        $this->allow_image_size = 2 * 1024 * 1024;//2mb
        $this->folder_image = 'data/news/';
    }

    function index()
    {

        $data = array();

        $news = $this->news_model->getAllNews();
        $data['news'] = $news;
        $this->layout->view('news/news_views', $data);

    }

    function create()
    {
        $data = array();
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = "";
                $file_info['size'] = 0;
            }
            $arr = array(
                'title' => $input['title'],
                'alias' => $input['alias'],
                'desc' => $input['desc'],
                'content' => $input['content'],
                'date' => date("Y-m-d H:i:s"),
                'createdby' => $this->session->userdata('id'),
                'image' => $filename_bg
            );
            $this->general_model->insert('news', $arr);
            redirect(base_url('admin/news'));
        }
        $this->layout->view("news/news_create_views", $data);
    }

    function edit($id)
    {
        $data = array();
        $news = $this->news_model->getNewsById($id);
        $data['news'] = $news[0];
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = $input['hidImage'];
                $file_info['size'] = 0;
            }
            $arr = array(
                'title' => $input['title'],
                'alias' => $input['alias'],
                'desc' => $input['desc'],
                'content' => $input['content'],

                'createdby' => $this->session->userdata('id'),
                'image' => $filename_bg
            );
            $this->general_model->update('news', $arr,$input['id']);
            redirect(base_url('admin/news'));
        }
        $this->layout->view("news/news_edit_views", $data);
    }

    function delete()
    {
        $id = $this->input->post("id", true);
        $this->general_model->delete('news', $id);
        die;
    }


}

?>