<?php


class page extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("page/page_model");
        $this->load->model("general/general_model");
        $this->layout->setLayout('layouts/hospital_layout');
        $this->allow_image_type = array('jpg', 'png');
        $this->allow_image_size = 2 * 1024 * 1024;//2mb
        $this->folder_image = 'data/news/';
    }

    function index()
    {

        $data = array();
        $news = $this->page_model->getAllPages();
        $data['pages'] = $news;
        $this->layout->view('page/page_index_views', $data);

    }

    function create()
    {
        $data = array();
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $arr = array(
                'title' => $input['title'],
                'content' => $input['content'],
                'alias' => $input['alias'],
                'date' => date('Y-m-d H:i:s'),
                'edit' => 1
            );
            $this->general_model->insert('pages', $arr);
            redirect(base_url('admin/page'));
        }
        $this->layout->view('page/page_create_views', $data);
    }

    function edit($id)
    {
        $data = array();
        $data['page'] = $this->page_model->getPageById($id);
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $arr = array(
                'title' => $input['title'],
                'content' => $input['content'],
                'alias' => $input['alias'],
                'date' => date('Y-m-d H:i:s')
            );
            $this->general_model->update('pages', $arr, $input['id']);
            redirect(base_url('admin/page'));
        }
        $this->layout->view('page/page_edit_views', $data);
    }

    function delete(){
        $id = $this->input->post('id', true);
        $this->general_model->delete('pages', $id);
    }
}