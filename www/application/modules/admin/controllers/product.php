<?php


class product extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("supplier/supplier_model");
        $this->load->model("general/general_model");
        $this->load->model("product/product_model");
        $this->load->model("brand/brand_model");
        $this->load->model("category/category_model");
        $this->load->model("industry/industry_model");
        $this->load->model("property/property_model");
        $this->layout->setLayout('layouts/hospital_layout');
        $this->allow_image_type = array('jpg', 'png');
        $this->allow_image_size = 2 * 1024 * 1024;//2mb
        $this->folder_image = 'data/products/';

    }

    function index()
    {

        $data = array();

        $products = $this->product_model->getAllProducts();
        $data['products'] = $products;
        $this->layout->view('product/product_views', $data);

    }

    function create()
    {
        $data = array();
        $data['industries'] = $this->industry_model->getAllIndustries();
        $suppliers = $this->supplier_model->getAllSuppliers();
        $data['suppliers'] = $suppliers;
        $data['properties'] = $this->property_model->get_all_properties();
        $brands = $this->brand_model->getAllBrands();
        $data['brands'] = $brands;
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = "";
                $file_info['size'] = 0;
            }
            $arrProduct = array(
                'name' => $input['name'],
                'alias' => $input['alias'],
                'desc' => $input['desc'],
                'category_id' => $input['category'],
                'sub_id' => $input['sub'],

                'industry_id' => $input['industry'],

                'content' => $input['content'],
                'price' => $input['price'],
                'image' => $filename_bg


            );
            $productId = $this->general_model->insert('products', $arrProduct);
            $allProperty = $this->property_model->get_all_properties();
            foreach ($allProperty as $item) {
                if ($input['property_' . $item['id']] != '') {
                    $arrProductProperty = array(
                        'product_id' => $productId,
                        'property_id' => $item['id'],
                        'value' => $input['property_' . $item['id']]
                    );
                    $this->general_model->insert('product_properties', $arrProductProperty);
                }
            }

            redirect(base_url('admin/product'));
        }
        $this->layout->view("product/product_create_views", $data);
    }

    function picture($pid)
    {
        $data = array();
        $product = $this->product_model->get_product_by_id($pid);
        $images = $this->product_model->getAllImagesByProduct($pid);
        $iid = $this->input->get('iid', true);
        $thisImage = $this->product_model->getImageById($iid);
        $data['thisImage'] = $thisImage;
        $data['product'] = $product[0];
        $data['images'] = $images;
        $data['iid'] = $iid;
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = "";
                $file_info['size'] = 0;
            }
            $arr = array(
                'product_id' => $pid,
                'image' => ($filename_bg) ? $filename_bg : $input['hidImage'],
                'desc' => $input['desc']
            );
            if (!$iid) {

                $this->general_model->insert('product_images', $arr);
            } else {
                $this->general_model->update('product_images', $arr, $iid);
            }
            redirect(base_url('admin/product/picture/' . $pid));
        }
        $this->layout->view('product/picture_views', $data);
    }

    function deleteImage()
    {
        $id = $this->input->post('id', true);
        $this->general_model->delete('product_images', $id);
        die;
    }

    function productDetele()
    {
        $id = $this->input->post('id', true);
        $this->general_model->delete('products', $id);
        $this->general_model->delete('product_images', $id);
        $this->general_model->deleteByField('product_properties', 'product_id', $id);
        die;
    }

    function edit($id)
    {
        $data = array();
        $data['industries'] = $this->industry_model->getAllIndustries();
        $suppliers = $this->supplier_model->getAllSuppliers();
        $data['suppliers'] = $suppliers;
        $data['properties'] = $this->property_model->get_all_properties();
        $brands = $this->brand_model->getAllBrands();
        $data['brands'] = $brands;
        $product = $this->product_model->get_product_by_id($id);
        $data['product'] = $product[0];
        $productProperties = $this->product_model->getPropertiesByProduct($id);
        $pp = array();
        foreach ($productProperties as $item) {
            $pp[$item['property_id']] = $item['value'];
        }
        $data['pp'] = $pp;

        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = $input['hidImage'];
                $file_info['size'] = 0;
            }
            $arrProduct = array(
                'name' => $input['name'],
                'alias' => $input['alias'],
                'desc' => $input['desc'],
                'category_id' => $input['category'],
                'sub_id' => $input['sub'],
                'industry_id' => $input['industry'],
                'content' => $input['content'],
                'price' => $input['price'],
                'image' => $filename_bg
            );
            $this->general_model->update('products', $arrProduct, $input['id']);
            $allProperty = $this->property_model->get_all_properties();
            $this->general_model->deleteByField('product_properties', 'product_id', $id);
            foreach ($allProperty as $item) {
                if ($input['property_' . $item['id']] != '') {
                    $arrProductProperty = array(
                        'product_id' => $id,
                        'property_id' => $item['id'],
                        'value' => $input['property_' . $item['id']]
                    );
                    $this->general_model->insert('product_properties', $arrProductProperty);
                }
            }
            redirect(base_url('admin/product'));
        }
        $this->layout->view('product/product_edit_view', $data);
    }

}

?>