<?php


class property extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("property/property_model");
        $this->load->model("general/general_model");
        $this->layout->setLayout('layouts/hospital_layout');
    }

    function index()
    {
        $data = array();
        $properties = $this->property_model->get_all_properties();
        $data['properties'] = $properties;
        $this->layout->view('property/property_views', $data);
    }

    function create()
    {
        $data = array();
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $arr = array(
                'name' => $input['name'],
                'type' => 1
            );
            $this->general_model->insert('properties', $arr);
            redirect(base_url('admin/property'));
        }
        $this->layout->view("property/create_views", $data);
    }

    function edit($id)
    {
        $data = array();
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $arr = array(
                'name' => $input['name']
            );
            $this->general_model->update('properties', $arr, $id);
            redirect(base_url('admin/property'));
        }
        $data['property'] = $this->property_model->getPropertyById($id);
        $this->layout->view('property/property_edit_views', $data);
    }

    function delete()
    {
        $id = $this->input->post("id", true);
        $this->general_model->delete('properties', $id);
        die;
    }


}

?>