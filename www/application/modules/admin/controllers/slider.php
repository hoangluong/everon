<?php

class slider extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("slider/slider_model");
        $this->load->model("industry/industry_model");
        $this->load->model("general/general_model");

        $this->layout->setLayout('layouts/hospital_layout');
        $this->allow_image_type = array('jpg', 'png');
        $this->allow_image_size = 2 * 1024 * 1024;//2mb
        $this->folder_image = 'data/sliders/';
    }

    function index()
    {
        $data = array();
        $data['sliders'] = $this->slider_model->getAllSlider();
        $this->layout->view('slider/slider_index_views', $data);
    }

    function create()
    {
        $data = array();
        $data['industries'] = $this->industry_model->getAllIndustries();
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = "";
                $file_info['size'] = 0;
            }
            $arr = array(
                'name' => $input['name'],
                'link' => $input['link'],
                'alias' => $input['alias'],
                'industry_id' => $input['industry'],
                'category_id' => $input['category'],
                'sub_id' => $input['sub'],
                'image' => $filename_bg
            );
            $this->general_model->insert('sliders', $arr);
            redirect(base_url('admin/slider'));
        }
        $this->layout->view("slider/slider_create_views", $data);
    }

    function delete()
    {
        $id = $this->input->post('id', true);
        $this->general_model->delete('sliders', $id);
        die;
    }

    function edit($id)
    {
        $data = array();
        $data['industries'] = $this->industry_model->getAllIndustries();
        $data['slider'] = $this->slider_model->getSliderById($id);
        if ($this->input->post('submit')) {
            $input = $this->input->post();
            $file = $_FILES['image']['name'];
            if ((int)$_FILES['image']['size'] > 0) {
                $file_ext = pathinfo($file);
                // check file type allow
                if (!in_array($file_ext['extension'], $this->allow_image_type)) {
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['image']['tmp_name']);
                if ($file_info['size'] > $this->allow_image_size) {
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']) . '_' . date('HisYmd') . '.' . $file_ext['extension'];
                move_uploaded_file($_FILES['image']['tmp_name'], $this->folder_image . $filename_bg);
            } else {
                $filename_bg = "";
                $file_info['size'] = 0;
            }
            $arr = array(
                'name' => $input['name'],
                'link' => $input['link'],
                'alias' => $input['alias'],
                'industry_id' => $input['industry'],
                'category_id' => $input['category'],
                'sub_id' => $input['sub'],
                'image' => ($filename_bg) ? $filename_bg : $input['hidImage']
            );
            $this->general_model->update('sliders', $arr, $input['id']);
            redirect(base_url('admin/slider'));
        }
        $this->layout->view("slider/slider_edit_view", $data);
    }
}

?>