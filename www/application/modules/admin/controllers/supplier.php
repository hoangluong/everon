<?php


class supplier extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("supplier/supplier_model");
        $this->load->model("general/general_model");
        $this->layout->setLayout('layouts/hospital_layout');
        $this->allow_image_type = array('jpg','png');
        $this->allow_image_size = 2*1024*1024;//2mb
        $this->folder_image = 'data/suppliers/';
        $this->layout->setLayout('layouts/hospital_layout');
    }

    function index(){

        $data = array();

        $suppliers = $this->supplier_model->getAllSuppliers();
        $data['suppliers'] = $suppliers;
        $this->layout->view('supplier/supplier_views',$data);

    }

    function create(){
        $data = array();
        if($this->input->post('submit')){
            $input = $this->input->post();
            $file = $_FILES['avatar']['name'];
            if((int)$_FILES['avatar']['size'] > 0){
                $file_ext = pathinfo($file);
                // check file type allow
                if(!in_array($file_ext['extension'],$this->allow_image_type)){
                    $data['error']['allow_file'] = 'File upload không đúng định dạng!';
                    return false;
                }
                // check file size
                $file_info = stat($_FILES['avatar']['tmp_name']);
                if($file_info['size'] > $this->allow_image_size){
                    $data['error']['allow_size'] = 'File upload <= 2 mb';
                    return false;
                }

                $filename_bg = $this->common->locdau($file_ext['filename']).'_'.date('HisYmd').'.'.$file_ext['extension'];
                move_uploaded_file($_FILES['avatar']['tmp_name'],$this->folder_image.$filename_bg);
            }else{
                $filename_bg = "";
                $file_info['size'] = 0;
            }
            $arr = array(
                'name'=>$input['name'],
                'alias' => $input['alias'],
                'avatar' => $filename_bg,
                'deputy' => $input['deputy'],
                'address' => $input['address'],
                'phone' => $input['phone']

            );
            $this->general_model->insert('suppliers',$arr);
            redirect(base_url('admin/supplier'));
        }
        $this->layout->view("supplier/supplier_create_views",$data);
    }


}
?>