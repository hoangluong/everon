<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Quản lý thành viên</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard') ?>">Bảng điều khiển</a></li>
            <li class="active">Quản lý thành viên</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Thêm thành viên mới</h3>
        </div>
    </div>
    <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">
        <div class="col-sm-6">
            <div class="white-box">
                <h3 class="box-title">Thông tin tài khoản</h3>

                <div class="form-group">
                    <label class="col-md-12" for="example-text">Tên đăng nhập</span></label>
                    <div class="col-md-12">
                        <input type="text" id="username" name="username" class="form-control"
                               placeholder="Tên nhãn hàng">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Mật khẩu</span></label>
                    <div class="col-md-12">
                        <input type="password" id="password" name="password" class="form-control"
                               placeholder="Mật khẩu">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Quyền</span></label>
                    <div class="col-md-12">
                        <select name="permit" class="form-control" id="">
                            <option value="0">Người dùng</option>
                            <option value="1" selected>Quản trị viên</option>
                            <option value="2">Administrator</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-12">Avatar</label>
                    <div class="col-sm-12">
                        <input type="file" name="image" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="white-box">
                <h3 class="box-title">Thông tin cá nhân</h3>

                <div class="form-group">
                    <label class="col-md-12" for="example-text">Họ và Tên</span></label>
                    <div class="col-md-12">
                        <input type="text" id="fullname" name="fullname" class="form-control"
                               placeholder="Tên nhãn hàng">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Ngày sinh</span></label>
                    <div class="col-md-12">
                        <input type="text" id="birthday" name="birthday" class="form-control mydatepicker"
                               placeholder="Ngày sinh">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Địa chỉ</span></label>
                    <div class="col-md-12">
                        <input type="text" id="address" name="address" class="form-control"
                               placeholder="Địa chỉ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Email</span></label>
                    <div class="col-md-12">
                        <input type="text" id="email" name="email" class="form-control"
                               placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Điện thoại</span></label>
                    <div class="col-md-12">
                        <input type="text" id="phone" name="phone" class="form-control"
                               placeholder="Điện thoại">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">
                Thêm mới
            </button>
            <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>

        </div>
    </form>
</div>
