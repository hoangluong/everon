<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý người dùng</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Phòng khám</a></li>
            <li class="active">Danh sách người dùng</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">Danh sách người dùng</div>
        <div class="panel-wrapper collapse in">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Họ và Tên</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($accounts as $item) {
                    $i++;
                    ?>
                    <tr>
                        <td align="center"><?php echo $i ?></td>
                        <td><?php echo $item['username'] ?></td>
                        <td><?php echo $item['email'] ?></td>
                    </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>