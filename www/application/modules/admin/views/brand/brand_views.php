<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý ngành hàng</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Bảng điều khiển</a></li>
            <li class="active">Ngành hàng</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">Danh sách nhãn hàng <span class="pull-right"><a href="<?php echo base_url('admin/brand/create')?>" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Thêm mới</a></span></div>
        <div class="panel-wrapper collapse in">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Logo</th>
                    <th>Tên nhãn</th>
                    <th>Đường dẫn</th>
                    <th>Quản lý</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($brands as $item) {
                    $i++;
                    ?>
                    <tr>
                        <td align="center"><?php echo $i ?></td>
                        <td><img src="<?php echo base_url('data/brands/'.$item['logo'])?>" alt=""/></td>
                        <td><?php echo $item['name'] ?></td>
                        <td><?php echo $item['alias'] ?></td>
                        <td><a href="">Sửa</a> | <a href="">Xóa</a></td>
                    </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>