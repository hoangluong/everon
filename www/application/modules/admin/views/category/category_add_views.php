<div class="row bg-title">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý danh mục hàng hóa</h4></div>
    <div class="col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>">Bảng điều khiển</a></li>
            <li class="active">Danh mục hàng hóa</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Thêm mới danh mục</h3>
            <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Tên danh mục</span></label>
                    <div class="col-md-12">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Tên danh mục">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Hình ảnh</span></label>

                    <div class="col-md-12">
                        <input type="file" name="image" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="example-text">Ngành hàng</span></label>
                    <div class="col-md-12">
                        <select name="industry" id="industry" class="form-control" onchange="getCategory(this.value)">
                            <option value="0">-- None --</option>
                            <?php
                            foreach ($industries as $item) {
                                ?>
                                <option value="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Danh mục cha</span></label>
                    <div class="col-md-12">
                        <select name="parent" id="parent" class="form-control">
                            <option value="0">- None -</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="special">Đường dẫn</span></label>
                    <div class="col-md-12">
                        <input type="text" id="alias" name="alias" class="form-control" placeholder="Đường dẫn">
                    </div>
                </div>


                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">Thêm mới</button>
                <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>
            </form>
        </div>
    </div>
</div>
<script>
    $("#name").blur(function () {
        $.ajax({
            url: '<?php echo base_url("admin/category/alias")?>',
            data: {name: $(this).val()},
            type: 'POST',
            success: function (msg) {
                $("#alias").val(msg);
            }
        })
    });
    function getCategory(industryId) {
        $.ajax({
            url: '<?php echo base_url('admin/category/getCategory')?>',
            data: {industryId: industryId},
            type: 'POST',
            success: function (response) {
                $('#parent').html(response);
            }
        });
    }
</script>