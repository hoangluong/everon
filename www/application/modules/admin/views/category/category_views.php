<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý danh mục hàng hóa</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url()?>">Bảng điều khiển</a></li>
            <li class="active">Danh mục hàng hóa</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-xs-12">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">danh mục hàng hóa <span class="pull-right"><a
                        href="<?php echo base_url('admin/category/create') ?>"
                        class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Thêm
                        mới</a></span></div>
            <div class="panel-wrapper collapse in">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Tên</th>
                        <th>Danh mục cha</th>
                        <th>Ngành hàng</th>
                        <th>đường dẫn</th>
                        <th>Quản lý</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($categories as $item) {
                        $parent = $this->category_model->get_category_by_id($item['parent']);
                        $i++;
                        ?>
                        <tr>
                            <td align="center"><?php echo $i ?></td>
                            <td><?php echo $item['name'] ?></td>
                            <td><?php echo ($parent)?$parent[0]['name']:'';?></td>
                            <td><?php echo $item['industry_name'] ?></td>
                            <td><?php echo $item['alias'] ?></td>
                            <td><a href="<?php echo base_url('admin/category/edit/'.$item['id'])?>">Sửa</a> | <a href="">Xóa</a></td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>