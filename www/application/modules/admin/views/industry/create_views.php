<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ngành hàng</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard')?>">Bảng điều khiển</a></li>
            <li class="active">Quản lý ngành hàng</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Thêm mới ngành hàng</h3>
            <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Tên ngành hàng</span></label>
                    <div class="col-md-12">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Tên ngành hàng">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-sm-12">icon 1</label>
                    <div class="col-sm-12">
                        <input type="file" name="icon1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-12">icon 2</label>
                    <div class="col-sm-12">
                        <input type="file" name="icon2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="special">Đường dẫn</span></label>
                    <div class="col-md-12">
                        <input type="text" id="key" name="key" class="form-control" placeholder="Đường dẫn">
                    </div>
                </div>


                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">Thêm mới</button>
                <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>
            </form>
        </div>
    </div>
</div>
