<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý tin tức</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Bảng điều khiển</a></li>
            <li class="active">Tin tức</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-xs-12">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Danh sách tin tức <span class="pull-right"><a
                        href="<?php echo base_url('admin/product/create') ?>"
                        class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Thêm
                        mới</a></span></div>
            <div class="panel-wrapper collapse in">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Hình ảnh</th>
                        <th>Tiêu đề</th>
                        <th>Ngày đăng</th>

                        <th>Quản lý</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($news as $item) {
                        $i++;
                        ?>
                        <tr>
                            <td align="center"><?php echo $i ?></td>
                            <td><img style="width: 50px" src="<?php echo base_url('data/news/' . $item['image']) ?>"
                                     alt=""/></td>
                            <td>
                                <b><?php echo $item['title'] ?></b><br>
                                <div><?php echo $item['desc']?></div>

                            </td>
                            <td>
                                <strong><?php echo $item['date'] ?></strong>
                            </td>
                            <td><a href="<?php echo base_url('admin/news/edit/'.$item['id'])?>">Sửa</a> | <a href="javascript:act.remove(<?php echo $item['id'] ?>)">Xóa</a>
                            </td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function forms() {
        this.remove = function (id) {
            if (confirm("Bạn có chắc chắn muốn xóa không?")) {
                $.ajax({
                    url: '<?php echo base_url("admin/news/delete")?>',
                    data: {id: id},
                    type: 'POST',
                    success: function (data) {
                        location.reload();
                    }
                })
            }
        }
    }
    var act = new forms();
</script>