<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Trang thông tin</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard')?>">Bảng điều khiển</a></li>
            <li class="active">Quản lý  trang thông tin</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Thêm mới trang thông tin</h3>
            <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Tiêu đề</span></label>
                    <div class="col-md-12">
                        <input type="text" id="title" name="title" class="form-control" placeholder="Tiêu đề">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-12">Nội dung</label>
                    <div class="col-sm-12">
                        <textarea class="textarea_editor form-control" name="content" rows="15" placeholder="Nội dung ..."></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="special">Đường dẫn</span></label>
                    <div class="col-md-12">
                        <input type="text" id="alias" name="alias" class="form-control" placeholder="Đường dẫn">
                    </div>
                </div>


                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">Thêm mới</button>
                <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>
            </form>
        </div>
    </div>
</div>
<script>
    $("#title").blur(function () {
        $.ajax({
            url: '<?php echo base_url("admin/category/alias")?>',
            data: {name: $(this).val()},
            type: 'POST',
            success: function (msg) {
                $("#alias").val(msg);
            }
        })
    });
</script>