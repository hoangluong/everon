<div class="row bg-title">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý hình ảnh sản phẩm</h4></div>
    <div class="col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>">Bảng điều khiển</a></li>
            <li class="active"><?php echo $product['name'] ?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form action="">
            <div class="white-box clearfix">
                <h3 class="box-title">Thông tin sản phẩm</h3>

                <div class="form-control">
                    <label class="col-md-3" for="example-text">Tên sản phẩm</span></label>

                    <div class="col-md-9">
                        <?php echo $product['name'] ?>
                    </div>
                </div>

            </div>
        </form>

    </div>
    <div class="col-sm-6">
        <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">

            <div class="white-box">
                <h3 class="box-title">Thêm hình ảnh</h3>

                <div class="form-group">
                    <label class="col-md-12" for="example-text">Hình ảnh sản phẩm</span></label>

                    <div class="col-md-12">
                        <input type="file" name="image" class="form-control"/>
                    </div>
                </div>
                <?php
                if (isset($iid) && $iid) {
                    ?>
                    <div class="form-group">

                        <div class="col-md-12">
                            <img src="<?php echo base_url('data/products/' . $thisImage['image']) ?>"
                                 style="width: 100%" alt=""/>
                            <input type="hidden" value="<?php echo $thisImage['image']?>" name="hidImage"/>
                            <input type="hidden" value="<?php echo $iid?>" name="iid"/>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Mô tả</span></label>

                    <div class="col-md-12">
                        <textarea class="form-control textarea_editor" name="desc" rows="8"
                                  id=""><?php echo (isset($thisImage['desc'])) ? $thisImage['desc'] : '' ?></textarea>
                    </div>
                </div>
                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">
                    Submit
                </button>
                <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>
            </div>
        </form>
    </div>
    <div class="col-sm-6">
        <div class="white-box">
            <h3 class="box-title">Hình ảnh sản phẩm</h3>

            <div class="images-list">
                <?php
                if (count($images) > 0) {
                    foreach ($images as $item) {
                        ?>
                        <div><img src="<?php echo base_url('data/products/' . $item['image']) ?>" alt=""/></div>
                        <div><?php echo $item['desc'] ?></div>
                        <div class="control"><a
                                href="<?php echo base_url('admin/product/picture/' . $item['product_id']) ?>?iid=<?php echo $item['id'] ?>">Sửa</a>
                            | <a href="javascript:act.remove(<?php echo $item['id'] ?>)">Xóa</a></div>
                    <?php
                    }
                } else {
                    echo '<div>Chưa có hình ảnh nào!</div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<style>
    .images-list img {
        width: 100%;
        margin-top: 10px;
    }

    .images-list .control {
        background: #00b3ee;
        padding: 5px;
    }

    .images-list .control a {

        color: #FFFFFF;
    }
</style>
<script>
    function forms() {
        this.remove = function (id) {
            if (confirm("Bạn có chắc chắn muốn xóa không?")) {
                $.ajax({
                    url: '<?php echo base_url("admin/product/deleteImage")?>',
                    data: {id: id},
                    type: 'POST',
                    success: function (data) {
                        location.reload();
                    }
                })
            }
        }
    var act = new forms();
</script>