<?php
$_CI = &get_instance();
$_CI->load->model('property/property_model');
?>
<div class="row bg-title">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý hàng hóa</h4></div>
    <div class="col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>">Bảng điều khiển</a></li>
            <li class="active">Quản lý sản phẩm</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">
        <div class="col-sm-6">
            <div class="white-box">
                <h3 class="box-title">Thông tin sản phẩm</h3>


                <div class="form-group">
                    <label class="col-md-12" for="example-text">Tên sản phẩm</span></label>

                    <div class="col-md-12">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Tên sản phẩm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Mô tả sản phẩm</span></label>

                    <div class="col-md-12">
                        <textarea name="desc" id="" rows="4" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Hình ảnh sản phẩm</span></label>

                    <div class="col-md-12">
                        <input type="file" name="image" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Giá sản phẩm</span></label>

                    <div class="col-md-12">
                        <input type="text" id="price" name="price" class="form-control" placeholder="Giá sản phẩm">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="special">Đường dẫn</span></label>

                    <div class="col-md-12">
                        <input type="text" id="alias" name="alias" class="form-control" placeholder="Đường dẫn">
                    </div>
                </div>


            </div>


        </div>
        <div class="col-sm-6">
            <div class="white-box">
                <h3 class="box-title">Thông tin khác</h3>

                <div class="form-group">
                    <label class="col-md-12" for="example-text">Ngành hàng</span></label>

                    <div class="col-md-12">
                        <select name="industry" id="industry" class="form-control" onchange="getCategory(this.value)">
                            <option value="0">-- None --</option>
                            <?php
                            foreach ($industries as $item) {
                                ?>
                                <option value="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Danh mục cha</span></label>

                    <div class="col-md-12">
                        <select name="category" id="category" class="form-control" onchange="getSub(this.value)">
                            <option value="0">- None -</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Danh mục</span></label>

                    <div class="col-md-12">
                        <select name="sub" id="sub" class="form-control">
                            <option value="0">- None -</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="white-box">
                <h3 class="box-title">Thuộc tính</h3>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th></th>

                            <th>Thuộc tính</th>
                            <th>Tiền thay đổi</th>
                        </tr>
                        <?php
                        $i = 0;
                        foreach ($properties as $item) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i ?></td>

                                <td>

                                    <!--  <input name="property_<?php echo $item['id'] ?>" type="text" class="form-control" placeholder="<?php echo $item['name'] ?>"/>-->
                                    <?php
                                    $_CI->property_model->getPropertyHtml($item['id']);
                                    ?>

                                </td>
                                <td><input type="text" class="form-control" value="0" name="money_<?php echo $item['id']?>"/></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Nội dung</h3>

                <div class="form-group">
                    <textarea class="textarea_editor form-control" name="content" rows="15"
                              placeholder="Giới thiệu sản phẩm ..."></textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="white-box">
                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">
                    Thêm mới
                </button>
                <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>
            </div>
        </div>
    </form>
</div>
<script>
    $("#name").blur(function () {
        $.ajax({
            url: '<?php echo base_url("admin/category/alias")?>',
            data: {name: $(this).val()},
            type: 'POST',
            success: function (msg) {
                $("#alias").val(msg);
            }
        })
    });
    function getCategory(industryId) {
        $.ajax({
            url: '<?php echo base_url('admin/category/getCategory')?>',
            data: {industryId: industryId},
            type: 'POST',
            success: function (response) {
                $('#category').html(response);
            }
        });
    }
    function getSub(categoryId) {
        $.ajax({
            url: '<?php echo base_url('admin/category/getSub')?>',
            data: {categoryId: categoryId},
            type: 'POST',
            success: function (response) {
                $('#sub').html(response);
            }
        });
    }
</script>