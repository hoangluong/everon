<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý thuộc tính sản phẩm</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Bảng điều khiển</a></li>
            <li class="active">Thuộc tính sản phẩm</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-xs-12">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Danh sách thuộc tính sản phẩm <span class="pull-right"><a
                        href="<?php echo base_url('admin/property/create') ?>"
                        class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Thêm
                        mới</a></span></div>
            <div class="panel-wrapper collapse in">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Thuộc tính</th>
                        <th>Loại</th>
                        <th>Quản lý</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($properties as $item) {
                        $i++;
                        ?>
                        <tr>
                            <td align="center"><?php echo $i ?></td>
                            <td><?php echo $item['name'] ?></td>
                            <td><strong><?php echo $item['type'] ?></strong></td>
                            <td><a href="<?php echo base_url('admin/property/edit/'.$item['id'])?>">Sửa</a> | <a href="javascript:act.remove(<?php echo $item['id'] ?>)">Xóa</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function forms() {
        this.remove = function (id) {
            if (confirm("Bạn có chắc chắn muốn xóa không?")) {
                $.ajax({
                    url: '<?php echo base_url("admin/property/delete")?>',
                    data: {id: id},
                    type: 'POST',
                    success: function (data) {
                        location.reload();
                    }
                })
            }
        }
    }
    var act = new forms();
</script>