<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Slider</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/dashboard') ?>">Bảng điều khiển</a></li>
            <li class="active">Quản lý Slider</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Thay đổi slide</h3>

            <form class="form-material form-horizontal" method="post" action="" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $slider[0]['id']?>"/>
                <input type="hidden" name="hidImage" value="<?php echo $slider[0]['image']?>"/>
                <div class="form-group">
                    <label class="col-md-6" for="example-text">Tên slide</span></label>

                    <div class="col-md-12">
                        <input type="text" id="name" name="name" class="form-control"
                               value="<?php echo $slider[0]['name'] ?>" placeholder="Tên ngành hàng">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Hình ảnh</span></label>

                    <div class="col-md-6">
                        <input type="file" name="image" class="form-control"/>
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo base_url('data/sliders/'.$slider[0]['image'])?>" style="height: 100px" alt=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Link</span></label>

                    <div class="col-md-12">
                        <input type="text" name="link" class="form-control" value="<?php echo $slider[0]['link'] ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Ngành hàng</span></label>

                    <div class="col-md-12">
                        <select name="industry" id="industry" class="form-control" onchange="getCategory(this.value)">
                            <option value="0">-- None --</option>
                            <?php
                            foreach ($industries as $item) {
                                ?>
                                <option
                                    value="<?php echo $item['id'] ?>"<?php echo ($item['id'] == $slider[0]['industry_id']) ? 'selected' : ''; ?>><?php echo $item['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Danh mục cha</span></label>

                    <div class="col-md-12">
                        <select name="category" id="category" class="form-control" onchange="getSub(this.value)">
                            <option value="0">- None -</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="example-text">Danh mục</span></label>

                    <div class="col-md-12">
                        <select name="sub" id="sub" class="form-control">
                            <option value="0">- None -</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="special">Đường dẫn</span></label>

                    <div class="col-md-12">
                        <input type="text" id="alias" name="alias" value="<?php echo $slider[0]['alias']?>" class="form-control" placeholder="Đường dẫn">
                    </div>
                </div>


                <button type="submit" name="submit" value="submit" class="btn btn-info waves-effect waves-light m-r-10">
                    Thêm mới
                </button>
                <button type="reset" class="btn btn-inverse waves-effect waves-light">Hủy</button>
            </form>
        </div>
    </div>
</div>
<script>
    getCategory(<?php echo $slider[0]['industry_id']?>, <?php echo $slider[0]['category_id']?>);
    getSub(<?php echo $slider[0]['category_id']?>, <?php echo $slider[0]['sub_id']?>);
    $("#name").blur(function () {
        $.ajax({
            url: '<?php echo base_url("admin/category/alias")?>',
            data: {name: $(this).val()},
            type: 'POST',
            success: function (msg) {
                $("#alias").val(msg);
            }
        })
    });
    function getCategory(industryId, selected) {
        $.ajax({
            url: '<?php echo base_url('admin/category/getCategory')?>',
            data: {industryId: industryId, selected: selected},
            type: 'POST',
            success: function (response) {
                $('#category').html(response);
            }
        });
    }
    function getSub(categoryId, selected) {
        $.ajax({
            url: '<?php echo base_url('admin/category/getSub')?>',
            data: {categoryId: categoryId, selected: selected},
            type: 'POST',
            success: function (response) {
                $('#sub').html(response);
            }
        });
    }
</script>