<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"> Quản lý slider</h4></div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Bảng điều khiển</a></li>
            <li class="active">Slider</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-xs-12">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Danh sách slide <span class="pull-right"><a
                        href="<?php echo base_url('admin/slider/create') ?>"
                        class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Thêm
                        mới</a></span></div>
            <div class="panel-wrapper collapse in">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Hình ảnh</th>
                        <th>Tên quảng cáo</th>
                        <th>Ngành hàng</th>
                        <th>Link</th>
                        <th>Quản lý</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($sliders as $item) {
                        $i++;
                        ?>
                        <tr>
                            <td align="center"><?php echo $i ?></td>
                            <td><img style="width: 50px" src="<?php echo base_url('data/sliders/' . $item['image']) ?>" alt=""/></td>
                            <td><?php echo $item['name'] ?></td>
                            <td>
                                <strong><?php echo $item['industry_name'] ?></strong>

                            </td>
                            <td><?php echo $item['link']?></td>
                            <td><a href="<?php echo base_url('admin/slider/edit/'.$item['id'])?>">Sửa</a> | <a href="javascript:Ctm.remove(<?php echo $item['id']?>)">Xóa</a></td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function forms(){
        this.remove=function(id){
            if(confirm("Bạn có chắc chắn muốn xóa không?")){
                $.ajax({
                    url: '<?php echo base_url("admin/slider/delete")?>',
                    data: {id:id},
                    type: 'POST',
                    success:function(data){
                        location.reload();
                    }
                })
            }
        }
    }
    var Ctm = new forms();

</script>