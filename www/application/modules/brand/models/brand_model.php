<?php



/**
 * @author luonghx
 * @copyright 2013
 */



class brand_model extends CI_Model {



    function brand_model() {
        parent::__construct();
    }

    function getAllbrands(){
        $this->db->select('*')
            ->from('brands')
            ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function insertBrand($data){
        $this->db->insert('industries',$data);
        return $this->db->insert_id();
    }

    function getBrandByIndustry($id){
        $this->db->select('*')
            ->from('brands')
            ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }




}

?>