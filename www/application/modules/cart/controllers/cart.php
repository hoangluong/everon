<?php

class cart extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("product/product_model");
        $this->load->model("industry/industry_model");
        $this->load->model("category/category_model");
        $this->load->model("slider/slider_model");
        $this->load->library('cart');

        $this->layout->setLayout('layouts/home_layout');
    }

    function add()
    {
        $id = $this->input->get('id', true);
        $product = $this->product_model->get_product_by_id($id);
        $qty = $this->input->get('qty');
        $price = $product[0]['price'];
        $name = 'sp ' . $id;
        $size = $this->input->get('size');
        $have = false;
        $data = array(
            'id' => $id,
            'name' => $name,
            'qty' => $qty,
            'price' => $price,
            'options' => array('size' => $size)
        );
        foreach ($this->cart->contents() as $item) {
            if ($item['id'] == $id) {
                $have = true;
            }
        }
        if ($have == false) {
            if ($this->cart->insert($data)) {
                echo "Them san pham thanh cong";
            } else {
                echo "Them san pham that bai";
            }
        }
        die;
    }

    function index()
    {
        $data = array();
        $productCart = $this->cart->contents();
        $products = array();
        foreach ($productCart as $item) {
            $product = $this->product_model->get_product_by_id($item['id']);
            $products[] = array(
                'rowid' => $item['rowid'],
                'id' => $item['id'],
                'name' => $product[0]['name'],
                'qty' => $item['qty'],
                'price' => $item['price'],
                'image' => $product[0]['image']
            );
        }
        $data['products'] = $products;
        $this->layout->view("cart/cart_views", $data);
    }

    function update()
    {

    }

    function remove()
    {
        $rowid = $this->input->get('row', true);
        $arr = array(
            'rowid' => $rowid,
            'qty' => 0
        );
        if ($this->cart->update($arr)) {
            die('Xóa thành công!');
        }
    }

    function destroy()
    {
        $this->cart->destroy();
    }

}

?>