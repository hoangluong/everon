<div class="graybg cart-info">
    <div class="container">
        <div class="breadcrumb">
            <ul>
                <li><a href="">Trang chủ</a> <span>/</span></li>
                <li><a href="">Giỏ hàng</a> <span>/</span></li>
            </ul>
            <div class="back">
                <a href="">Tiếp tục mua hàng</a>
            </div>
            <hr/>


            <div class="col-sm-8">
                <div class="row">
                    <h3 class="title">Giỏ hàng của tôi</h3>

                    <div class="list-cart-product ">
                        <div class="list-body">
                            <div class="cart-list-title">
                                <div class="row">

                                    <div class="col-sm-7 count-product">
                                        <?php echo $this->cart->total_items(); ?> sản phẩm
                                    </div>
                                    <div class="col-sm-2 price-title">GIÁ BÁN</div>
                                    <div class="col-sm-2 qty-title">SỐ LƯỢNG</div>
                                    <div class="col-sm-1"></div>
                                </div>
                            </div>
                            <?php
                            foreach ($products as $item) {
                                ?>
                                <div class="product-item clearfix">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="thumb">
                                                <img src="<?php echo base_url('data/products/' . $item['image']) ?>"
                                                     alt=""/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="title"><?php echo $item['name'] ?></div>

                                        </div>
                                        <div class="col-sm-2">

                                            <div class="price"> <?php echo number_format($item['price']) ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 size">
                                            <select name="" class="slQty col-sm-4" id="qty<?php echo $item['id'] ?>"
                                                >
                                                <option value="">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-1 remove"><i row="<?php echo $item['rowid'] ?>"
                                                                        class="glyphicon glyphicon-remove"></i></div>
                                    </div>

                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <h5 class="title">THÔNG TIN ĐƠN HÀNG</h5>
                    <hr/>

                    <div class="col-sm-6"><div class="row">Tổng tiền thanh toán:</div></div>
                    <div class="col-sm-6"><div class="row text-right"><?php echo number_format($this->cart->total());?> VNĐ</div></div>
                </div>
            </div>
        </div>
    </div>
    <hr/>

</div>
<script>
    $('.remove i').click(function () {
        cart.remove($(this).attr('row'));
    });
    function cartShop() {
        this.remove = function (rowId) {
            $.ajax({
                url: base_url + 'cart/remove',
                type: 'get',
                data: {row: rowId},
                success: function (e) {
                    alert(e);
                }
            })
        }
    }

    var cart = new cartShop();
</script>