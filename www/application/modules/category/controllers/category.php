<?php

/**
 * @author
 * @copyright 2014
 */
class category extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("product/product_model");
        $this->load->model("category/category_model");
        $this->load->model("industry/industry_model");
        $this->load->model("slider/slider_model");
        $this->load->model("property/property_model");
        $this->layout->setLayout('layouts/home_layout');
    }

    function index()
    {
        $data = array();
        $data['colors'] = $color = $this->property_model->getPropertyData(2);
        $data['brands'] = $brand = $this->property_model->getPropertyData(4);
        $data['materials'] = $material = $this->property_model->getPropertyData(5);
        $order_price = ($this->input->get('price')) ? $this->input->get('price') : null;
        $brand = ($this->input->get('brand')) ? $this->input->get('brand') : null;
        $color = ($this->input->get('color')) ? $this->input->get('color') : null;
        $material = ($this->input->get('material')) ? $this->input->get('material') : null;
        $data['color'] = $color;
        $data['brand'] = $brand;
        $data['material'] = $material;
        $REQUEST_URI = $_SERVER['REQUEST_URI'];
        $REQUEST_URI_ORDER = explode('=', $REQUEST_URI);

        $order_price = ($order_price != 'none') ? $order_price : (isset($REQUEST_URI_ORDER[1])) ? $REQUEST_URI_ORDER[1] : 'none';

        $data['order_price'] = $order_price;
        $data['site_menu_product'] = true;
        $key = $this->uri->rsegment(3);
        $sub = $this->uri->rsegment(4);

        $page = ($this->uri->rsegment(5)) ? $this->uri->rsegment(5) : 1;
        $numrow = 20;
        $from = ($page - 1) * $numrow;
        $data['page'] = $page;
        if ($sub && !is_numeric($sub)) {

            $data['category'] = $category = $this->category_model->get_category_by_key($key);
            $data['cate_sub'] = $cate_sub = $this->category_model->get_category_by_key($sub);
            $category_id = isset($cate_sub[0]['id']) ? $cate_sub[0]['id'] : $category[0]['id'];

            $data['products'] = $this->product_model->get_product_by_sub($cate_sub[0]['id'], $from, $numrow, $order_price);

        } else {

            $from = ($sub - 1) * $numrow;
            $data['page'] = $sub;
            $data['category'] = $category = $this->category_model->get_category_by_key($key);
            $category_id = $category[0]['id'];
            $data['products'] = $this->product_model->get_product_by_category($category[0]['id'], $from, $numrow, $order_price, $brand, $material, $color);

        }

        $industry = $this->industry_model->getIndustryById(isset($category[0]['parent']) ? $category[0]['parent'] : $category_id);

        $data['industry'] = $industry;
        $categories = $this->category_model->getCategoryByIndustry(@$industry[0]['id']);
        $data['categories'] = $categories;
        $total = $this->product_model->get_total_product(null, $category_id);
        $data['num_page'] = ceil($total / $numrow);
        $data['slider'] = $this->slider_model->getAllSlider(null, $category_id, $sub);
        $this->layout->view("category/category_index_view", $data);
    }

    function collection()
    {
        $data = array();
        $data['color'] = $this->property_model->getPropertyData(2);
        $data['brand'] = $this->property_model->getPropertyData(4);
        $data['material'] = $this->property_model->getPropertyData(5);
        $order_price = ($this->input->get('price')) ? $this->input->get('price') : 'none';
        $REQUEST_URI = $_SERVER['REQUEST_URI'];
        $REQUEST_URI_ORDER = explode('=', $REQUEST_URI);

        $order_price = ($order_price != 'none') ? $order_price : (isset($REQUEST_URI_ORDER[1])) ? $REQUEST_URI_ORDER[1] : 'none';

        $data['order_price'] = $order_price;
        $data['site_menu_product'] = true;
        $key = $this->uri->rsegment(3);
        $sub = $this->uri->rsegment(4);

        $page = ($this->uri->rsegment(5)) ? $this->uri->rsegment(5) : 1;
        $numrow = 20;
        $from = ($page - 1) * $numrow;
        $data['page'] = $page;

        $data['cate_sub'] = $cate_sub = $this->category_model->get_category_by_key($key);
        $category_id = $cate_sub[0]['id'];

        $data['products'] = $this->product_model->get_product_by_sub($cate_sub[0]['id'], $from, $numrow, $order_price);


        $industry = $this->industry_model->getIndustryById(isset($category[0]['parent']) ? $category[0]['parent'] : $category_id);

        $data['industry'] = $industry;
        $categories = $this->category_model->getCategoryByIndustry(@$industry[0]['id']);
        $data['categories'] = $categories;
        $total = $this->product_model->get_total_product(null, $category_id);
        $data['num_page'] = ceil($total / $numrow);
        $data['slider'] = $this->slider_model->getAllSlider(null, $category_id, $sub);
        $this->layout->view("category/category_collection_views", $data);
    }


}


?>