<?php

/**
 * @author 
 * @copyright 2014
 */


class category_model extends CI_Model {
	
    function category_model() {
        parent::__construct();
    }
    
    function insert_category($data){
        $this->db->insert('categories',$data);
    }
    
    function get_all_category(){
        $this->db->select('C.*, G.name as industry_name')
                 ->from('categories C') 
                 ->join('industries G','G.id = C.industry_id','left')
                 ->order_by('C.order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }
    
     function getCategoryByIndustry($industryId,$onlyCategory = false){
        $this->db->select('*')
                 ->from('categories')
                 ->where('industry_id',$industryId);
        if($onlyCategory){
            $this->db->where('parent',0);
        }
        $this->db->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;        
     }
    function get_all_category_by_parent($parent = 0){
        $this->db->select('*')
                 ->from('categories')
                 ->where('parent',$parent)
                 ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    } 
 
     function get_category_by_id($id){
        $this->db->select('*')
                 ->from('categories')
                 ->where('id',$id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
     }
     
     function update_category($id,$data){
        $this->db->where('id',$id)->update('categories',$data);
     }
     
     function get_category_by_key($key){
        $this->db->select('*')
                 ->from('categories')
                 ->where('alias',$key);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
     }
     
     function delete_category($id){
        $this->db->delete('categories',array('id'=>$id));
     }
}

?>