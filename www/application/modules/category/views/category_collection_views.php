<div class="graybg">
    <div class="container">
        <div class="breadcrumb">
            <ul>
                <li><a href="">Trang chủ</a> <span>/</span></li>
                <li><a href="">Collection</a> <span>/</span></li>
                <li><a href=""><?php echo $cate_sub[0]['name']?></a> <span>/</span></li>
            </ul>
        </div>
    </div>
    <div class="banner-category">
        <?php
        foreach ($slider as $item) {
            ?>
            <img src="<?php echo base_url('data/sliders/' . $item['image']) ?>" alt="">
        <?php } ?>
    </div>
    <div class="list-category">
        <?php
        $i=0;
        foreach ($products as $item){
            $i++;
            ?>
            <div class="col-sm-4 category-item">
                <div class="row">
                    <div class="thumb">
                        <img
                                src="<?php echo base_url('data/products/' . $item['image']) ?>"
                                alt="">

                    </div>
                    <div class="info">
                        <div class="info-in">
                            <div class="title"><?php echo $item['name']?></div>
                            <a href="<?php echo base_url('P/' . $item['id'] . '-' . $item['alias']) ?>">Xem chi tiết</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        if($i==0){
        ?>
        <div class="col-sm-12">
            <div class="not-result text-center">
                <br>
                <br>
                <br>
                Không tìm thấy sản phẩm nào!
                <br>
                <br>
                <br>
            </div>
        </div>
        <?php }?>

    </div>
</div>
