﻿<?php
$_CI = &get_instance();
$_CI->load->model('property_model');
?>
<div class="graybg">
    <div class="container">
        <div class="breadcrumb">
            <ul>
                <li><a href="">Trang chủ</a> <span>/</span></li>
                <li><a href="">Phòng ngủ</a> <span>/</span></li>
            </ul>
        </div>
        <div class="banner-category">
            <?php
            foreach ($slider as $item) {
                ?>
                <img src="<?php echo base_url('data/sliders/' . $item['image']) ?>" alt="">
            <?php } ?>
        </div>
        <div class="category-body">
            <div class="filter clearfix">
                <form action="" method="get">
                    <div class="col-sm-1">Bộ lọc</div>
                    <div class="col-sm-2">
                        <select name="brand" id="">
                            <option value="">Thương hiệu</option>
                            <?php
                            foreach ($brands as $item) {
                                ?>
                                <option
                                    value="<?php echo $item['value'] ?>"<?php echo ($item['value'] == $brand) ? 'selected' : ''; ?>><?php echo $item['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="color" id="">
                            <option value="">Màu sắc</option>
                            <?php
                            foreach ($colors as $item) {
                                ?>
                                <option
                                    value="<?php echo $item['value'] ?>"<?php echo ($item['value'] == $color) ? 'selected' : ''; ?>><?php echo $item['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="material" id="">
                            <option value="">Chất liệu</option>
                            <?php
                            foreach ($materials as $item) {
                                ?>
                                <option
                                    value="<?php echo $item['value'] ?>"<?php echo ($item['value'] == $material) ? 'selected' : ''; ?>><?php echo $item['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="gallery" id="">
                            <option value="">Bộ sưu tập</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select name="price" id="">
                            <option value="">Giá</option>
                        </select>
                    </div>
                    <div class="col-sm-1"><input type="submit" value="OK"/></div>
            </div>
        </div>
        <div class="category-list">
            <div class="row">
                <?php
                $i = 0;

                foreach ($products as $item) {

                    $property = $_CI->property_model->getPropertyByProduct($item['id']);
                    $pp_color = isset($property[2]) ? $property[2] : null;
                    $pp_brand = isset($property[4]) ? $property[4] : null;
                    $pp_material = isset($property[5]) ? $property[5] : null;

                    // color = 2 - thuong hieu = 4
                    if ((($pp_color == $color) || (!$color)) && (($pp_brand == $brand) || (!$brand)) && (($pp_material == $material) || (!$material))) {
                        $i++;
                        ?>
                        <div class="col-sm-4">
                            <div class="item">
                                <div class="thumb"><a
                                        href="<?php echo base_url('P/' . $item['id'] . '-' . $item['alias']) ?>"><img
                                            src="<?php echo base_url('data/products/' . $item['image']) ?>"
                                            alt=""></a></div>
                                <div class="title"><a
                                        href="<?php echo base_url('P/' . $item['id'] . '-' . $item['alias']) ?>">Gối tựa
                                        AVG 100</a></div>
                                <div class="price"><?php echo number_format($item['price']) ?> VND</div>
                            </div>
                        </div>
                    <?php }
                }
                if($i==0){
                ?>
            <div class="col-sm-12">
                <div class="not-result text-center">
                    <br>
                    <br>
                    <br>
                    Không tìm thấy sản phẩm nào!
                    <br>
                    <br>
                    <br>
                </div>
            </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>

</div>