<?php



/**
 * @author luonghx
 * @copyright 2013
 */



class general_model extends CI_Model {



    function general_model() {
        parent::__construct();
    }

    function insert($table,$data){
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
    function update($table,$data,$id){
        $this->db->where('id',$id)->update($table,$data);
    }

    function delete($table,$id){
        $this->db->where('id',$id)->delete($table);
    }
    function deleteByField($table,$field,$val){
        $this->db->where($field,$val)->delete($table);
    }


}

?>