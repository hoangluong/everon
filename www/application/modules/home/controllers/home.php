<?php
class home extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("account/account_model");
        $this->load->model("industry/industry_model");
        $this->load->model("slider/slider_model");
        $this->load->model("news/news_model");
        $this->layout->setLayout('layouts/home_layout');
    }
    function index(){
        $data = array();
        $industries = $this->industry_model->getAllIndustries();
        $homeSlider = $this->slider_model->getHomeSlider();
        $homeSlot = $this->news_model->getNewsHomeSlot();
        $homeNews = array();
        foreach($homeSlot as $item){
            $homeNews[$item['homeslot']] = $item;
        }
        echo "<pre>";
        print_r($homeNews);
        echo "</pre>";
        $data['homeSlot'] = $homeNews;
        $data['ishome'] = true;
        $data['industries'] = $industries;
        $data['sliders'] = $homeSlider;
        $this->layout->view('home_views',$data);
    }
}