<?php
class industry extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("product/product_model");
        $this->load->model("industry/industry_model");
        $this->load->model("category/category_model");
        $this->load->model("slider/slider_model");
        $this->layout->setLayout('layouts/home_layout');
    }
    function index($key,$page = 1){
        $data = array();

        $numrow = 20;
        $from = ($page-1)*$numrow;
        $data['page'] = $page;
        $industry = $this->industry_model->getIndustryByKey($key);
        $products = $this->product_model->getProductByIndustry($industry[0]['id']);
        $category = $this->category_model->getCategoryByIndustry($industry[0]['id']);
        $data['products'] = $products;
        $data['industry'] = $industry;
        $data['categories'] = $category;
        $total = $this->product_model->get_total_product($industry[0]['id']);
        $data['num_page'] =ceil($total/$numrow);

        $this->layout->view('industry_views',$data);

    }

    function gift()
    {
        $id = 1;
        $data['industry'] = $industry = $this->industry_model->getIndustryById($id);
        $data['products'] = $this->product_model->getAllProducts(null,$id);
        $data['slider'] = $this->slider_model->getAllSlider($id);
        $this->layout->view('industry/gift_views', $data);
    }

    function collection(){
        $id = 3;
        $data['industry'] = $industry = $this->industry_model->getIndustryById($id);
        $data['products'] = $this->product_model->getAllProducts(null,$id);
        $data['slider'] = $this->slider_model->getAllSlider($id);

        $this->layout->view('industry/collection_views', $data);
    }
}