<?php



/**
 * @author luonghx
 * @copyright 2013
 */



class industry_model extends CI_Model {



    function industry_model() {
        parent::__construct();
    }

    function getAllIndustries(){
        $this->db->select('*')
            ->from('industries')
        ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function insertIndustry($data){
        $this->db->insert('industries',$data);
        return $this->db->insert_id();
    }

    function getIndustryByKey($key){
        $this->db->select('*')
            ->from('industries')
            ->where('key',$key)
            ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getIndustryById($id){
        $this->db->select('*')
            ->from('industries')
            ->where('id',$id)
            ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }



}

?>