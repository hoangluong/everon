<div class="graybg">
    <div class="container">
        <div class="breadcrumb">
            <ul>
                <li><a href="">Trang chủ</a> <span>/</span></li>
                <li><a href=""><?php echo $industry[0]['name'] ?></a> <span>/</span></li>
            </ul>
        </div>
        <div class="slider">
            <div><img src="<?php echo base_url('data/sliders/' . $slider[0]['image']) ?>" alt=""/></div>
        </div>
        <div class="gift-list">
            <div class="gift">
                <img src="<?php echo base_url('themes/everon/images/gift.png') ?>" alt=""/>
            </div>
            <div class="gift-body">
                <div class="gift-title">Quà tặng cho mọi người</div>
                <div class="row">
                    <?php
                    foreach ($products as $item) {
                        ?>
                        <div class="product-item">
                            <div class="col-sm-3">
                                <div class="thumb"><a href="<?php echo base_url('P/'.$item['id'].'-'.$item['alias'])?>"><img src="<?php echo base_url('data/products/' . $item['image']) ?>"
                                                        alt=""/></a></div>
                                <div class="title"><a href="<?php echo base_url('P/'.$item['id'].'-'.$item['alias'])?>"><?php echo $item['name']?></a> &nbsp; &nbsp; &nbsp;<b><?php echo number_format($item['price'])?> VNĐ</b></div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>