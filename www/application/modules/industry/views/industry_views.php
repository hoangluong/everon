<div class="breadcrumb">
    <ul>
        <li><a href="">Trang chủ</a> &nbsp;»&nbsp;</li>
        <li><a href="<?php echo base_url('C/' . $industry[0]['key']) ?>"><?php echo $industry[0]['name'] ?></a></li>
    </ul>
</div>
<div class="col-sm-3">
    <div class="row">
        <div class="cat-menu">
            <div class="cat-menu-title"><?php echo $industry[0]['name']?></div>
            <div class="cat-menu-body">
                <ul>
                    <?php
                    foreach ($categories as $item) {
                        ?>
                        <li><a href="<?php echo base_url('C/'.$item['alias'])?>"><?php echo $item['name'] ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-9">
    <div class="list-product">
        <h2 class="title"><?php echo $industry[0]['name'] ?></h2>

        <div class="filter">
            <select name="" id="">
                <option value="">Giá từ thấp đến cao</option>
                <option value="">Giá từ cao xuống thấp</option>
            </select>
        </div>
        <div class="list-product-body clearfix">
            <?php
            $i = 0;
            foreach ($products as $item) {
                $i++;
                ?>
                <div class="products-item col-sm-3">
                    <div class="thumb">
                        <img src="<?php echo base_url('data/products/' . $item['image']) ?>" alt=""/>
                    </div>
                    <div class="name"><a href="<?php echo base_url('P/'.$item['id'].'-'.$item['alias'])?>"><?php echo $item['name'] ?></a></div>
                    <div class="price">
                        <?php echo number_format($item['price']) ?> đ
                    </div>
                    <div class="buy-now">
                        <a href="<?php echo base_url('P/'.$item['id'].'-'.$item['alias'])?>">Mua ngay</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="pageding clearfix" style="text-align: right;">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <td class="pagingIntact"><a>Xem trang</a>
                </td>

                <?php
                $from = ($page - 2 > 0) ? ($page - 2) : 1;
                $to = ($page + 2 <= $num_page) ? ($page + 2) : $num_page;
                for ($i = $from; $i <= $to; $i++) {
                    if ($i != $page) {
                        ?>

                        <td class="pagingSpace"></td>
                        <td class="pagingIntact"><a
                                href="<?php echo base_url("danh-muc/" . $category[0]['alias'] . ((isset($cate_sub[0]['key'])) ? '/' . $cate_sub[0]['alias'] : '') . '/' . $i) ?><?php echo ($order_price != 'none') ? '?price=' . $order_price : '' ?>"><?php echo $i ?></a>
                        </td>
                    <?php
                    } else {
                        ?>
                        <td class="pagingSpace"></td>
                        <td class="pagingViewed"><a href="#"><?php echo $i ?></a></td>
                    <?php
                    }
                }?>


            </tr>
            </tbody>
        </table>
    </div>
</div>

      