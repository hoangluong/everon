<?php
class news extends MX_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model("account/account_model");
        $this->load->model("industry/industry_model");
        $this->load->model("slider/slider_model");
        $this->load->model("news/news_model");
        $this->layout->setLayout('layouts/home_layout');
    }

    function index($alias){
        $data = array();
        $id = (int)$alias;


        $this->layout->view('news_index_views',$data);
    }
}