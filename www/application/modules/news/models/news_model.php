<?php



/**
 * @author luonghx
 * @copyright 2013
 */



class news_model extends CI_Model {



    function news_model() {
        parent::__construct();
    }

    function getAllNews(){
        $this->db->select('*')
            ->from('news')
            ->order_by('order','desc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getNewsById($id){
        $this->db->select('*')
            ->from('news')
            ->where('id',$id) ;
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getNewsHomeSlot(){
        $this->db->select('*')
            ->from('news')
            ->where_in('homeslot',array(1,2,3,4)) ;
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }



}

?>