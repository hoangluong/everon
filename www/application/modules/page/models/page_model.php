<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 4/11/2017
 * Time: 4:44 PM
 */
class page_model extends CI_Model
{

    function page_model()
    {
        parent::__construct();
    }

    function getAllPages()
    {
        $this->db->select('*')
            ->from('pages');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }
    function getPageById($id)
    {
        $this->db->select('*')
            ->from('pages')
            ->where('id',$id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return[0];
    }


}
?>