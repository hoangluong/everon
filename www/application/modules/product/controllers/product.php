<?php

class product extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("product/product_model");
        $this->load->model("category/category_model");
        $this->load->library('cart');

        $this->layout->setLayout('layouts/home_layout');
    }

    function index($id){
        $data = array();
        $product = $this->product_model->get_product_by_id($id);
        $data['product'] = $product;
        $properties = $this->product_model->getPropertiesByProduct($id);
        $data['properties'] = $properties;
        $images = $this->product_model->getAllImagesByProduct($id);
        $data['images'] = $images;
        $data['products'] = $this->product_model->getProductByIndustry($product[0]['industry_id']);

        $this->layout->view('product_index_view',$data);
    }
}