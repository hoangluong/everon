<?php


/**
 * @author luonghx
 * @copyright 2013
 */
class product_model extends CI_Model
{


    function product_model()
    {
        parent::__construct();
    }

    function getAllProducts($text = null, $industry = null, $category = null, $sub = null, $brand = null, $supplier = null, $index = null, $page = 0, $numrow = 20)
    {
        $this->db->select('P.*, I.name as industry_name, C.name as category_name, S.name as sub_name, B.name as brand_name, SU.name as supplier_name')
            ->from('products P')
            ->join('industries I', 'I.id = P.industry_id', 'left')
            ->join('categories C', 'C.id = P.category_id', 'left')
            ->join('categories S', 'S.id = P.sub_id', 'left')
            ->join('brands B', 'B.id = P.brand_id', 'left')
            ->join('suppliers SU', 'SU.id = P.supplier_id', 'left');
        if ($text) {
            $this->db->like('P.name', $text);
            $this->db->like('P.code', $text);
        }
        if ($industry) $this->db->where('P.industry_id', $industry);
        if ($category) $this->db->where('P.category_id', $industry);
        if ($sub) $this->db->where('P.sub_id', $industry);
        if ($brand) $this->db->where('P.brand_id', $brand);
        if ($supplier) $this->db->where('P.supplier_id', $supplier);
        if ($index != null) $this->db->where('P.index', $index);


        $this->db->order_by('P.order', 'asc');
        $this->db->limit($numrow, $page * $numrow);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function get_product_by_category($category_id, $from = 0, $limit = 20, $order = 'none', $brand = 'none', $material = 'none', $color = 'none')
    {
        $this->db->select('P.*')
            ->from('products P')
            ->where('P.category_id', $category_id);
        if ($order != 'none') {
            $this->db->order_by('P.price', $order);
        }
        $this->db->order_by('P.id', 'desc');
        $this->db->limit($limit, $from);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function get_total_product($industry = null, $category = null, $brand = null, $supplier = null)
    {

        $this->db->select('*')
            ->from('products');

        if ($industry) $this->db->where("industry_id", $industry);
        if ($category) $this->db->where("category_id", $category)->or_where('sub_id', $category);
        if ($brand) $this->db->where("brand_id", $brand);
        if ($supplier) $this->db->where('supplier_id', $supplier);


        $rs = $this->db->get();
        $return = $rs->num_rows();
        $rs->free_result();
        //  die($return.'---');
        return $return;
    }

    function get_product_by_sub($sub_id, $from = 0, $limit = 20, $order = 'none')
    {
        $this->db->select('*')
            ->from('products')
            //   ->where('active',1)
            ->where('sub_id', $sub_id);
        if ($order != 'none') {
            $this->db->order_by('price', $order);
        }
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $from);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function get_product_by_id($id)
    {
        $this->db->select('P.*,C.name as category_name, S.name as sub_name, I.name as industry_name, B.name as brand_name, SU.name as supplier_name')
            ->from('products as P')
            ->join('industries I', 'I.id = P.industry_id', 'left')
            ->join('categories C', 'C.id = P.category_id', 'left')
            ->join('categories S', 'S.id = P.sub_id', 'left')
            ->join('brands B', 'B.id = P.brand_id', 'left')
            ->join('suppliers SU', 'SU.id = P.supplier_id', 'left')
            ->where('P.id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getPropertiesByProduct($productId)
    {
        $this->db->select('PP.*, P.name as property_name')
            ->from('product_properties PP')
            ->join('properties P', 'P.id = PP.property_id', 'inner')
            ->where('PP.product_id', $productId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getAllImagesByProduct($productId)
    {
        $this->db->select('PP.*')
            ->from('product_images PP')
            ->where('PP.product_id', $productId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getImageById($id)
    {
        $this->db->select('PP.*')
            ->from('product_images PP')
            ->where('PP.id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return isset($return[0]) ? $return[0] : null;
    }

    function getProductByIndustry($id)
    {
        $this->db->select('*')
            ->from('products')
            ->where('industry_id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }


}

?>