<div class="graybg">
<div class="container">
    <div class="row">
        <div class="breadcrumb">
            <ul>
                <li><a href="">Trang chủ</a><span>/</span></li>
                <li><a href=""><?php echo $product[0]['industry_name'] ?></a><span>/</span></li>
                <?php
                if ($product[0]['category_id']) {
                    ?>
                    <li><a href=""><?php echo $product[0]['category_name'] ?></a><span>/</span></li>
                <?php } ?>
                <li><?php echo $product[0]['name'] ?></li>
            </ul>
        </div>
        <div class="product-detail clearfix">
            <div class="col-sm-5">
                <div class="row">
                    <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                        <li data-thumb="<?php echo base_url('data/products/' . $product[0]['image']) ?>">
                            <img src="<?php echo base_url('data/products/' . $product[0]['image']) ?>"/>
                        </li>
                        <?php

                        foreach ($images as $item) {
                            ?>
                            <li data-thumb="<?php echo base_url('data/products/' . $item['image']) ?>">
                                <img src="<?php echo base_url('data/products/' . $item['image']) ?>"/>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="title"><?php echo $product[0]['name'] ?></div>
                <div class="vote">
                    <i class="glyphicon glyphicon-star"></i>
                    <i class="glyphicon glyphicon-star"></i>
                    <i class="glyphicon glyphicon-star"></i>
                    <i class="glyphicon glyphicon-star"></i>
                    <i class="glyphicon glyphicon-star"></i>
                    <span>| Sẵn sàng giao hàng</span>
                </div>
                <div class="price"><label for="">Giá bán:</label> <?php echo number_format($product[0]['price']) ?>
                    VNĐ
                </div>
                <div class="col-sm-12 size">
                    <select name="" class="slSize col-sm-8" id="size">
                        <option value="1">L</option>
                        <option value="2">XL</option>
                        <option value="3">XXL</option>
                    </select>
                    <select name="" class="slQty col-sm-4" id="qty">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
                <div>
                    <button class="btn-buy" onclick="addToCart(<?php echo $product[0]['id']?>,$('#qty').val(),$('#size').val())">MUA</button>
                </div>
                <div class="shoping-info">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/cart-icon.jpg"
                                                  alt=""/></div>
                            <div>Thêm vào giỏ hàng</div>
                        </div>
                        <div class="col-sm-3">
                            <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/cart-icon.jpg"
                                                  alt=""/></div>
                            <div>Sản phẩm yêu thích</div>
                        </div>
                        <div class="col-sm-3">
                            <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/cart-icon.jpg"
                                                  alt=""/></div>
                            <div>Share</div>
                        </div>
                        <div class="col-sm-3">
                            <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/cart-icon.jpg"
                                                  alt=""/></div>
                            <div>Đại lý</div>
                        </div>
                    </div>
                </div>
                <hr/>
                <ul class="nav nav-tabs product-tabs">
                    <li class="active"><a data-toggle="tab" href="#desc">Miêu tả</a></li>
                    <li><a data-toggle="tab" href="#detail">Chi tiết</a></li>
                    <li><a data-toggle="tab" href="#vote">Đánh giá của khách hàng</a></li>

                </ul>

                <div class="tab-content">
                    <div id="desc" class="tab-pane fade in active">

                        <p><?php echo $product[0]['desc'] ?></p>
                    </div>
                    <div id="detail" class="tab-pane fade">

                        <p><?php echo $product[0]['content'] ?></p>
                    </div>
                    <div id="vote" class="tab-pane fade">


                    </div>

                </div>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <hr/>
        <div class="ship-info">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/car-icon.jpg" alt=""/>
                    </div>
                    <div>Miễn phí vận chuyển</div>
                </div>
                <div class="col-sm-2">
                    <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/donggoi-ico.jpg"
                                          alt=""/></div>
                    <div>Đóng gói sản phẩm</div>
                </div>
                <div class="col-sm-2">
                    <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/chinhsach-icon.jpg"
                                          alt=""/></div>
                    <div>Chính sách đổi trả</div>
                </div>
                <div class="col-sm-2">
                    <div class="ico"><img src="<?php echo base_url('themes/everon') ?>/images/chinhsach-icon.jpg"
                                          alt=""/></div>
                    <div>Bảo hành 24 tháng</div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>

    </div>
</div>

<div class="white-bg ">
    <div class="container">
        <div class="col-sm-8">
            <div class="list-product">
                <div class="list-title">Tự chọn</div>
                <hr/>
                <div class="list-body">
                    <?php
                    foreach ($products as $item) {
                        ?>
                        <div class="product-item clearfix">
                            <div class="col-sm-5">
                                <div class="thumb">
                                    <img src="<?php echo base_url('data/products/' . $item['image']) ?>" alt=""/>
                                </div>
                            </div>
                            <div class="col-sm-7">

                                <div class="title"><?php echo $item['name'] ?></div>
                                <div class="vote">
                                    <i class="glyphicon glyphicon-star"></i>
                                    <i class="glyphicon glyphicon-star"></i>
                                    <i class="glyphicon glyphicon-star"></i>
                                    <i class="glyphicon glyphicon-star"></i>
                                    <i class="glyphicon glyphicon-star"></i>
                                    <span>| Sẵn sàng giao hàng</span>
                                </div>
                                <div class="price"><label for="">Giá
                                        bán:</label> <?php echo number_format($item['price']) ?> VNĐ
                                </div>
                                <div class="col-sm-12 size">
                                    <select name="" class="slSize col-sm-8" id="size<?php echo $item['id']?>">
                                        <option value="">L</option>
                                        <option value="">XL</option>
                                        <option value="">XXL</option>
                                    </select>
                                    <select name=""  class="slQty col-sm-4" id="qty<?php echo $item['id']?>">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                    </select>
                                </div>
                                <div>
                                    <button class="btn-buy btn-green" onclick="addToCart(<?php echo $item['id']?>,$('#qty<?php echo $item['id']?>').val(),$('#size<?php echo $item['id']?>').val())">THÊM VÀO GIỎ HÀNG</button>
                                </div>

                            </div>

                        </div>
                        <hr/>
                    <?php } ?>
                </div>
            </div>

        </div>
        <div class="col-sm-4">
            <div class="choosed">
                <div class="title">Đã chọn</div>
                <div class="sub-title">Đi tới giỏ hàng</div>
                <div class="cart">

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    $('#image-gallery').lightSlider({
        gallery: true,
        item: 1,
        thumbItem: 5,
        slideMargin: 0,
        speed: 500,
        auto: false,
        loop: true,
        onSliderLoad: function () {
            $('#image-gallery').removeClass('cS-hidden');
        }
    });
    function addToCart(pid,qty,size) {
        $.ajax({
            url : base_url + 'cart/add',
            type: 'get',
            data: {id:pid,size:size,qty:qty},
            success:function(e){
                alert(e);
            }
        });
    }

</script>
</div>