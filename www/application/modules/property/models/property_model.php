<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 3/13/2017
 * Time: 5:36 PM
 */
class property_model extends CI_Model
{

    function property_model()
    {
        parent::__construct();
    }

    function get_all_properties()
    {
        $this->db->select('*')
            ->from('properties');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getPropertyById($id)
    {
        $this->db->select('*')
            ->from('properties')
            ->where('id', $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return[0];
    }

    function getPropertyData($propertyId)
    {
        $this->db->select('*')
            ->from('property_datas')
            ->where('property_id', $propertyId);
        $rs = $this->db->get();
        $return2 = $rs->result_array();
        $rs->free_result();
        return $return2;
    }

    function getPropertyByProduct($productId)
    {
        $this->db->select('*')
            ->from('product_properties')
            ->where('product_id', $productId);
        $rs = $this->db->get();
        $return2 = $rs->result_array();
        $rs->free_result();
        $data = array();
        foreach ($return2 as $item){
            $data[$item['property_id']] = $item['value'];
        }
        return $data;
    }

    function getPropertyHtml($id, $focusData = null)
    {

        $return2 = $this->getPropertyData($id);
        $thisProperty = $this->getPropertyById($id);
        $type = $thisProperty['type'];
        if ($type == 1) {
            $html = '<input name="property_' . $id . '" type="text" value="' . (($focusData) ? $focusData : '') . '" class="form-control" placeholder="' . $thisProperty['name'] . '"/>';
        } elseif ($type == 2) {
            $html = '<select name="property_' . $id . '"  class="form-control" >';
            $html .= '<option value="">' . $thisProperty['name'] . '</option>';
            foreach ($return2 as $item) {
                $html .= '<option value="' . $item['value'] . '" ' . (($focusData) ? 'selected' : '') . '>' . $item['name'] . '</option>';
            }
            $html .= '</select>';
        } else {
            $html = $thisProperty['type'];
        }

        echo $html;
    }


}