<?php



/**
 * @author luonghx
 * @copyright 2013
 */



class slider_model extends CI_Model {



    function slider_model() {
        parent::__construct();
    }

    function getHomeSlider(){
        $this->db->select('SL.*, I.name as industry_name')
            ->from('sliders SL')
            ->join('industries I', 'I.id = SL.industry_id', 'inner')
            ->join('categories C', 'C.id = SL.category_id', 'inner')
            ->join('categories S', 'S.id = SL.sub_id', 'inner');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getAllSlider($industryId = null,$categoryId = null,$subId = null){
        $this->db->select('SL.*, I.name as industry_name')
            ->from('sliders SL')
            ->join('industries I', 'I.id = SL.industry_id', 'left')
            ->join('categories C', 'C.id = SL.category_id', 'left')
            ->join('categories S', 'S.id = SL.sub_id', 'left');
        if($industryId) $this->db->where('SL.industry_id',$industryId);
        if($categoryId) $this->db->where('SL.category_id',$categoryId);
        if($subId) $this->db->where('SL.sub_id',$subId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getSliderById($id){
        $this->db->select('SL.*, I.name as industry_name')
            ->from('sliders SL')
            ->join('industries I', 'I.id = SL.industry_id', 'left')
            ->join('categories C', 'C.id = SL.category_id', 'left')
            ->join('categories S', 'S.id = SL.sub_id', 'left')
            ->where('SL.id',$id)
        ;
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }








}

?>