<?php



/**
 * @author luonghx
 * @copyright 2013
 */



class supplier_model extends CI_Model {



    function supplier_model() {
        parent::__construct();
    }

    function getAllSuppliers(){
        $this->db->select('*')
            ->from('suppliers')
        ->order_by('order','asc');
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function insertSupplier($data){
        $this->db->insert('supplier',$data);
        return $this->db->insert_id();
    }





}

?>