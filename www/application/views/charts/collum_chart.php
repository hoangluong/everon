<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Highcharts Example</title>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript">
            $(function () {
                var chart;
                $(document).ready(function() {
                    
                    Highcharts.setOptions({
                        title: {
                            style: {
                                color: '#333',
                                fontWeight: 'normal',
                                fontSize: '15px',
                                fontFamily: 'Arial, Verdana, sans-serif',
                                fontWeight: 'bold',
                                color: '#146E9D'

                            }            
                        }
                        
                       
                    });
                    
                    var colors = Highcharts.getOptions().colors,
                    categories = <?php echo json_encode($categories); ?>,
                    data = [
<?php
$i = 0;
foreach ($collumData as $cData):
    ?>
                        {
                            y: <?php echo $cData; ?>,
                            color: colors[<?php echo $i; ?>]
                        },
    <?php
    $i++;
endforeach;
?>
            ];
    
            function setChart(name, categories, data, color) {
                chart.xAxis[0].setCategories(categories, false);
                chart.series[0].remove(false);
                chart.addSeries({
                    name: name,
                    data: data,
                    color: color || 'white'
                }, false);
                chart.redraw();
            }
    
            chart = new Highcharts.Chart({
                
                chart: {
                    renderTo: 'container',
                    type: 'column'
                },
                credits : {
                    enabled : false
                },
               
                title: {
                    text: ''
                },
                       
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    title: {
                        text: 'Số lượng bầu chọn của khán giả'
                    }
                },
                
                plotOptions: {
                    column: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function() {
                                    var drilldown = this.drilldown;
                                    if (drilldown) { // drill down
                                        setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                                    } else { // restore
                                        setChart(name, categories, data);
                                    }
                                }
                            }
                        },
                        dataLabels: {
                            enabled: true,
                            color: colors[0],
                            style: {
                                fontWeight: 'bold'
                            },
                            formatter: function() {
                                return this.y ;
                            }
                        }
                    }
                },
                tooltip: {
                    formatter: function() {
                        var point = this.point,
                        s = this.x +':<b>'+ this.y +' bình chọn</b><br/>';
                        if (point.drilldown) {
                            s += 'Click to view '+ point.category +' versions';
                        } else {
                            s += '';
                        }
                        return s;
                    }
                },
                series: [{
                        name: name,
                        data: data,
                        color: 'white',
                        showInLegend : false
                    }],
                exporting: {
                    enabled: false
                }
              
            });
            $("#chartSVG").val(chart.getSVG());
        });
        
    });
        </script>


    </head>
    <body>
        <script src="<?php echo base_url(); ?>public/js/hightchart/js/highcharts.js"></script>
        <script src="<?php echo base_url(); ?>public/js/hightchart/js/modules/exporting.js"></script>

        <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
        <input type="hidden" name="chartSVG" id="chartSVG" value=''/>

    </body>
</html>
