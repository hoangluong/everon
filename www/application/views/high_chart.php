<script type="text/javascript">
$(function () {
    var chart;
    var dataX = <?php echo json_encode($x); ?>;
    var dX =  new Array();    
    for(var i=0; i< dataX.length; i++ ){
		dX[i] = dataX[dataX.length - 1 - i];
    }    
    var dataY = <?php echo json_encode($y);?>;//[28, 21, 69, 28, 28, 34, 42]
    var dY =  new Array();    
    for(var i=0; i< dataY.length; i++ ){
		dY[i] = dataY[dataY.length - 1 - i];
    }  	
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                type: 'line',
				marginLeft:100,
                marginRight: 130,
                marginBottom: 25
            },
            title: {
               style:{
					display:'none'
               }
            },
            subtitle: {
               
            },
            exporting:{
				enabled:false
            },
            legend:{
				enabled:false
            },
            xAxis: {
                categories: dX,
				labels:
				{
				  enabled: false
				},
				tickmarkPlacement:'on',
				showFirstLabel:true,
				showLastLabel:true	
            },
            yAxis: {
				min: 0,
				minPadding: 50,
				//maxPadding: 0.5,				
                title: {
                    text: 'Clicks'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.x +'</b><br/>'+
                         'Clicks:'+ this.y ;
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [{
                name: 'Clicks',
                data: dY
            }]
        });
		chart.setSize(1000, 150);
        $("#chartSVG").val(chart.getSVG());
    });
    
});
		</script>	
<script src="<?php echo base_url('public/js/highcharts/js/highcharts.js')?>"></script>
<script src="<?php echo base_url('public/js/highcharts/js/modules/exporting.js')?>"></script>
<div id="container" style="min-width: 400px; height: 150px; margin: 0 auto"></div>
<input type="hidden" name="chartSVG" id="chartSVG" value=''/>