<?php
$_CI = &get_instance();
$_CI->load->model('category/category_model');
$gifts = $this->category_model->getCategoryByIndustry(1, true);
$bedroom = $this->category_model->getCategoryByIndustry(2, true);
$collection = $this->category_model->getCategoryByIndustry(3, true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
    <link rel="stylesheet" href="<?php echo base_url('themes/everon/') ?>/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url('themes/everon/') ?>/css/lightslider.min.css">
    <title>Title</title>
</head>
<body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="<?php echo base_url('themes/everon/') ?>/js/lightslider.min.js"></script>
<script>
    var base_url = '<?php echo base_url()?>';
</script>
<div class="wrapper">
    <div class="header clearfix">

        <div class="logo"><a href="<?php echo base_url() ?>"><img
                    src="<?php echo base_url('themes/everon/') ?>/images/logo.png" alt=""></a>
        </div>
        <div class="menu">
            <ul>
                <li row-id="gift"><a href="<?php echo base_url('gift') ?>">Gift</a></li>
                <li row-id="bedroom"><a href="javascript:void(0)">Bedroom</a></li>
                <li row-id="collection"><a href="<?php echo base_url('collection') ?>">Collection</a></li>
            </ul>
            <div class="sub-menu">

                <div class="container">
                    <div class="sub-bedroom">

                        <?php
                        foreach ($bedroom as $item) {
                            ?>
                            <div class="sub-item">
                                <div class="thumb">
                                    <a href="<?php echo base_url('bedroom/' . $item['alias']) ?>">
                                        <img src="<?php echo base_url('data/category/' . $item['image']) ?>" alt=""/>
                                    </a>
                                </div>
                                <div class="title">
                                    <a href="<?php echo base_url('bedroom/' . $item['alias']) ?>">
                                        <?php echo $item['name'] ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="sub-collection">
                        <?php
                        foreach ($collection as $item) {
                            ?>
                            <div class="sub-item">
                                <div class="thumb">
                                    <a href="<?php echo base_url('collection/' . $item['alias']) ?>">
                                        <img src="<?php echo base_url('data/category/' . $item['image']) ?>" alt=""/>
                                    </a>
                                </div>
                                <div class="title">
                                    <a href="<?php echo base_url('collection/' . $item['alias']) ?>">
                                        <?php echo $item['name'] ?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </div>
            <div class="sub-login" style="display: none">
                <div class="container">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="col-sm-7 login-left">
                            <div class="row">
                                <form action="">
                                    <div class="login-title">Đăng Nhập</div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3"><label for="">Email</label></div>
                                        <div class="col-sm-9"><input type="text"/></div>
                                    </div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3"><label for="">Mật khẩu</label></div>
                                        <div class="col-sm-9"><input type="text"/></div>
                                    </div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3">&nbsp;</div>
                                        <div class="col-sm-9"><input type="submit" value="Đăng nhập"/>
                                            <a href="">Quên mật khẩu?</a></div>
                                    </div>
                                    <div class="frm clearfix">
                                        <div class="col-sm-3">&nbsp;</div>
                                        <div class="col-sm-9">
                                            <div class="nomr-text">Bạn chưa có tài khoản ?</div>
                                            <div class="btn-register clearfix">
                                                <a href="#">Đăng ký thành viên</a>
                                            </div>
                                        </div>
                                    </div>


                                </form>


                            </div>
                        </div>
                        <div class="col-sm-1">
                            <img src="<?php echo base_url('themes/everon/images/or.jpg') ?>" alt=""/>
                        </div>
                        <div class="col-sm-4">
                            <div class="connect">
                                <a href=""><img src="<?php echo base_url('themes/everon/images/facebook.jpg') ?>"
                                                alt=""/></a>
                                <a href=""><img src="<?php echo base_url('themes/everon/images/google.jpg') ?>" alt=""/></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-login">
            <span class="search"><i class="glyphicon glyphicon-search"></i> </span>
            <span class="login">
            <a href="javascript:$('.sub-login').slideToggle();$('.sub-menu').height(0);$('.sub-bedroom, .sub-collection').hide();">Login</a></span>
            <span class="showCartNumber"><a href="<?php echo base_url('cart')?>"><?php echo $this->cart->total_items();?></a></span>
        </div>
    </div>

    <div class="main">
        <?php echo $content_for_layout; ?>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <ul>
                        <li class="title">Hỗ trợ</li>
                        <li><a href="">FAQ</a></li>
                        <li><a href="">Liên hệ</a></li>
                        <li><a href="">Đổi trả</a></li>
                        <li><a href="">Bảo hành</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul>
                        <li class="title">Thông tin hữu ích</li>
                        <li><a href="">Everon FAMILY</a></li>
                        <li><a href="">Chính sách vận chuyển</a></li>
                        <li><a href="">Tìm cửa hàng</a></li>
                        <li><a href="">Sự kiện</a></li>
                        <li><a href="">BB</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul>
                        <li class="title">Về chúng tôi</li>
                        <li><a href="">Everpia JSC</a></li>
                        <li><a href="">Tin tức</a></li>
                        <li><a href="">Đầu tư</a></li>
                        <li><a href="">Việc làm</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul>
                        <li class="title">Social Media</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="thisMenu" value=""/>
<script type="application/javascript">
    $('.hero-slider').slick({
        arrows: false,
        dotted: false
    });
    $('.sub-collection, .sub-bedroom').slick({
        arrows: false,
        dotted: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1
    });
    $('.menu li a').hover(function () {


        var id = $(this).parent().attr('row-id');
        if (id == 'bedroom') {
            $('.sub-menu').height(320);

            if ($('.sub-bedroom').css('display') == 'none') {
                if ($('#thisMenu').val() != 'bedroom') {
                    $('.sub-bedroom').slideDown();
                    $('.sub-bedroom').show();

                } else {
                    $('.sub-bedroom').show();
                }
                $('#thisMenu').val('bedroom');
            }
            $('.sub-collection, .sub-bedroom').slick('unslick');
            $('.sub-bedroom').slick({
                arrows: false,
                dotted: false,
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1
            });
            $('.sub-collection, .sub-login').hide();
        } else if (id == 'collection') {
            $('.sub-menu').height(320);

            if ($('.sub-collection').css('display') == 'none') {
                if ($('#thisMenu').val() != 'collection') {
                    $('.sub-collection').slideDown();
                    $('.sub-collection').show();


                } else {
                    $('.sub-collection').show();
                }

                $('#thisMenu').val('collection');
            }
            $('.sub-collection, .sub-bedroom').slick('unslick');
            $('.sub-collection').slick({
                arrows: false,
                dotted: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1
            });
            $('.sub-bedroom, .sub-login').hide();
        } else {
            $('.sub-menu').height(0);
            $('.sub-bedroom, .sub-collection').hide();
        }
    });


</script>
</body>
</html>